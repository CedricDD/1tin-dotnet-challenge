﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
#endregion

namespace Quiz
{
    public partial class ExportMessageBox : Window
    {
        #region Properties
        private String fullPath;
        #endregion

        #region Cnstructor
        public ExportMessageBox(String fullPath)
        {
            InitializeComponent();
            this.fullPath = fullPath;
            okButton.Opacity = Constants.OpacityPointOne;
            okButton.IsEnabled = false;
        }
        #endregion

        #region Methods
        public void SetVisibilityOKButton()                                 //Enable the okButton
        {
            okButton.Opacity = Constants.OpacityFull;
            okButton.IsEnabled = true;
        }
        #endregion

        #region Handlers
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Constants.ExplorerEXE, Constants.ExplorerSelect + fullPath);           //Open folder and highlight exported file
            this.Close();                                                                        //Close window
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
