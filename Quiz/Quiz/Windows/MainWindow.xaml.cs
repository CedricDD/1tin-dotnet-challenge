﻿#region ClassComment
/*
 * Created by Brecht Philips - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Data;
using System.Xaml;
#endregion

namespace Quiz
{
    public partial class MainWindow : Window
    {
        #region Properties
        private User loggedInUser;
        Game game;
        #endregion

        #region Constructor
        public MainWindow()
        {
                InitializeComponent();
                SetLayoutLoggedOff();
                ResetButtonIndicators();
                SetToolTips();
        }
        #endregion

        #region Methods

        private void SetToolTips()
        {
            logOffButton.ToolTip = Constants.LogOffButtonToolTip;
            quitButton.ToolTip = Constants.QuitButtonToolTip;
            mathQuizButton.ToolTip = Constants.MathQuizButtonToolTip;
            languageQuizButton.ToolTip = Constants.LanguageQuizButtonToolTip;
            knowledgeQuizButton.ToolTip = Constants.KnowledgeQuizButtonToolTip;
            manageQuestionsButton.ToolTip = Constants.ManageQuestionsButtonToolTip;
            reportButton.ToolTip = Constants.ReportButtonToolTip;
            aboutImage.ToolTip = Constants.AboutButtonToolTip;
            TitleBar.ToolTip = Constants.TitleBarToolTip;
            userPreferencesButton.ToolTip = Constants.UserPreferencesButtonToolTip;
            minimizeButton.ToolTip = Constants.MinimizeButtonToolTip;
            startGameButton.ToolTip = Constants.StartGameButtonToolTip;
        }

        private void ResetButtonIndicators()
        {
            mathQuizButtonIndicator.Visibility = Visibility.Hidden;
            languageQuizButtonIndicator.Visibility = Visibility.Hidden;
            knowledgeQuizButtonIndicator.Visibility = Visibility.Hidden;
            manageQuestionsButtonIndicator.Visibility = Visibility.Hidden;
            startGameButtonIndicator.Visibility = Visibility.Hidden;
            reportButtonIndicator.Visibility = Visibility.Hidden;
        }

        public void ResetButtons()
        {
            mathQuizButton.IsEnabled = true;
            mathQuizButton.Opacity = Constants.OpacityFull;
            languageQuizButton.IsEnabled = true;
            languageQuizButton.Opacity = Constants.OpacityFull;
            knowledgeQuizButton.IsEnabled = true;
            knowledgeQuizButton.Opacity = Constants.OpacityFull;
            startGameButton.IsEnabled = true;
            startGameButton.Opacity = Constants.OpacityFull;
            ResetButtonIndicators();
        }

        public void SetButtons(Button temp)
        {
            temp.IsEnabled = false;
            switch (temp.Name)
            {
                case Constants.MathQuizButtonName:
                    languageQuizButton.IsEnabled = false;
                    languageQuizButton.Opacity = Constants.OpacityPointOne;
                    knowledgeQuizButton.IsEnabled = false;
                    knowledgeQuizButton.Opacity = Constants.OpacityPointOne;
                    startGameButton.IsEnabled = false;
                    startGameButton.Opacity = Constants.OpacityPointOne;
                    break;
                case Constants.LanguageQuizButtonName:
                    mathQuizButton.IsEnabled = false;
                    mathQuizButton.Opacity = Constants.OpacityPointOne;
                    knowledgeQuizButton.IsEnabled = false;
                    knowledgeQuizButton.Opacity = Constants.OpacityPointOne;
                    startGameButton.IsEnabled = false;
                    startGameButton.Opacity = Constants.OpacityPointOne;
                    break;
                case Constants.KnowledgeQuizButtonName:
                    mathQuizButton.IsEnabled = false;
                    mathQuizButton.Opacity = Constants.OpacityPointOne;
                    languageQuizButton.IsEnabled = false;
                    languageQuizButton.Opacity = Constants.OpacityPointOne;
                    startGameButton.IsEnabled = false;
                    startGameButton.Opacity = Constants.OpacityPointOne;
                    break;
            }
            if (loggedInUser.GetUserType().Equals(Constants.UserTypeAdminText))
            {
                ResetButtons();
            }

        }

        public void SetLayoutStudent()
        {
            ResetButtonIndicators();
            logOffButton.Opacity = Constants.OpacityFull;
            logOffButton.IsEnabled = true;
            manageQuestionsButton.Opacity = Constants.OpacityPointOne;
            manageQuestionsButton.IsEnabled = false;
            reportButton.Opacity = Constants.OpacityPointOne;
            reportButton.IsEnabled = false;
            mathQuizButton.Opacity = Constants.OpacityFull;
            mathQuizButton.IsEnabled = true;
            languageQuizButton.Opacity = Constants.OpacityFull;
            languageQuizButton.IsEnabled = true;
            knowledgeQuizButton.Opacity = Constants.OpacityFull;
            knowledgeQuizButton.IsEnabled = true;
            startGameButton.Opacity = Constants.OpacityFull;
            startGameButton.IsEnabled = true;
            mainContentControl.Content = new SplashScreenUserControl();
            userPreferencesButton.Visibility = Visibility.Visible;
            userPreferencesPanel.Visibility = Visibility.Visible;
            adminPreferencesPanel.Visibility = Visibility.Collapsed;
            exportUserReportButton.Visibility = Visibility.Visible;
        }

        private void SetLayoutTeacher()
        {
            ResetButtonIndicators();
            logOffButton.Opacity = Constants.OpacityFull;
            logOffButton.IsEnabled = true;
            manageQuestionsButton.Opacity = Constants.OpacityFull;
            manageQuestionsButton.IsEnabled = true;
            reportButton.Opacity = Constants.OpacityFull;
            reportButton.IsEnabled = true;
            mathQuizButton.Opacity = Constants.OpacityPointOne;
            mathQuizButton.IsEnabled = false;
            languageQuizButton.Opacity = Constants.OpacityPointOne;
            languageQuizButton.IsEnabled = false;
            knowledgeQuizButton.Opacity = Constants.OpacityPointOne;
            knowledgeQuizButton.IsEnabled = false;
            startGameButton.Opacity = Constants.OpacityPointOne;
            startGameButton.IsEnabled = false;
            mainContentControl.Content = new SplashScreenUserControl();
            userPreferencesButton.Visibility = Visibility.Visible;
            userPreferencesPanel.Visibility = Visibility.Visible;
            adminPreferencesPanel.Visibility = Visibility.Collapsed;
            exportUserReportButton.Visibility = Visibility.Collapsed;
        }

        private void SetLayoutAdmin()
        {
            ResetButtonIndicators();
            logOffButton.Opacity = Constants.OpacityFull;
            logOffButton.IsEnabled = true;
            manageQuestionsButton.Opacity = Constants.OpacityFull;
            manageQuestionsButton.IsEnabled = true;
            reportButton.Opacity = Constants.OpacityFull;
            reportButton.IsEnabled = true;
            mathQuizButton.Opacity = Constants.OpacityFull;
            mathQuizButton.IsEnabled = true;
            languageQuizButton.Opacity = Constants.OpacityFull;
            languageQuizButton.IsEnabled = true;
            knowledgeQuizButton.Opacity = Constants.OpacityFull;
            knowledgeQuizButton.IsEnabled = true;
            startGameButton.Opacity = Constants.OpacityFull;
            startGameButton.IsEnabled = true;
            mainContentControl.Content = new SplashScreenUserControl();
            userPreferencesButton.Visibility = Visibility.Visible;
            userPreferencesPanel.Visibility = Visibility.Collapsed;
            adminPreferencesPanel.Visibility = Visibility.Visible;
        }

        private void SetLayoutLoggedOff()
        {
            ResetButtonIndicators();
            logOffButton.Opacity = Constants.OpacityPointOne;
            logOffButton.IsEnabled = false;
            manageQuestionsButton.Opacity = Constants.OpacityPointOne;
            manageQuestionsButton.IsEnabled = false;
            reportButton.Opacity = Constants.OpacityPointOne;
            reportButton.IsEnabled = false;
            mathQuizButton.Opacity = Constants.OpacityPointOne;
            mathQuizButton.IsEnabled = false;
            languageQuizButton.Opacity = Constants.OpacityPointOne;
            languageQuizButton.IsEnabled = false;
            knowledgeQuizButton.Opacity = Constants.OpacityPointOne;
            knowledgeQuizButton.IsEnabled = false;
            startGameButton.Opacity = Constants.OpacityPointOne;
            startGameButton.IsEnabled = false;
            mainContentControl.Content = new LoginRegisterUserControl(this, Constants.UserTypeStudentText);
            loggedInlabel.Content = string.Empty;
            loggedInUser = null;
            userPreferencesButton.Visibility = Visibility.Collapsed;
            userChangePrefsBorder.Visibility = Visibility.Collapsed;
            userPreferencesBorder.Visibility = Visibility.Collapsed;
        }

        public void UpdateLoggedInUserLabel()
        {
            loggedInlabel.ToolTip = String.Format("{0} {1}\n{2} {3}", Constants.LoginNameText, loggedInUser.GetLoginName, Constants.GroupText, loggedInUser.GetGroup);
            if (loggedInUser.GetUserType().Equals(Constants.UserTypeStudentText))
            {
            Student tempStudent = (Student)loggedInUser;
            loggedInlabel.Content = String.Format("{0}: {1}\n{2} {3}\n{4} {5}",
            tempStudent.GetUserType(), String.Format("{0} {1}",
            tempStudent.GetFirstName, tempStudent.GetLastName),
            Constants.QuizScoreText, tempStudent.GetQuizScore, Constants.GameHighScoreText, tempStudent.GetGameScore);
            }
            else
            {
                loggedInlabel.ToolTip = String.Format("{0} {1}\n{2} {3}", Constants.LoginNameText, loggedInUser.GetLoginName, Constants.GroupText, loggedInUser.GetGroup);
                loggedInlabel.Content = String.Format("{0}: {1}", loggedInUser.GetUserType(), String.Format("{0} {1}", loggedInUser.GetFirstName, loggedInUser.GetLastName));
            }
        }

        public void SetCorrectLayout()
        {
            if (loggedInUser.GetUserType().Equals(Constants.UserTypeAdminText))
            {
                SetLayoutAdmin();
            }
            else if (loggedInUser.GetUserType().Equals(Constants.UserTypeStudentText))
            {
                SetLayoutStudent();
            }
            else
            {
                SetLayoutTeacher();
            }
        }

        private void ToggleUserPrefsVisibility(bool prefButton)
        {
            if (prefButton)
            {
                if (userPreferencesBorder.Visibility == Visibility.Visible)
                {
                    userPreferencesBorder.Visibility = Visibility.Collapsed;
                }
                else
                {
                    userPreferencesBorder.Visibility = Visibility.Visible;
                }
                if (userChangePrefsBorder.Visibility == Visibility.Visible)
                {
                    userChangePrefsBorder.Visibility = Visibility.Collapsed;
                }
            }
            else
            {
                if (userPreferencesBorder.Visibility == Visibility.Visible)
                {
                    userPreferencesBorder.Visibility = Visibility.Collapsed;
                }
                if (userChangePrefsBorder.Visibility == Visibility.Visible)
                {
                    userChangePrefsBorder.Visibility = Visibility.Collapsed;
                }
            }
        }

        private void ExportUserReport()
        {
            ExcelExport.GenerateUserReport((Student)loggedInUser);
        }

        #region Comments
        //Date: 30/04/15 15:00
        //Author: Cédric De Dycker
        #endregion
        private Game SetupGame()
        {
            contentGrid.Background = Constants.GrayTwoBrush;
            if (game != null && game.GetRunningState())
            {
                game.StopGame();
            }
            if (this.loggedInUser is Student)
            {
                game = new Game(this, ((Student)this.loggedInUser).GetQuizScore);
            }
            else
            {
                game = new Game(this, Constants.AdminPlayTime);
            }
            game.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            game.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            mainContentControl.Content = game;
            game.StartLoop();
            return game;
        }

        private void CheckGameStatus()
        {
            if (game != null && game.GetRunningState())
            {
                game.StopGame();
            }
            else return;
        }

        public User GetLoggedInUser()
        {
            return loggedInUser;
        }
        public void SetLoggedInUser(User u)
        {
            loggedInUser = u;
        }

        private void FindAndSetIndicator(Button tempButton)
        {
            Border tempBorder = (Border)RootWindow.FindName(String.Format("{0}{1}", tempButton.Name, Constants.IndicatorName));     //Finds the correct Border underneath the clicked button
            tempBorder.Visibility = Visibility.Visible;                                                                             //Makes the border visible
        }
        #endregion

        #region Handlers

        private void QuizClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            ResetButtonIndicators();
            SetButtons(tempButton);
            switch (tempButton.Name)
            {
                case Constants.MathQuizButtonName:
                    CheckGameStatus();
                    this.mainContentControl.Content = new TakeQuizUserControl(Constants.MathCourse, this);
                    break;
                case Constants.LanguageQuizButtonName:
                    CheckGameStatus();
                    this.mainContentControl.Content = new TakeQuizUserControl(Constants.LanguageCourse, this);
                    break;
                case Constants.KnowledgeQuizButtonName:
                    CheckGameStatus();
                    this.mainContentControl.Content = new TakeQuizUserControl(Constants.KnowledgeCourse, this);
                    break;
            }
            ToggleUserPrefsVisibility(false);
            FindAndSetIndicator(tempButton);
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            ResetButtonIndicators();                                                                                     //Hide all button indicators
            ToggleUserPrefsVisibility(false);
            switch (tempButton.Name)
            {
                case Constants.ManageQuestionsButtonName:
                    CheckGameStatus();
                    SetCorrectLayout();
                    try
                    {
                        this.mainContentControl.Content = new ManageQuestionsUserControl();                              //Open the question management
                    }
                    catch (FileCorruptException ex)
                    {
                        new CustomMessageBox(ex.Message).Show(); ;
                        FileManager.WriteToLogFile(String.Concat(Constants.FileCorruptText, ex.GetFilePath()));
                    }
                    break;
                case Constants.ReportButtonName:
                    CheckGameStatus();
                    try
                    {
                        this.mainContentControl.Content = new ReportsUserControl();                                      //Open the user reports
                    }
                    catch (FileCorruptException ex)
                    {
                        new CustomMessageBox(ex.Message).Show();
                        FileManager.WriteToLogFile(String.Concat(Constants.FileCorruptText, ex.GetFilePath()));
                    }
                    break;
                case Constants.StartGameButtonName:
                    CheckGameStatus();
                    this.mainContentControl.Content = SetupGame();
                    break;
                default:
                    break;
            }
            FindAndSetIndicator(tempButton);
       }
    
        private void AboutMouseEnterHandler(object sender, MouseEventArgs e)
        {
            Image temp = (Image)sender;
            temp.Opacity = 0.5;
        }

        private void AboutMouseLeaveHandler(object sender, MouseEventArgs e)
        {
            Image temp = (Image)sender;
            temp.Opacity = 1;
        }

        private void AboutMouseClickHandler(object sender, MouseButtonEventArgs e)
        {
            if (!Application.Current.Windows.OfType<AboutWindow>().Any())
            {
                AboutWindow about = new AboutWindow();
                about.Show();
            }
        }

        private void TopButtonsClickHandler(object sender, RoutedEventArgs e)
        {
            Button temp = (Button)sender;
            switch (temp.Name)
            {
                case Constants.QuitButtonName:
                    CheckGameStatus();
                    Application.Current.Shutdown();
                    break;
                case Constants.LogOffButtonName:
                    CheckGameStatus();
                    SetLayoutLoggedOff();                                                                           //Logoff current user
                    break;
                case Constants.UserPrefsButtonName:
                    CheckGameStatus();
                    ToggleUserPrefsVisibility(true);
                    break;
                case Constants.ChangeFirstNameButtonName:
                    userChangePrefsBorder.Visibility = Visibility.Visible;
                    userChangePrefsControl.Content = new UserPreferencesUserControl(Constants.changeFirstNameGridName, this);
                    break;
                case Constants.ChangeLastNameButtonName:
                    userChangePrefsBorder.Visibility = Visibility.Visible;
                    userChangePrefsControl.Content = new UserPreferencesUserControl(Constants.changeLastNameGridName, this);
                    break;
                case Constants.ChangePasswordButtonName:
                    userChangePrefsBorder.Visibility = Visibility.Visible;
                    userChangePrefsControl.Content = new UserPreferencesUserControl(Constants.changePasswordGridName, this);
                    break;
                case Constants.ExportUserReportButtonName:
                    ExportUserReport();
                    break;
                case Constants.AddTeacherButtonName:
                    mainContentControl.Content = new LoginRegisterUserControl(this, Constants.UserTypeTeacherText);
                    userPreferencesBorder.Visibility = Visibility.Collapsed;
                    break;
                case Constants.MinimizeButtonName:
                    this.WindowState = WindowState.Minimized;
                    break;
                case Constants.ResetUserPasswordButtonName:
                    this.mainContentControl.Content = new ResetUserPassword();
                    ToggleUserPrefsVisibility(false);
                    break;
                case Constants.LogFilesButtonName:
                    this.mainContentControl.Content = new LogFilesUserControl();
                    ToggleUserPrefsVisibility(false);
                    break;
            }
        }

        private void TitleBarClickHandler(object sender, MouseButtonEventArgs e)
        {
            Application.Current.MainWindow.DragMove();
        }
        #endregion
    }
}
