﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
#endregion

namespace Quiz
{
    public partial class CustomMessageBox : Window
    {
        #region Constructor
        private bool shutDown;
        public CustomMessageBox(String message)
        {
            InitializeComponent();
            messageTextBlock.Text = message;
        }

        public CustomMessageBox(String message, bool shutDown)
        {
            InitializeComponent();
            messageTextBlock.Text = message;
            this.shutDown = shutDown;
        }
        #endregion

        #region Handlers
        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (shutDown == true)
            {
                Application.Current.Shutdown();                                 //Close application
            }   
            else
            {
                this.Close();                                                   //Close window
            }
            
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                this.Close();
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        #endregion
    }
}
