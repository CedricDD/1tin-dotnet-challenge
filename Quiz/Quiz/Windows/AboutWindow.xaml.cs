﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 15/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Navigation;
using System.ComponentModel;
#endregion

namespace Quiz
{
    public partial class AboutWindow : Window
    {
        #region Constructor
        public AboutWindow()
        {
            InitializeComponent();
            InitializeWindow();
        }
        #endregion

        #region Methods
        private void InitializeWindow()                                                                         //Center the window depending on the resolution of the screen
        {
            double screenWidth = System.Windows.SystemParameters.PrimaryScreenWidth;
            double screenHeight = System.Windows.SystemParameters.PrimaryScreenHeight;
            double windowWidth = this.Width;
            double windowHeight = this.Height;
            this.Left = (screenWidth / 2) - (windowWidth / 2);
            this.Top = (screenHeight / 2) - (windowHeight / 2);
            emailImage.ToolTip = Constants.EmailUsToolTip;
        }
        #endregion

        #region Handlers
        private void TitleBarClickHandler(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Image_MouseEnter(object sender, MouseEventArgs e)
        {
            Image temp = (Image)sender;
            temp.Opacity = 0.5;
        }

        private void Image_MouseLeave(object sender, MouseEventArgs e)
        {
            Image temp = (Image)sender;
            temp.Opacity = 1;
        }

        private void closeClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void emailClick(object sender, MouseEventArgs e)
        {
            Process.Start(string.Format("mailto:{0}subject={1}", Constants.AboutEmailAddress, Constants.EmailSubject));
        }
        #endregion
    }
}
