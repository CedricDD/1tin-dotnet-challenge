﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 30/04/15 17:00
    //Author: Cédric De Dycker
    #endregion
    public class Randomizer
    {
        #region Properties
        private const int N = 624;
        private const int M = 398;
        private const uint MATRIX_A = 0x9908b0dfU;
        private const uint UPPER_MASK = 0x80000000U;
        private const uint LOWER_MASK = 0x7fffffffU;
        private const int MAX_RAND_INT = 0x7fffffff;
        private uint[] mag01 = { 0x0U, MATRIX_A };
        private uint[] mt = new uint[N];
        private int mti = N + 1;

        #endregion

        #region Constructors
        public Randomizer()
        {
            Initgenrand((uint)DateTime.Now.Millisecond);
        }
        public Randomizer(int seed)
        {
            Initgenrand((uint)seed);
        }
        #endregion

        #region Methods
        private void Initgenrand(uint s)
        {
            mt[0] = s & 0xffffffffU;
            for (mti = 1; mti < N; mti++)
            {
                mt[mti] = (uint)(1812433253U * (mt[mti - 1] ^ (mt[mti - 1] >> 30)) + mti);
                mt[mti] &= 0xffffffffU;
            }
        }

        public int Next()
        {
           return genrand_int31();
        }

        public int Next(int minValue, int maxValue)
        {
            if (minValue > maxValue)
            {
                int tmp = maxValue;
                maxValue = minValue;
                minValue = tmp;
            }
            return (int)(Math.Floor((maxValue - minValue + 1) * GenRandReal1() +
            minValue));

        }

        private int genrand_int31()
        { return (int)(GenRandN32()>>1); }

        double GenRandReal1()
        { return GenRandN32() * (1.0 / 4294967295.0); }

        uint GenRandN32()
{
    uint y;
    if (mti >= N)
    {
        int kk;
        if (mti == N+1)
        Initgenrand(5489U);
        for (kk=0;kk<N-M;kk++)
        {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1U];
        }
        for (;kk<N-1;kk++)
        {
            y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
            mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1U];
        }
        y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
        mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1U];
        mti = 0;
    }
    y = mt[mti++];
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680U;
    y ^= (y << 15) & 0xefc60000U;
    y ^= (y >> 18);
    return y;
}
    
        #endregion
    }
}
