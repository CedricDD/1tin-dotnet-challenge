﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 05/05/2015
 */ 
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Quiz
{
    
    public class UserResult
    {
        #region Properties
        private String questionDate;
        private String questionCourse;
        private String questionScore;
        private String question;
        #endregion

        #region Constructor
        public UserResult(String questionDate, String questionCourse, String questionScore, String question)
        {
            this.questionDate = questionDate;
            this.questionCourse = questionCourse;
            this.questionScore = questionScore;
            this.question = question;
        }
        #endregion

        #region Methods
        public override string ToString()
        {
            return String.Format("{0,-20}{1,-20}{2,-20}{3}", this.questionDate, this.questionCourse, this.questionScore, this.question);
        }

        public String GetQuestionDate
        {
            get
            {
                return this.questionDate;
            }
        }

        public String GetQuestionCourse
        {
            get
            {
                return this.questionCourse;
            }
        }

        public String GetQuestionScore
        {
            get
            {
                return this.questionScore;
            }
        }

        public String GetQuestion
        {
            get
            {
                return this.question;
            }
        }
        #endregion
    }
}
