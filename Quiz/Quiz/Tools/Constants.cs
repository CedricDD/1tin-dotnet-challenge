﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 11/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows;
using System.Drawing;
#endregion

namespace Quiz
{
    public static class Constants
    {
        #region Paths
        public const String coursesPath = "Resources\\Files\\courses";
        public const String MathPath = coursesPath + "\\math.txt";
        public const String LanguagePath = coursesPath + "\\language.txt";
        public const String KnowledgePath = coursesPath + "\\knowledge.txt";
        public const String LoginsPath = "Resources\\Files\\users\\logins";
        public const String ResultsPath = "Resources\\Files\\users\\results";
        public const String LogPath = "Resources\\Files\\log";
        #endregion

        #region Chars
        public const char MathCourse = 'M';
        public const char LanguageCourse = 'L';
        public const char KnowledgeCourse = 'K';
        public const char UserTypeStudent = 'S';
        public const char UserTypeTeacher = 'T';
        public const char UserTypeAdmin = 'A';
        public const char BarChar = '|';
        public const char SemiColon = ';';
        #endregion

        #region Cryptography Keys
        public const String AsymmetricPublicKey = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
        public const String AsymmetricPrivateKey = "<RSAKeyValue><Modulus>21wEnTU+mcD2w0Lfo1Gv4rtcSWsQJQTNa6gio05AOkV/Er9w3Y13Ddo5wGtjJ19402S71HUeN0vbKILLJdRSES5MHSdJPSVrOqdrll/vLXxDxWs/U0UT1c8u6k/Ogx9hTtZxYwoeYqdhDblof3E75d9n2F0Zvf6iTb4cI7j6fMs=</Modulus><Exponent>AQAB</Exponent><P>/aULPE6jd5IkwtWXmReyMUhmI/nfwfkQSyl7tsg2PKdpcxk4mpPZUdEQhHQLvE84w2DhTyYkPHCtq/mMKE3MHw==</P><Q>3WV46X9Arg2l9cxb67KVlNVXyCqc/w+LWt/tbhLJvV2xCF/0rWKPsBJ9MC6cquaqNPxWWEav8RAVbmmGrJt51Q==</Q><DP>8TuZFgBMpBoQcGUoS2goB4st6aVq1FcG0hVgHhUI0GMAfYFNPmbDV3cY2IBt8Oj/uYJYhyhlaj5YTqmGTYbATQ==</DP><DQ>FIoVbZQgrAUYIHWVEYi/187zFd7eMct/Yi7kGBImJStMATrluDAspGkStCWe4zwDDmdam1XzfKnBUzz3AYxrAQ==</DQ><InverseQ>QPU3Tmt8nznSgYZ+5jUo9E0SfjiTu435ihANiHqqjasaUNvOHKumqzuBZ8NRtkUhS6dsOEb8A2ODvy7KswUxyA==</InverseQ><D>cgoRoAUpSVfHMdYXW9nA3dfX75dIamZnwPtFHq80ttagbIe4ToYYCcyUz5NElhiNQSESgS5uCgNWqWXt5PnPu4XmCXx6utco1UVH8HGLahzbAnSy6Cj3iUIQ7Gj+9gQ7PkC434HTtHazmxVgIR5l56ZjoQ8yGNCPZnsdYEmhJWk=</D></RSAKeyValue>";
        public const String SymmetricPassword = "Quizzy";
        #endregion

        #region QuestionTypes
        public const char OneChoiceQuestionType = 'O';
        public const char MultipleChoiceQuestionType = 'M';
        public const char DragDropQuestionType = 'D';
        public const char TableQuestionType = 'T';
        #endregion

        #region Opacity
        public const double OpacityPointOne = 0.1;
        public const double OpacityFull = 1;
        #endregion

        #region Brushes (has to be static)
        public static SolidColorBrush WhiteBrush = new SolidColorBrush(Colors.White);
        public static SolidColorBrush BlackBrush = new SolidColorBrush(Colors.Black);
        public static SolidColorBrush RedBrush = new SolidColorBrush(Colors.Red);
        public static SolidColorBrush LightBlueBrush = new SolidColorBrush(Colors.LightBlue);
        public static SolidColorBrush LightGrayBrush = new SolidColorBrush(Colors.LightGray);
        public static SolidColorBrush LightGreenBrush = new SolidColorBrush(Colors.LightGreen);
        public static SolidColorBrush LightRedBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FF6A6A"));
        public static SolidColorBrush GrayTwoBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#222"));
        public static SolidColorBrush GrayThreeBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#333"));
        public static SolidColorBrush GrayFourBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#444"));
        public static SolidColorBrush GrayFiveBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#555"));
        public static SolidColorBrush GraySixBrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#666"));
        public static SolidColorBrush GrayABrush = (SolidColorBrush)(new BrushConverter().ConvertFrom("#AAA"));
        public static SolidColorBrush TransparantBrush = new SolidColorBrush(Colors.Transparent);
        public static SolidColorBrush HighlightBrush1 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#cde1ec"));
        public static SolidColorBrush HighlightBrush2 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#aebec7"));
        public static SolidColorBrush HighlightBrush3 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#909da4"));
        public static SolidColorBrush HighlightBrush4 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#707a80"));
        public static SolidColorBrush HighlightBrush5 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#4f565a"));
        public static SolidColorBrush DragDropAnswerBrush1 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#28a8ee"));
        public static SolidColorBrush DragDropAnswerBrush2 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#2089c3"));
        public static SolidColorBrush DragDropAnswerBrush3 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#1c6e9c"));
        public static SolidColorBrush DragDropAnswerBrush4 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#134f70"));
        public static SolidColorBrush DragDropAnswerBrush5 = (SolidColorBrush)(new BrushConverter().ConvertFrom("#0b2f42"));
        public static System.Drawing.Color ExcelBackgroundColor = ColorTranslator.FromHtml("#B7DEE8");
        #endregion

        #region Thickness 
        //(has to be static)
        public static Thickness ThicknessOne = new Thickness(1);
        public static Thickness ThicknessTwo = new Thickness(2);
        public static Thickness ThicknessThree = new Thickness(3);
        public static Thickness ThicknessFour = new Thickness(4);
        public static Thickness ThicknessFive = new Thickness(5);
        public static Thickness ThicknessTenBottom = new Thickness(0, 10, 0, 0);
        #endregion

        #region Strings
        public const String LengthWarningString = "Gelieve niet meer dan 25 tekens in te geven!";
        public const String DifficultyToolTipString = "De moeilijkheidsgraad beinvloedt de tijd die de gebruiker krijgt om de vraag op te lossen!";
        public const String DifficultyLabelString = "Moeilijkheidsgraad";
        public const String CourseLabelString = "Vak";
        public const String QuestionTypeLabelString = "Vraagtype";
        public const String QuestionLabelString = "Opgave";
        public const String CourseMathName = "Wiskunde";
        public const String CourseLanguageName = "Talen";
        public const String CourseKnowledgeName = "Kennis";
        public const String CourseMathRadioButtonName = "courseMathRadioButton";
        public const String CourseLanguageRadioButtonName = "courseLanguageRadioButton";
        public const String CourseKnowledgeRadioButtonName = "courseKnowledgeRadioButton";
        public const String CourseRadioButtonGroupname = "courseRadioButtons";
        public const String QuestionTypeRadioButtonGroupname = "questionTypeRadioButtons";
        public const String DifficultyRadioButtonGroupname = "difficultyRadioButtons";
        public const String QuestionTypeORadioButtonName = "questionTypeORadioButton";
        public const String QuestionTypeMRadioButtonName = "questionTypeMRadioButton";
        public const String QuestionTypeDRadioButtonName = "questionTypeDRadioButton";
        public const String QuestionTypeTRadioButtonName = "questionTypeTRadioButton";
        public const String Difficulty1RadioButtonName = "difficulty1RadioButton";
        public const String Difficulty2RadioButtonName = "difficulty2RadioButton";
        public const String Difficulty3RadioButtonName = "difficulty3RadioButton";
        public const String OneString = "1";
        public const String TwoString = "2";
        public const String ThreeString = "3";
        public const String QuestionTypeORadioButtonContent = "Enkele keuze";
        public const String QuestionTypeMRadioButtonContent = "Meerkeuze";
        public const String QuestionTypeDRadioButtonContent = "Drag en drop";
        public const String QuestionTypeTRadioButtonContent = "Vermenigvuldigingstabel";
        public const String AnswerLabelContent = "Antwoord";
        public const String Juist = "Juist";
        public const String Fout = "Fout";
        public const String RightAnswerText = "Juist antwoord:";
        public const String YourAnswerText = "Jouw antwoord:";
        public const String StellingText = "Stelling:";
        public const String OpgaveText = "Opgave: ";
        public const String MultipleAnswerALabelContent = "Antwoorden";
        public const String MultipleAnswerBLabelContent = Juist;
        public const String AnswerA1TextBoxName = "answerA1TextBox";
        public const String AnswerA2TextBoxName = "answerA2TextBox";
        public const String AnswerA3TextBoxName = "answerA3TextBox";
        public const String AnswerA4TextBoxName = "answerA4TextBox";
        public const String AnswerA5TextBoxName = "answerA5TextBox";
        public const String AnswerB1CheckBoxName = "answerB1CheckBox";
        public const String AnswerB2CheckBoxName = "answerB2CheckBox";
        public const String AnswerB3CheckBoxName = "answerB3CheckBox";
        public const String AnswerB4CheckBoxName = "answerB4CheckBox";
        public const String AnswerB5CheckBoxName = "answerB5CheckBox";
        public const String QuizAnswerB1CheckBoxName = "quizAnswerB1CheckBox";
        public const String QuizAnswerB2CheckBoxName = "quizAnswerB2CheckBox";
        public const String QuizAnswerB3CheckBoxName = "quizAnswerB3CheckBox";
        public const String QuizAnswerB4CheckBoxName = "quizAnswerB4CheckBox";
        public const String QuizAnswerB5CheckBoxName = "quizAnswerB5CheckBox";
        public const String AnswersOne = "Reeks 1";
        public const String AnswersTwo = "Reeks 2";
        public const String DropWarningLabelContent = "Zorg dat antwoorden links en rechts overeenkomen!";
        public const String AnswerB1TextBoxName = "answerB1TextBox";
        public const String AnswerB2TextBoxName = "answerB2TextBox";
        public const String AnswerB3TextBoxName = "answerB3TextBox";
        public const String AnswerB4TextBoxName = "answerB4TextBox";
        public const String AnswerB5TextBoxName = "answerB5TextBox";
        public const String QuizAnswerB1LabelName = "quizAnswerB1Label";
        public const String QuizAnswerB2LabelName = "quizAnswerB2Label";
        public const String QuizAnswerB3LabelName = "quizAnswerB3Label";
        public const String QuizAnswerB4LabelName = "quizAnswerB4Label";
        public const String QuizAnswerB5LabelName = "quizAnswerB5Label";
        public const String TableAnswerLabelContent = "Tafelgetal";
        public const String QuizTableAnswer1TextBox = "QuizTableAnswer1TextBox";
        public const String QuizTableAnswer2TextBox = "QuizTableAnswer2TextBox";
        public const String QuizTableAnswer3TextBox = "QuizTableAnswer3TextBox";
        public const String QuizTableAnswer4TextBox = "QuizTableAnswer4TextBox";
        public const String QuizTableAnswer5TextBox = "QuizTableAnswer5TextBox";
        public const String QuizTableAnswer6TextBox = "QuizTableAnswer6TextBox";
        public const String QuizTableAnswer7TextBox = "QuizTableAnswer7TextBox";
        public const String QuizTableAnswer8TextBox = "QuizTableAnswer8TextBox";
        public const String QuizTableAnswer9TextBox = "QuizTableAnswer9TextBox";
        public const String QuizTableAnswer10TextBox = "QuizTableAnswer10TextBox";
        public const String MessageIfNoQuestionsQuiz = "Er zitten te weinig vragen in de database om een quiz af te werken!";
        public const String MessageIfNumberValueNotCorrect = "Gelieve enkel gehele getallen van 1 tot en met 100 in te geven";
        public const String MessageIfUserDoesNotExit = "Deze gebruiker bestaat niet!";
        public const String MessageIfPasswordIncorrect = "Het paswoord is incorrect!";
        public const String LoggedInAsText = "Ingelogd als ";
        public const String UserTypeStudentText = "Student";
        public const String UserTypeTeacherText = "Leerkracht";
        public const String UserTypeAdminText = "Admin";
        public const String MessagePasswordNotRightFormat = "Het wachtwoord moet bestaan uit minstens 8 karakters, maximum 25 karakters, waaronder een hoofdletter!";
        public const String MessageUserExists = "Deze login naam bestaat al";
        public const String AdminLoginName = "admin";
        public const String AdminFirstName = "Systeem";
        public const String AdminLastName = "Beheerder";
        public const String AdminGroup = "Admins";
        public const String AdminPassword = "admin";
        public const String ReportLoginNameLabelDesc = "Loginnaam";
        public const String ReportFirstNameLabelDesc = "Voornaam";
        public const String ReportLastNameLabelDesc = "Achternaam";
        public const String ReportGroupLabelDesc = "Klas";
        public const String ReportQuizScoreLabelDesc = "Quiz score";
        public const String ReportGameScoreLabelDesc = "Game high score";
        public const String MessageIfFirstNameEmpty = "De voornaam mag niet leeg zijn!";
        public const String MessageIfLastNameEmpty = "De achternaam mag niet leeg zijn!";
        public const String MessageIfPasswordEmpty = "Het paswoord mag niet leeg zijn!";
        public const String DotTXT = ".txt";
        public const String StarDotTXT = "*.txt";
        public const String FolderBrowseDialogDesc = "Kies een map";
        public const String ExportButtonToolTip = "Exporteer de gebruikersgegevens";
        public const String CourseChoiceMathButtonToolTip = "Open de Wiskunde vragenlijst";
        public const String CourseChoiceLanguageButtonToolTip = "Open de Taal vragenlijst";
        public const String CourseChoiceKnowledgeButtonToolTip = "Open de Kennis vragenlijst";
        public const String SaveButtonToolTip = "Sla de wijzigingen op";
        public const String AddButtonToolTip = "Vroeg een vraag toe";
        public const String DeleteButtonToolTip = "Verwijder een vraag";
        public const String BackupButtonToolTip = "Backup alle vragen";
        public const String LogOffButtonToolTip = "Klik hier om uit te loggen";
        public const String QuitButtonToolTip = "Klik hier om de applicatie af te sluiten";
        public const String MathQuizButtonToolTip = "Open de Wiskunde Quiz";
        public const String LanguageQuizButtonToolTip = "Open de Talen Quiz";
        public const String KnowledgeQuizButtonToolTip = "Open de Kennis Quiz";
        public const String ManageQuestionsButtonToolTip = "Open het Vragenbeheer";
        public const String ReportButtonToolTip = "Open de rapportage";
        public const String AboutButtonToolTip = "Over ons";
        public const String EmailUsToolTip = "Klik hier om ons een email te versturen!";
        public const String TitleBarToolTip = "Neem de titelbalk vast om het venster te verschuiven";
        public const String UserPreferencesButtonToolTip = "Klik hier voor de gebruikersinstellingen";
        public const String MinimizeButtonToolTip = "Klik hier om de applicatie te minimaliseren";
        public const String StartGameButtonToolTip = "Klik hier om de game te starten";
        public const String BarString = "|";
        public const String NextButtonName = "nextButton";
        public const String StartQuizButtonName = "startQuizButton";
        public const String CancelQuizButtonName = "cancelQuizButton";
        public const String ResultButtonName = "resultButton";
        public const String QuitButtonName = "quitButton";
        public const String changeFirstNameGridName = "changeFirstNameGrid";
        public const String changeLastNameGridName = "changeLastNameGrid";
        public const String changePasswordGridName = "changePasswordGrid";
        public const String MessageIfNoResultsAvailable = "Er zijn geen resultaten beschikbaar voor deze gebruiker!";
        public const String MessageIfNoExcelInstalled = "Er is geen MS Excel geinstalleerd op deze computer!";
        public const String MessageIfFileIsLocked = "Het bestand is in gebruik, de resultaten zijn daarom niet weg geschreven!";
        public const String ExcelFileTypeFilter = "Excel Files (*.xlsx|*.xlsx|All files (*.*)|*.*";
        public const String ExcelApplication = "Excel.Application";
        public const String ExcelRangeRow1 = "A1:E1";
        public const String ExcelRangeRow4 = "A4:E4";
        public const String ExcelRangeResultColumn1 = "A5:A5000";
        public const String ExcelRangeResultColumn2 = "B5:B000";
        public const String ExcelRangeResultColumn3 = "C5:C5000";
        public const String ExcelRangeResultColumn4 = "D5:D5000";
        public const String ExcelColumns = "A:E";
        public const String ExcelColumnD = "D:D";
        public const String UserResultsDate = "Datum";
        public const String UserResultsCourse = "Vak";
        public const String UserResultsScore = "Vraagscore";
        public const String UserResultsQuestion = "Vraag";
        public const String SplashQuizLabelContent = "Ben je zeker dat je de quiz wil starten?";
        public const String MessageIfFileCorrupt = "Er is een probleem met de database, gelieve de administrator te verwittigen!";
        public const String FileCorruptText = "Corrupt bestand: ";
        public const String IndicatorName = "Indicator";
        public const String FailedShipRemove = "Fout bij verwijdering schip:";
        public const String RawTXTToolTip = "Klik hier om het log bestand van de geselecteerde datum te openen";
        public const String AboutEmailAddress = "galacticquizzy@gmail.com";
        public const String EmailSubject = "Quizzy Application Help";
        public const String ExcelProgress10 = "Excel bestand aanmaken";
        public const String ExcelProgress20 = "Gebruikersinformatie wegschrijven";
        public const String ExcelProgress40 = "Resultaten wegschrijven";
        public const String ExcelProgress60 = "Opmaak verzorgen";
        public const String ExcelProgress80 = "Excel bestand opslaan";
        public const String ExcelProgress90 = "Excel bestand succesvol gegenereerd!";
        public const String FailedShipMargin = "Ships have been reset. X en Y are set as Auto.";
        public const String DateFormatDash = "dd-MM-yyyy";
        public const String DateFormatSlash = "DD/MM/yyyy";
        public const String TimeFormat = "HH:mm:ss";
        public const String TextFormat = "@";
        public const String NumberFormat = "0";
        public const String DatesListBoxName = "datesListBox";
        public const String LoginButtonName = "loginButton";
        public const String RegisterButtonName = "registerButton";
        public const String LoginRadioButtonName = "loginRadioButton";
        public const String RegisterRadioButtonName = "registerRadioButton";
        public const String Space = " ";
        public const String IndicatorString = "Indicator";
        public const String RestOfStringMultipleChoice = ";;;;;False;False;False;False;False";
        public const String RestOfStringDragDrop = ";;;;;;;;;";
        public const String RestOfStringTableQ = "1";
        public const String CourseChoiceMathButtonName = "courseChoiceMathButton";
        public const String CourseChoiceLanguageButtonName = "courseChoiceLanguageButton";
        public const String CourseChoiceKnowledgeButtonName = "courseChoiceKnowledgeButton";
        public const String DeleteButtonName = "deleteButton";
        public const String AddButtonName = "addButton";
        public const String SaveButtonName = "saveButton";
        public const String BackupButtonName = "backupButton";
        public const String DefaultMathQuestion = "M;1;O;;";
        public const String DefaultLanguageQuestion = "L;1;O;;";
        public const String DefaultKnowledgeQuestion = "K;1;O;;";
        public const String SortContent = "Content";
        public const String YourScoreString = "Jouw Score:";
        public const String ExplorerEXE = "explorer.exe";
        public const String ExplorerSelect = "/select,";
        public const String MathQuizButtonName = "mathQuizButton";
        public const String LanguageQuizButtonName = "languageQuizButton";
        public const String KnowledgeQuizButtonName = "knowledgeQuizButton";
        public const String ManageQuestionsButtonName = "manageQuestionsButton";
        public const String ReportButtonName = "reportButton";
        public const String StartGameButtonName = "startGameButton";
        public const String LogOffButtonName = "logOffButton";
        public const String UserPrefsButtonName = "userPreferencesButton";
        public const String ChangeFirstNameButtonName = "changeFirstNameButton";
        public const String ChangeLastNameButtonName = "changeLastNameButton";
        public const String ChangePasswordButtonName = "changePasswordButton";
        public const String ExportUserReportButtonName = "exportUserReportButton";
        public const String AddTeacherButtonName = "addTeacherButton";
        public const String MinimizeButtonName = "minimizeButton";
        public const String ResetUserPasswordButtonName = "resetUserPasswordButton";
        public const String LogFilesButtonName = "logFilesButton";
        public const String LoginNameText = "Loginnaam:";
        public const String GroupText = "Klas:";
        public const String QuizScoreText = "Quiz score:";
        public const String GameHighScoreText = "Game High Score:";
       
        #endregion

        #region Doubles
        public const double OnePointSeven = 1.7;
        #endregion

        #region Ints
        public const int PointPerCorrectAnswer = 10;
        public const int QuestionTimeLimit = 180;
        #endregion

        #region Comments
        //Date: 4/05/15 21:00
        //Author: Cédric De Dycker
        #endregion

        #region Game
        public enum ObjectState
        {
            Alive, Hit, Dead
        }
        public static int ScreenHeight = 650;
        public static int ScreenWidth = 800;
        public const int ObjectSize = 48;
        public static string MenuBackgroundPath;
        public const string imageExtension = ".png";
        public static int fps = 60;
        public const double frictionCoefficient = 0.97;
        public const string BackgroundBasePath = "Resources/Images/Game/Backgrounds/bg";
        public const string playerBasePath = "/Images/Game/Sprites/PlayerSprite";
        public const string enemyBasePath = "/Images/Game/Sprites/EnemySprite";
        public static string playerSpritePath;
        public static string enemySpritePath;
        public const int MaxSpeed = 25;
        public static string GameBackgroundPath;
        public static int ShipAmount = 5;
        public static int SelectedShip;
        public static int SelectedEnemy;
        public static int SelectedMenuBackground = 4;
        public static int SelectedGameBackground = 3;
        public const int AdminPlayTime = 3600;
        public const int SpriteCount = 2;
        #endregion
    }
}
