﻿#region Class Comment
/*
 * Created by Cedric De Dycker - 1TINL
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
#endregion

namespace Quiz
{
    #region Comments
    //Date: 28/04/15 13:00
    //Author: Cédric De Dycker
    #endregion
    public class ImageManager
    {
        #region Constructor
        public static BitmapImage GetBitMapImage(String path, int width, int height)
        {
            BitmapImage sprite = new BitmapImage();
            sprite.BeginInit();
            sprite.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
            sprite.DecodePixelWidth = width;
            sprite.DecodePixelHeight = height;
            sprite.EndInit();
            return sprite;
        }
        
        public static BitmapImage GetBitMapImage(String path)
        {
            BitmapImage sprite = new BitmapImage();
            sprite.BeginInit();
            sprite.UriSource = new Uri(path, UriKind.RelativeOrAbsolute);
            sprite.EndInit();
            return sprite;
        }
        #endregion
    }
}
