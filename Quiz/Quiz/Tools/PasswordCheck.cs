﻿#region Class Comment
/*
 * Created by Hans Habraken - 1TINL
 * 28/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
#endregion

namespace Quiz
{
    public static class PasswordCheck
    {
        #region Methods
        public static bool CheckPassWordFormat(String passWord)
        {
            String s = passWord;
            Match password = Regex.Match(s, @"
                                ^                   # Match the start of the string   
                                (?=.*\p{Lu})        # Positive lookahead assertion, is true when there is an uppercase letter
                                \S{8,25}            # Minimum 8, Maximum 25 non whitespace characters
                                $                   # Match the end of the string
                                ", RegexOptions.IgnorePatternWhitespace);
            if (password.Success)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ComparePasswords(String password, String passwordRepeat)
        {
            if (password.Equals(passwordRepeat))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
    }
}
