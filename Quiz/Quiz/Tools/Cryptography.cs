﻿#region Class Comment
/*
 * Created by Hans Habraken - 1TINL
 * 20/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using System.Windows;
using System.Windows.Markup;
#endregion

namespace Quiz
{
    public static class Cryptography
    {
        #region Methods
        public static String AsymmetricEncrypt(String data, String asymetricPublicKey)
        {

            var dataByte = Encoding.UTF8.GetBytes(data);
            var rsa = new RSACryptoServiceProvider(1024);
            rsa.FromXmlString(asymetricPublicKey);
            var encryptedByte = rsa.Encrypt(dataByte, true);
            String encryptedData = Convert.ToBase64String(encryptedByte);

            return encryptedData;

        }

        public static String AsymmetricDecrypt(string encryptedData, String asymetricPrivateKey, String path)
        {
            try
            {
                var encrypteddataByte = Convert.FromBase64String(encryptedData);
                var rsa = new RSACryptoServiceProvider(1024);
                rsa.FromXmlString(asymetricPrivateKey);
                var deCryptedByte = rsa.Decrypt(encrypteddataByte, true);
                String deCryptedData = Encoding.UTF8.GetString(deCryptedByte);
                return deCryptedData;
            }
            catch (Exception)   //System.Windows.Markup.XamlParseException or FormatException
            {
                String exMessage = String.Concat(Constants.MessageIfFileCorrupt, Environment.NewLine, Environment.NewLine, Constants.FileCorruptText, Environment.NewLine, Path.GetFullPath(path));
                FileCorruptException temp = new FileCorruptException(exMessage);
                temp.SetFilePath(Path.GetFullPath(path));
                throw temp;
            }
        }

        public static String SymmetricEncrypt(String data, String key)
        {

            byte[] dataBytes = System.Text.Encoding.Unicode.GetBytes(data);
            PasswordDeriveBytes pdb = new PasswordDeriveBytes(key, new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 
            0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
            byte[] encryptedData = SymmetricEncrypt(dataBytes, pdb.GetBytes(32), pdb.GetBytes(16));
            return Convert.ToBase64String(encryptedData);
        }

        public static byte[] SymmetricEncrypt(byte[] dataBytes, byte[] key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(dataBytes, 0, dataBytes.Length);
            cs.Close();
            byte[] encryptedData = ms.ToArray();
            return encryptedData;

        }

        public static String SymmetricDecrypt(String encryptedData, String key, String path)
        {
            try
            {
                byte[] encryptedBytes = Convert.FromBase64String(encryptedData);
                PasswordDeriveBytes pdb = new PasswordDeriveBytes(key,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 
            0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
                byte[] decryptedData = SymmetricDecrypt(encryptedBytes, pdb.GetBytes(32), pdb.GetBytes(16));
                return System.Text.Encoding.Unicode.GetString(decryptedData);
            }
            catch (FormatException)
            {
                String exMessage = String.Concat(Constants.MessageIfFileCorrupt, Environment.NewLine, Environment.NewLine, Constants.FileCorruptText, Environment.NewLine, Path.GetFullPath(path));
                FileCorruptException temp = new FileCorruptException(exMessage);
                temp.SetFilePath(Path.GetFullPath(path));
                throw temp;
            }
        }

        public static byte[] SymmetricDecrypt(byte[] encryptedBytes, byte[] key, byte[] IV)
        {
            MemoryStream ms = new MemoryStream();
            Rijndael alg = Rijndael.Create();
            alg.Key = key;
            alg.IV = IV;
            CryptoStream cs = new CryptoStream(ms, alg.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(encryptedBytes, 0, encryptedBytes.Length);
            cs.Close();
            byte[] decryptedData = ms.ToArray();
            return decryptedData;
        }
        #endregion
    }
}
