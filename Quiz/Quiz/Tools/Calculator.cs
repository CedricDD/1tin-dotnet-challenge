﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 17/04/15 14:00
    //Author: Cédric De Dycker
    #endregion
    public class Calculator
    {
        #region Game
        public static double AngleTwoPoints(double x1, double y1, double x2, double y2)
        {
            double dx = x2 - x1;
            double dy = y2 - y1;
            double angle = (Math.Atan2(dy, dx) * 180 / Math.PI);
            return angle;
        }
        #endregion
    }
}
