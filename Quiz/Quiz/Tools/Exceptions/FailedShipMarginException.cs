﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 3/05/15 19:30
    //Author: Cédric De Dycker
    #endregion
    public class FailedShipMarginException : Exception
    {
        public FailedShipMarginException(String Message)
        :base(Message)
        {

        }
    }
}
