﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 3/05/15 21:00
    //Author: Cédric De Dycker
    #endregion
    public class FailedShipRemove: Exception
    {
        public FailedShipRemove(String message)
            : base(message)
        {
            
        }
    }
}
