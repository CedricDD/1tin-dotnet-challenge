﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    public class FileCorruptException: Exception
    {
        private String filePath;
        public FileCorruptException(String message)
            : base(message)
        {

        }

        public void SetFilePath(String filePath)
        {
            this.filePath = filePath;
        }

        public String GetFilePath()
        {
            return this.filePath;
        }
    }
}
