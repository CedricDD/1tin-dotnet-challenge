﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 28/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;
using System.Windows;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Diagnostics;
#endregion

namespace Quiz
{
   public static class ExcelExport
    {
        #region Properties
       private static ExportMessageBox waitWindow;
        private static DispatcherTimer waitTimer;
        private static int counter = 0;
        private static String fullPath;
        private static Student tempStudent;
        private static Excel.Application excelApplication;
        private static Excel.Workbook excelWorkBook;
        private static Excel.Worksheet excelWorkSheet;
        private static List<UserResult> resultsList;
        private static Object misValue;
        #endregion

        #region Public Methods
        public static void GenerateUserReport(Student student)
        {
            tempStudent = student;
            Type officeType = Type.GetTypeFromProgID(Constants.ExcelApplication);
            if (officeType == null)
            {
                new CustomMessageBox(Constants.MessageIfNoExcelInstalled).Show();
                FileManager.WriteToLogFile(Constants.MessageIfNoExcelInstalled);
            }
            else
            {
                SaveFileDialog saveFile = new SaveFileDialog();
                saveFile.Filter = Constants.ExcelFileTypeFilter;
                if (saveFile.ShowDialog() == true)
                {
                    fullPath = saveFile.FileName;
                    CreateTimer();
                }
            }
        }
        #endregion

        #region Handlers
        private static void waitTimer_Tick(object sender, EventArgs e)
        {
            counter++;
            switch (counter)
            {
                case 10:
                    CreateWorkBook();
                    waitWindow.progressLabel.Content = Constants.ExcelProgress10;
                    break;
                case 20:
                    SetContentHeaders();
                    waitWindow.progressLabel.Content = Constants.ExcelProgress20;
                    break;
                case 40:
                    SetContent();
                    waitWindow.progressLabel.Content = Constants.ExcelProgress40;
                    break;
                case 60:
                    SetLayout();
                    waitWindow.progressLabel.Content = Constants.ExcelProgress60;
                    break;
                case 80:
                    SaveFile();
                    waitWindow.progressLabel.Content = Constants.ExcelProgress80;
                    break;
                case 90:
                    QuitFile();
                    break;
                case 96:
                    Finished();
                    break;
            }
            waitWindow.waitProgressBar.Value = counter;
        }
        #endregion

        #region Private Methods

        private static void Finished()
        {
            waitTimer.Stop();
            waitWindow.SetVisibilityOKButton();
            waitWindow.okButton.Visibility = Visibility.Visible;
        }

        private static void QuitFile()
        {
            //Quit file
            excelApplication.Quit();
            waitWindow.progressLabel.Content = Constants.ExcelProgress90;
            //Release objects
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelWorkSheet);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelWorkBook);
            System.Runtime.InteropServices.Marshal.ReleaseComObject(excelApplication);
        }

        private static void SaveFile()
        {
            //Save file
            excelWorkBook.SaveAs(fullPath, Excel.XlFileFormat.xlOpenXMLWorkbook, misValue, misValue, false, false, Excel.XlSaveAsAccessMode.xlNoChange, Excel.XlSaveConflictResolution.xlUserResolution, true, misValue, misValue, misValue);
            excelWorkBook.Close(true, misValue, misValue);
        }

        private static void SetLayout()
        {
            //Set layout
            excelWorkSheet.Range[Constants.ExcelRangeRow1].Interior.Color = Constants.ExcelBackgroundColor;
            excelWorkSheet.Range[Constants.ExcelRangeRow1].Font.Bold = true;
            excelWorkSheet.Range[Constants.ExcelRangeRow4].Interior.Color = Constants.ExcelBackgroundColor;
            excelWorkSheet.Range[Constants.ExcelRangeRow4].Font.Bold = true;
            (excelWorkSheet.get_Range(Constants.ExcelColumns, System.Type.Missing)).EntireColumn.ColumnWidth = 35;
            (excelWorkSheet.get_Range(Constants.ExcelColumnD, System.Type.Missing)).EntireColumn.ColumnWidth = 55;
            excelWorkSheet.UsedRange.Columns.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
        }

        private static void SetContent()
        {
            //Set content of content-cells
            if (resultsList.Count == 0)
            {
                excelApplication.Cells[5, 1] = Constants.MessageIfNoResultsAvailable;
            }
            else
            {
                for (int i = 0; i < resultsList.Count; i++)
                {
                    UserResult tempResult = resultsList[i];
                    excelApplication.Cells[i + 5, 1] = tempResult.GetQuestionDate;
                    excelApplication.Cells[i + 5, 2] = tempResult.GetQuestionCourse;
                    excelApplication.Cells[i + 5, 2].NumberFormat = Constants.TextFormat;
                    excelApplication.Cells[i + 5, 3] = tempResult.GetQuestionScore;
                    excelApplication.Cells[i + 5, 3].NumberFormat = Constants.NumberFormat;
                    excelApplication.Cells[i + 5, 4] = tempResult.GetQuestion;
                    excelApplication.Cells[i + 5, 4].NumberFormat = Constants.TextFormat;
                    excelWorkSheet.Range[excelWorkSheet.Cells[i + 5, 4], excelWorkSheet.Cells[i + 5, 5]].Merge();
                }
            }
        }

        private static void SetContentHeaders()
        {
            //Set content of header-cells
            excelApplication.Cells[1, 1] = Constants.ReportFirstNameLabelDesc;
            excelApplication.Cells[1, 2] = Constants.ReportLastNameLabelDesc;
            excelApplication.Cells[1, 3] = Constants.ReportGroupLabelDesc;
            excelApplication.Cells[1, 4] = Constants.ReportQuizScoreLabelDesc;
            excelApplication.Cells[1, 5] = Constants.ReportGameScoreLabelDesc;
            excelApplication.Cells[2, 1] = tempStudent.GetFirstName;
            excelApplication.Cells[2, 2] = tempStudent.GetLastName;
            excelApplication.Cells[2, 3] = tempStudent.GetGroup;
            excelApplication.Cells[2, 4] = tempStudent.GetQuizScore.ToString();
            excelApplication.Cells[2, 5] = tempStudent.GetGameScore.ToString();
            excelApplication.Cells[4, 1] = Constants.UserResultsDate;
            excelApplication.Cells[4, 2] = Constants.UserResultsCourse;
            excelApplication.Cells[4, 3] = Constants.UserResultsScore;
            excelApplication.Cells[4, 4] = Constants.UserResultsQuestion;
            excelWorkSheet.Range[excelWorkSheet.Cells[4, 4], excelWorkSheet.Cells[4, 5]].Merge();
        }

        private static void CreateWorkBook()
        {
            //Create App, WorkBook and WorkSheet
            resultsList = FileManager.ReadUserResultsFile(tempStudent);
            excelApplication = new Microsoft.Office.Interop.Excel.Application();
            misValue = System.Reflection.Missing.Value;
            excelWorkBook = excelApplication.Workbooks.Add(misValue);
            excelWorkSheet = (Excel.Worksheet)excelWorkBook.Worksheets.get_Item(1);
        }

        private static void CreateTimer()
        {
            //Create timer and progress window
            waitWindow = new ExportMessageBox(fullPath);
            waitTimer = new DispatcherTimer();
            waitTimer.Interval = TimeSpan.FromMilliseconds(25);
            waitTimer.Tick += waitTimer_Tick;
            waitWindow.Show();
            waitTimer.Start();
        }
        #endregion
    }
}
