﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 04/04/2015
 * 
 * Made Static by Cedric De Dycker - 1TINL
 * 30/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Collections.ObjectModel;
using System.Security;
using System.Security.Cryptography;
using System.Runtime.InteropServices;
using System.Windows;
#endregion

namespace Quiz
{
    public static class FileManager
    {
        #region Questions
        public static void CheckCourseFile(char course, ObservableCollection<Question> allQuestions)
        {
            String path = String.Empty;
            switch (course)
            {
                case Constants.MathCourse:
                    path = Constants.MathPath;
                    break;
                case Constants.LanguageCourse:
                    path = Constants.LanguagePath;
                    break;
                case Constants.KnowledgeCourse:
                    path = Constants.KnowledgePath;
                    break;
            }
            if (Directory.Exists(Constants.coursesPath))
            {
                if (File.Exists(path))
                {
                    if (new FileInfo(path).Length != 0)
                    {
                        ReadCourseFile(path, allQuestions);
                    }
                }
                else
                {
                    WriteEmptyCourseFile(path);
                }
            }
            else
            {
                Directory.CreateDirectory(Constants.coursesPath);
                WriteEmptyCourseFile(path);
            }
        }

        private static void WriteEmptyCourseFile(String path)
        {
            using (StreamWriter tempWriter = new StreamWriter(path))
            {
                tempWriter.Write(String.Empty);
            }
        }

        public static void ReadCourseFile(String path, ObservableCollection<Question> allQuestions)
        {
            String line;
            using (StreamReader tempReader = new StreamReader(path))
            {
                while ((line = tempReader.ReadLine()) != null)
                {
                    allQuestions.Add(CreateSubQuestion(Cryptography.SymmetricDecrypt(line, Constants.SymmetricPassword, path)));
                }
            }
        }

        public static void WriteCourseFile(String path, ObservableCollection<Question> collection)
        {
            File.Delete(path);
            using (StreamWriter tempWriter = File.CreateText(path))
            {
                foreach (Question temp in collection)
                {
                    tempWriter.WriteLine(Cryptography.SymmetricEncrypt(temp.ToFile(), Constants.SymmetricPassword));
                }
            }
        }

        public static void BackupAllCourseFiles(String folder, ManageQuestionsUserControl manageInstance)
        {
            String fullFolderPath = System.IO.Path.Combine(folder, Constants.coursesPath);
            if (!Directory.Exists(fullFolderPath))
            {
                Directory.CreateDirectory(fullFolderPath);
            }       
            String courseFile = System.IO.Path.Combine(folder, Constants.MathPath);
            WriteCourseFile(courseFile, manageInstance.UpdateCourseList(Constants.MathCourse));
            courseFile = System.IO.Path.Combine(folder, Constants.LanguagePath);
            WriteCourseFile(courseFile, manageInstance.UpdateCourseList(Constants.LanguageCourse));
            courseFile = System.IO.Path.Combine(folder, Constants.KnowledgePath);
            WriteCourseFile(courseFile, manageInstance.UpdateCourseList(Constants.KnowledgeCourse));
        }

        public static Question CreateSubQuestion(String line)
        {
            Question temp = new OneChoice(Constants.MathCourse, 1, Constants.OneChoiceQuestionType, String.Empty, String.Empty);
            String[] properties = line.Split(Constants.SemiColon);
            char course = Convert.ToChar(properties[0]);
            int difficulty = Convert.ToInt32(properties[1]);
            char questionType = Convert.ToChar(properties[2]);
            String question = properties[3];
            switch (questionType)
            {
                case Constants.OneChoiceQuestionType:
                    temp = new OneChoice(course, difficulty, questionType, question, properties[4]);
                    break;
                case Constants.MultipleChoiceQuestionType:
                    String[] possibleAnswers = new String[5];
                    bool[] correctAnswers = new bool[5];
                    for (int i = 0; i <= 4; i++)
                    {
                        possibleAnswers[i] = properties[i + 4];
                        correctAnswers[i] = Convert.ToBoolean(properties[i + 9]);
                    }
                    temp = new MultipleChoice(course, difficulty, questionType, question, possibleAnswers, correctAnswers);
                    break;
                case Constants.DragDropQuestionType:
                    String[] dragAnswers = new String[5];
                    String[] dropAnswers = new String[5];
                    for (int i = 0; i <= 4; i++)
                    {
                        dragAnswers[i] = properties[i + 4];
                        dropAnswers[i] = properties[i + 9];
                    }
                    temp = new DragNDrop(course, difficulty, questionType, question, dragAnswers, dropAnswers);
                    break;
                case Constants.TableQuestionType:
                    temp = new TableQuestion(course, difficulty, questionType, question, Convert.ToInt32(properties[4]));
                    break;
            }
            return temp;
        }
        #endregion

        #region Users

        public static void CheckUserFiles(List<User> userList)
        {
            if (Directory.Exists(Constants.LoginsPath))
            {
                ReadUserFiles(userList);
            }
        }

        public static List<User> ReadUserFiles(List<User> userList)
        {
            foreach (String file in Directory.EnumerateFiles(Constants.LoginsPath, Constants.StarDotTXT))
            {
                using (StreamReader tempReader = new StreamReader(file))
                {
                    String line = tempReader.ReadLine();
                    String deCryptedData = Cryptography.AsymmetricDecrypt(line, Constants.AsymmetricPrivateKey, file);
                    userList.Add(CreateSubUserObject(deCryptedData));
                }
            }
            return userList;
        }

        public static void WriteUserFiles(List<User> userList)                      //Writes all users to the correct files
        {
            foreach (User tempUser in userList)
            {
                if (!(userList.IndexOf(tempUser) == 0))
                {
                    WriteUserFile(tempUser);
                }
            }
        }

        public static void WriteUserFile(User tempUser)                            //Writes one user to the correct file
        {
            String fileName = String.Format("{0}{1}", tempUser.GetLoginName, Constants.DotTXT);
            String userPath = System.IO.Path.Combine(Constants.LoginsPath, fileName);
            String userContent = tempUser.ToFile();
            if (!Directory.Exists(Constants.LoginsPath))
            {
                Directory.CreateDirectory(Constants.LoginsPath);
            }
            if (File.Exists(userPath))
            {
                File.Delete(userPath);
            }
            using (StreamWriter tempWriter = File.CreateText(userPath))
            {
                String cryptedData = Cryptography.AsymmetricEncrypt(userContent, Constants.AsymmetricPublicKey);
                tempWriter.WriteLine(cryptedData);
            }

        }

        public static User CreateSubUserObject(String line)                             //Returns a user object based on an input String
        {
            User tempUser;
            String[] properties = line.Split(Constants.SemiColon);
            if (properties[0].Equals(Convert.ToString(Constants.UserTypeStudent)))
            {
                tempUser = new Student(properties[1], properties[2], properties[3], properties[4], properties[5], Convert.ToInt32(properties[6]), Convert.ToInt32(properties[7]));
            }
            else
            {
                tempUser = new Teacher(properties[1], properties[2], properties[3], properties[4], properties[5]);
            }
            return tempUser;
        }
        #endregion

        #region Result

        public static List<UserResult> ReadUserResultsFile(User tempUser)
        {
            List<UserResult> resultsList = new List<UserResult>();
            String fileName = String.Format("{0}{1}", tempUser.GetLoginName, Constants.DotTXT);
            String userPath = System.IO.Path.Combine(Constants.ResultsPath, fileName);
            String line;
            if (Directory.Exists(Constants.ResultsPath))
            {
                if (File.Exists(userPath))
                {
                    using (StreamReader tempReader = new StreamReader(userPath))
                    {
                        while ((line = tempReader.ReadLine()) != null)
                        {
                            String decryptedData = Cryptography.SymmetricDecrypt(line, Constants.SymmetricPassword, userPath);
                            String[] properties = decryptedData.Split(';');
                            String courseName = CheckCourse(properties[1]);
                            resultsList.Add(new UserResult(properties[0], courseName,properties[2],properties[3]));
                        }
                    }
                }
            }
            return resultsList;
        }

        private static String CheckCourse(String courseString)
        {
            String courseName = String.Empty;
            switch (Convert.ToChar(courseString))
            {
                case Constants.MathCourse:
                    courseName = Constants.CourseMathName;
                    break;
                case Constants.LanguageCourse:
                    courseName = Constants.CourseLanguageName;
                    break;
                case Constants.KnowledgeCourse:
                    courseName = Constants.CourseKnowledgeName;
                    break;
            }
            return courseName;
        }

        public static void WriteUserResultsFile(String resultLine, User tempUser)
        {
            StreamWriter tempWriter;
            String fileName = String.Format("{0}{1}", tempUser.GetLoginName, Constants.DotTXT);
            String userPath = System.IO.Path.Combine(Constants.ResultsPath, fileName);
            if (!Directory.Exists(Constants.ResultsPath))
            {
                Directory.CreateDirectory(Constants.ResultsPath);
            }
            tempWriter = new StreamWriter(userPath, true);
            String cryptedData = Cryptography.SymmetricEncrypt(resultLine, Constants.SymmetricPassword);
            tempWriter.WriteLine(cryptedData);
            tempWriter.Close();
        }
        #endregion

        #region Log
        public static void WriteToLogFile(String message)
        {
            String dateToday = DateTime.Now.ToString(Constants.DateFormatDash);
            String timeToday = DateTime.Now.ToString(Constants.TimeFormat);
            String logFileName = String.Concat(dateToday, Constants.DotTXT);
            String logPath = System.IO.Path.Combine(Constants.LogPath, logFileName);
            if (!Directory.Exists(Constants.LogPath)){
                Directory.CreateDirectory(Constants.LogPath);
            }
            using (StreamWriter wTemp = new StreamWriter(logPath, true))
            {
                wTemp.WriteLine(String.Format("{0}{1}{2}", dateToday.PadRight(15), timeToday.PadRight(15), message));
            }
        }

        public static List<String> GetLogFileList()
        {
            List<String> logFilesList = new List<String>();
            if (Directory.Exists(Constants.LogPath))
            {
                foreach (String file in Directory.EnumerateFiles(Constants.LogPath, Constants.StarDotTXT))
                {
                    logFilesList.Add(Path.GetFileName(file).Replace(Constants.DotTXT,String.Empty));
                }
            }
            return logFilesList;
        }

        public static List<String> ReadLogFile(String fileName)
        {
            String fullPath = System.IO.Path.Combine(Constants.LogPath, fileName);
            String line;
            List<String> errorList = new List<String>();
            using (StreamReader tempReader = new StreamReader(fullPath))
            {
                while ((line = tempReader.ReadLine()) != null)
                {
                    errorList.Add(line);
                }
            }
            return errorList;
        }
        #endregion

    }
}
