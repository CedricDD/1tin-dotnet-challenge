﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 21/04/2015
 */ 
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Collections;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Data;
using System.Drawing;
#endregion

namespace Quiz
{
    public partial class ReportsUserControl : UserControl
    {
        #region Properties
        List<User> userList = new List<User>();
        List<Student> studentList = new List<Student>();
        List<String> groupList = new List<String>();
        ObservableCollection<Student> currentGroupList = new ObservableCollection<Student>();
        #endregion

        #region Constructor
        public ReportsUserControl()
        {
            InitializeComponent();
            GenerateUsers();
            GenerateStudents();
            GenerateGroups();
            UpdateBoxes();
            exportButton.ToolTip = Constants.ExportButtonToolTip;
        }
        #endregion

        #region Methods
        private void UpdateBoxes()
        {
            groupListListBox.ItemsSource = groupList;
            studentsListBox.ItemsSource = currentGroupList;
            groupListListBox.Items.SortDescriptions.Add(new System.ComponentModel.               //Sort Alphabetically
                SortDescription(Constants.SortContent, System.ComponentModel.
                ListSortDirection.Ascending));
            studentsListBox.Items.SortDescriptions.Add(new System.ComponentModel.               //Sort Alphabetically
                SortDescription(Constants.SortContent, System.ComponentModel.
                ListSortDirection.Ascending));
            groupListListBox.SelectedIndex = 0;
            studentsListBox.SelectedIndex = 0;
        }

        private void GenerateGroups()
        {
            foreach (User tempStudent in studentList)
            {
                if (groupList.IndexOf(tempStudent.GetGroup) == -1)
                {
                    groupList.Add(tempStudent.GetGroup);
                }
            }


        }

        private void GenerateStudents()
        {
            foreach (User tempUser in userList)
            {
                if (tempUser.GetUserType().Equals(Constants.UserTypeStudentText))
                {
                    studentList.Add((Student)tempUser);
                }
            }
        }

        private void GenerateUsers()
        {
            FileManager.CheckUserFiles(userList);
        }
        #endregion

        #region Handlers
        private void SelectionChangedHandler(object sender, SelectionChangedEventArgs e)
        {
            UpdateGroupList();
        }

        private void UpdateGroupList()
        {
            currentGroupList.Clear();
            foreach (Student tempStudent in studentList)
            {
                if (tempStudent.GetGroup.Equals(groupListListBox.SelectedItem.ToString()))
                {
                    currentGroupList.Add(tempStudent);
                }
            }
            studentsListBox.SelectedIndex = 0;
        }

        private void StudentsListBoxSelectionChangedHandler(object sender, SelectionChangedEventArgs e)
        {
            if (studentsListBox.SelectedItem != null)
            {
                ListBox tempListbox = (ListBox)sender;
                Student tempStudent = (Student)tempListbox.SelectedItem;
                reportGrid.Children.Clear();
                reportGrid.Children.Add(tempStudent.GetReportGrid());
                GeneratePreviousResults(tempStudent);
            }
        }

        private void GeneratePreviousResults(Student tempStudent)
        {
            List<UserResult> resultsList = FileManager.ReadUserResultsFile(tempStudent);
            resultsTextBlock.Text = String.Empty;
            foreach (UserResult result in resultsList)
            {
                resultsTextBlock.Text += String.Concat(result.ToString(),Environment.NewLine);
            }
        }

        private void ExportButtonClickHandler(object sender, RoutedEventArgs e)
        {
            ExcelExport.GenerateUserReport((Student)studentsListBox.SelectedItem);
            }
        }
        #endregion
    }

