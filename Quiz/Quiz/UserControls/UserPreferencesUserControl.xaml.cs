﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 22/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
#endregion

namespace Quiz
{
    public partial class UserPreferencesUserControl : UserControl
    {
        #region Properties
        private String gridToShow;
        private MainWindow mainInstance;
        #endregion

        #region Constructor
        public UserPreferencesUserControl(String gridToShow, MainWindow mainInstance)
        {
            InitializeComponent();
            this.gridToShow = gridToShow;
            this.mainInstance = mainInstance;
            ShowCorrectGrid();
            SetContent();
        }
        #endregion

        #region Methods
        private void SetContent()
        {
            editFirstNameTextBox.Text = mainInstance.GetLoggedInUser().GetFirstName;
            editLastNameTextBox.Text = mainInstance.GetLoggedInUser().GetLastName;
        }

        private void ShowCorrectGrid()
        {
            switch (gridToShow)
            {
                case Constants.changeFirstNameGridName:
                    changeFirstNameGrid.Visibility = Visibility.Visible;
                    changeLastNameGrid.Visibility = Visibility.Hidden;
                    changePasswordGrid.Visibility = Visibility.Hidden;
                    break;
                case Constants.changeLastNameGridName:
                    changeLastNameGrid.Visibility = Visibility.Visible;
                    changeFirstNameGrid.Visibility = Visibility.Hidden;
                    changePasswordGrid.Visibility = Visibility.Hidden;
                    break;
                case Constants.changePasswordGridName:
                    changePasswordGrid.Visibility = Visibility.Visible;
                    changeFirstNameGrid.Visibility = Visibility.Hidden;
                    changeLastNameGrid.Visibility = Visibility.Hidden;
                    break;
            }
        }

        private void ShowHideSaveButton(bool onOff)
        {
            if (onOff)
            {
                saveButton.Opacity = Constants.OpacityFull;
                saveButton.IsEnabled = true;
            }
            else
            {
                saveButton.Opacity = Constants.OpacityPointOne;
                saveButton.IsEnabled = false;
            }

        }

        private void SetMainWindowLayout()
        {
            mainInstance.userChangePrefsBorder.Visibility = Visibility.Collapsed;
            mainInstance.UpdateLoggedInUserLabel();
        }
        #endregion

        #region Handlers
        private void VisiblityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (checkMarkImagePasswords.Visibility == Visibility.Visible)
            {
                ShowHideSaveButton(true);
            }
            else
            {
                ShowHideSaveButton(false);
            }
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            SaveUserPrefs();
            SetMainWindowLayout();
        }

        private void SaveUserPrefs()
        {
            switch (gridToShow)
            {
                case Constants.changeFirstNameGridName:
                    if (!editFirstNameTextBox.Text.Equals(String.Empty))
                    {
                        mainInstance.GetLoggedInUser().SetFirstName(editFirstNameTextBox.Text);

                    }
                    else
                    {
                        new CustomMessageBox(Constants.MessageIfFirstNameEmpty).Show();
                    }
                    break;
                case Constants.changeLastNameGridName:
                    if (!editLastNameTextBox.Text.Equals(String.Empty))
                    {
                        mainInstance.GetLoggedInUser().SetLastName(editLastNameTextBox.Text);
                    }
                    else
                    {
                        new CustomMessageBox(Constants.MessageIfLastNameEmpty).Show();
                    }
                    break;
                case Constants.changePasswordGridName:
                    if (!editPasswordBox.Password.Equals(String.Empty) && !editPasswordRepeatBox.Password.Equals(String.Empty))
                    {
                        mainInstance.GetLoggedInUser().SetPassword(editPasswordBox.Password);
                    }
                    else
                    {
                        new CustomMessageBox(Constants.MessageIfPasswordEmpty).Show();
                    }
                    break;
            }

            FileManager.WriteUserFile(mainInstance.GetLoggedInUser());
        }

        private void PasswordChangedHandler(object sender, RoutedEventArgs e)
        {
            PasswordBox tempPasswordBox = (PasswordBox)sender;
            if (PasswordCheck.CheckPassWordFormat(tempPasswordBox.Password) && PasswordCheck.ComparePasswords(editPasswordBox.Password, editPasswordRepeatBox.Password))
            {
                checkMarkImagePasswords.Visibility = Visibility.Visible;
                xMarkImagePasswords.Visibility = Visibility.Hidden;
                saveButton.Visibility = Visibility.Visible;
            }
            else
            {
                checkMarkImagePasswords.Visibility = Visibility.Hidden;
                xMarkImagePasswords.Visibility = Visibility.Visible;
                saveButton.Visibility = Visibility.Hidden;
            }
        }
        #endregion
    }
}
