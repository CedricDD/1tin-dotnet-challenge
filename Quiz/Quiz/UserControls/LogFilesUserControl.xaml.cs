﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/04/2015
 */ 
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;
#endregion

namespace Quiz
{
    public partial class LogFilesUserControl : UserControl
    {

        #region Constructor
        public LogFilesUserControl()
        {
            InitializeComponent();
            LoadLogFiles();
            rawFileButton.ToolTip = Constants.RawTXTToolTip;
        }
        #endregion

        #region Methods
        private void LoadLogFiles()
        {
            datesListBox.ItemsSource = FileManager.GetLogFileList();
            datesListBox.SelectedIndex = 0;
        }

        private void PopulateErrorList(String fileName)
        {
            errorListTextBlock.Inlines.Clear();
            foreach (String tempString in FileManager.ReadLogFile(fileName))
            {
                errorListTextBlock.Text += String.Format("{0}\n", tempString);
            }
        }

        private void rawFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (datesListBox.SelectedIndex != -1)
            {
             Process.Start(System.IO.Path.Combine(Constants.LogPath,String.Concat(datesListBox.SelectedItem.ToString(),Constants.DotTXT)));
            }
        }

        private void SelectionChangedHandler(object sender, SelectionChangedEventArgs e)
        {
            ListBox tempListBox = (ListBox)sender;
            String fileName = String.Concat(tempListBox.SelectedItem.ToString(),Constants.DotTXT);
            switch (tempListBox.Name){
                case Constants.DatesListBoxName:
                    PopulateErrorList(fileName);
                break;
            }
        }
        #endregion
    }
}
