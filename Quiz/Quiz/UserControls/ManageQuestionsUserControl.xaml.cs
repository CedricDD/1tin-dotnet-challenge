﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Controls.Primitives;
#endregion

namespace Quiz
{
    public partial class ManageQuestionsUserControl : UserControl, IReplace
    {
        #region Properties
        ObservableCollection<Question> allQuestions = new ObservableCollection<Question>();
        #endregion

        #region Constructor
        public ManageQuestionsUserControl()
        {
            InitializeComponent();
            LoadQuestions();                                                                                //Loads all the question files and creates objects
            SetIReplacePropertyOnQuestions();                                                               //Sets the IReplaceProperty to this instance for all question objects
            ButtonToClick(Constants.MathCourse);                                                            //Click the math button to the initialize
            CheckManageButtons();
            SetToolTips();
        }

        private void SetToolTips()
        {
            courseChoiceMathButton.ToolTip = Constants.CourseChoiceMathButtonToolTip;
            courseChoiceLanguageButton.ToolTip = Constants.CourseChoiceLanguageButtonToolTip;
            courseChoiceKnowledgeButton.ToolTip = Constants.CourseChoiceKnowledgeButtonToolTip;
            saveButton.ToolTip = Constants.SaveButtonToolTip;
            addButton.ToolTip = Constants.AddButtonToolTip;
            deleteButton.ToolTip = Constants.DeleteButtonToolTip;
            backupButton.ToolTip = Constants.BackupButtonToolTip;
        }

        private void LoadQuestions()
        {
            FileManager.CheckCourseFile(Constants.MathCourse, allQuestions);                                             //Checks and the reads the math file + adds the questions the allQuestion list
            FileManager.CheckCourseFile(Constants.LanguageCourse, allQuestions);                                         //Checks and the reads the language file + adds the questions the allQuestion list
            FileManager.CheckCourseFile(Constants.KnowledgeCourse, allQuestions);                                        //Checks and the reads the knowledge file + adds the questions the allQuestion
        }
        #endregion

        #region Replace (Callback Interface)

        //This method is called within the question object. It receives the old object and the new questionType in order to replace the old object with a new one.

        public void OnReplaceRequested(Question oldQuestionObject, char questionType)
        {
            Question old = (Question)questionsListBox.SelectedItem;                                                                         //Select the old question object from the listbox
            int index = allQuestions.IndexOf(old);                                                                                          //Lookup the position of the old object
            Question newQuestionObject;                                                                                                     //Declare new object
            String restOfString = String.Empty;                                                                                             //Create empty string
            switch (questionType)
            {
                case Constants.OneChoiceQuestionType:
                    restOfString = String.Empty;                                                                                            //OneChoice = empty answer
                    break;
                case Constants.MultipleChoiceQuestionType:                                                                                  //MultipleChoice = 5 empty answers, 5 false bools        
                    restOfString = Constants.RestOfStringMultipleChoice;
                    break;
                case Constants.DragDropQuestionType:
                    restOfString = Constants.RestOfStringDragDrop;                                                                          //DragDrop = 5 empty drag answers, 5 empty drop answers
                    break;
                case Constants.TableQuestionType:
                    restOfString = Constants.RestOfStringTableQ;                                                                            //TableQuestions = Default multiplier 1
                    break;
            }
            newQuestionObject = FileManager.CreateSubQuestion(String.Format("{0};{1};{2};{3};{4}",                                          //Create new object with old question parameters and default answers.
            oldQuestionObject.GetCourse, oldQuestionObject.GetDifficulty, questionType, oldQuestionObject.GetQuestion, restOfString));
            allQuestions[index] = newQuestionObject;                                                                                        //Replace the old object with the new one
            questionsListBox.ItemsSource = UpdateCourseList(newQuestionObject.GetCourse);                                                   //Update the listbox
            FileManager.WriteCourseFile(newQuestionObject.GetPath(), UpdateCourseList(newQuestionObject.GetCourse));                               //Update the txt file
            questionsListBox.SelectedItem = newQuestionObject;                                                                              //Select the new question object in the listbox
            SetIReplacePropertyOnQuestions();                                                                                               //Set instance to all question objects in the list
        }

        private void SetIReplacePropertyOnQuestions()
        {
            foreach (Question temp in allQuestions)
            {
                temp.SetIReplaceProperty(this);                                                                                             //This sets the current instance of ManageQuestionsUserControl on all Questions in the list
            }
        }

        #endregion

        #region ListBoxActions
        public ObservableCollection<Question> UpdateCourseList(char course)
        {
            ObservableCollection<Question> courseList = new ObservableCollection<Question>();
            for (int i = 0; i < allQuestions.Count; i++)                                                    //For every question object in allQuestion
            {
                if (allQuestions[i].GetCourse == course)                                                    //Get the questions that math the corresponding course
                {
                    courseList.Add(allQuestions[i]);                                                        //Add every question of a certain course to the new List
                }
            }
            return courseList;                                                                              //Return the new list containing only the question objects of the corresponding course
        }

        private void SelectFirstItem()
        {
            if (questionsListBox.Items.Count != 0)
            {
                questionsListBox.SelectedIndex = 0;                                                         //Select index 0 in the list (only if the list is populated)
            }
        }
        #endregion

        #region Handlers
        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            ResetButtonIndicators();
            Border tempBorder = (Border)navElements.FindName(String.Format("{0}{1}", tempButton.Name, Constants.IndicatorString));
            tempBorder.Visibility = Visibility.Visible;
            switch (tempButton.Name)
            {
                case Constants.CourseChoiceMathButtonName:
                    SetVisibility(Constants.MathCourse);                                                    //Change the layout for math
                    break;
                case Constants.CourseChoiceLanguageButtonName:
                    SetVisibility(Constants.LanguageCourse);                                                //Change the layout for language
                    break;
                case Constants.CourseChoiceKnowledgeButtonName:
                    SetVisibility(Constants.KnowledgeCourse);                                               //Change the layout for knowledge
                    break;
            }

        }

        private void ModifyClickHandler(object sender, RoutedEventArgs e)
        {
            Button tempButton = (Button)sender;
            switch (tempButton.Name)
            {
                case Constants.DeleteButtonName:
                    DeleteQuestion();                                                                       //Calls the Delete Question method
                    CheckManageButtons();
                    break;
                case Constants.AddButtonName:
                    AddQuestion();                                                                          //Calls the Add Question method
                    CheckManageButtons();
                    break;
                case Constants.SaveButtonName:
                    SaveQuestion();                                                                         //Calls the Save Question method
                    SetSaveButtonDisabled();
                    break;
                case Constants.BackupButtonName:                                                                        //Calls the Backup Questions method
                    BackupAllQuestions();
                    break;
            }

        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Question tempQuestion = (Question)questionsListBox.SelectedItem;
            int tempQuestionIndex = questionsListBox.SelectedIndex;
            Selector selector = (Selector)sender;
            if (selector is ListBox)
            {
                (selector as ListBox).ScrollIntoView(selector.SelectedItem);                               //Scroll to added item in listbox (useful when a new item is added to the bottom of the listbox)
            }
            allDetailsGrid.Children.Clear();
            if (tempQuestionIndex != -1)
            {
                allDetailsGrid.Children.Add(tempQuestion.GetDetailsGrid(this));                             //Get the correct grid Layout
            }
            SetSaveButtonDisabled();
        }

        #endregion

        #region DeleteAddSaveBackup
        private void BackupAllQuestions()
        {
            System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();
            fbd.Description = Constants.FolderBrowseDialogDesc;
            System.Windows.Forms.DialogResult result = fbd.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                FileManager.BackupAllCourseFiles(fbd.SelectedPath, this);
            }
        }

        private void DeleteQuestion()
        {
            if (allQuestions.Count != 0)
            {
                Question tempDelete = (Question)questionsListBox.SelectedItem;
                int tempDeleteIndex = allQuestions.IndexOf(tempDelete);                                     //Get the index number in the List of the selection Qeestion
                allQuestions.RemoveAt(tempDeleteIndex);                                                     //Remove the question
                questionsListBox.ItemsSource = UpdateCourseList(tempDelete.GetCourse);                      //Update the listbox content
                FileManager.WriteCourseFile(tempDelete.GetPath(), UpdateCourseList(tempDelete.GetCourse));   //Write the txt file
                SelectFirstItem();                                                                          //Select the first item in the listbox
            }
        }

        private void AddQuestion()
        {
            Question tempAdd = tempAdd = FileManager.CreateSubQuestion(Constants.DefaultMathQuestion);
            if (courseChoiceLanguageButtonIndicator.Visibility == Visibility.Visible)
            {
                tempAdd = FileManager.CreateSubQuestion(Constants.DefaultLanguageQuestion);
            }
            else if (courseChoiceKnowledgeButtonIndicator.Visibility == Visibility.Visible)
            {
                tempAdd = FileManager.CreateSubQuestion(Constants.DefaultKnowledgeQuestion);
            }
            allQuestions.Add(tempAdd);                                                                  //Add the question to the List
            SetIReplacePropertyOnQuestions();                                                           //Set the interface property on the question just added
            questionsListBox.ItemsSource = UpdateCourseList(tempAdd.GetCourse);                         //Change the listbox content the course of the object just added
            FileManager.WriteCourseFile(tempAdd.GetPath(), UpdateCourseList(tempAdd.GetCourse));        //Write the txt file
            questionsListBox.SelectedItem = tempAdd;                                                    //Select the item just added in the listbox
            saveButton.FontWeight = FontWeights.Normal;

        }

        private void SaveQuestion()
        {
            if (questionsListBox.SelectedIndex != -1)
            {
                Question tempSave = (Question)questionsListBox.SelectedItem;
                ButtonToClick(tempSave.GetCourse);                                                                  //Click the course button corresponding with the object just replaced
                int tempSaveIndex = allQuestions.IndexOf(tempSave);                                                 //Get the index number in the List of the selection Qeestion
                allQuestions[tempSaveIndex] = tempSave;                                                             //Replace the selected question object in the list with the new question object
                questionsListBox.ItemsSource = UpdateCourseList(tempSave.GetCourse);                                //Change the listbox content the course of the object just replaced
                FileManager.WriteCourseFile(Constants.MathPath, UpdateCourseList(Constants.MathCourse));               //Write math.txt with the updated math questions list
                FileManager.WriteCourseFile(Constants.LanguagePath, UpdateCourseList(Constants.LanguageCourse));       //Write language.txt with the updated language question list
                FileManager.WriteCourseFile(Constants.KnowledgePath, UpdateCourseList(Constants.KnowledgeCourse));     //Write knowledge.txt with the updated knowledge question list
                questionsListBox.SelectedItem = tempSave;                                                           //Select the item just replaced in the listbox

            }
        }
        #endregion

        #region Various

        public void SetSaveButtonEnabled()
        {
            saveButton.Opacity = Constants.OpacityFull;
            saveButton.IsEnabled = true;
        }

        public void SetSaveButtonDisabled()
        {
            saveButton.Opacity = Constants.OpacityPointOne;
            saveButton.IsEnabled = false;
        }

        private void SetVisibility(char course)
        {
            courseChoiceMathButton.Background = Constants.GrayThreeBrush;
            courseChoiceLanguageButton.Background = Constants.GrayThreeBrush;
            courseChoiceKnowledgeButton.Background = Constants.GrayThreeBrush;
            courseChoiceMathButton.FontWeight = FontWeights.Normal;
            courseChoiceLanguageButton.FontWeight = FontWeights.Normal;
            courseChoiceKnowledgeButton.FontWeight = FontWeights.Normal;
            SelectFirstItem();
            questionsListBox.ItemsSource = UpdateCourseList(course);
            switch (course)
            {
                case Constants.MathCourse:
                    courseChoiceMathButton.FontWeight = FontWeights.Bold;
                    courseChoiceMathButton.Background = Constants.GraySixBrush;
                    break;
                case Constants.LanguageCourse:
                    courseChoiceLanguageButton.FontWeight = FontWeights.Bold;
                    courseChoiceLanguageButton.Background = Constants.GraySixBrush;
                    break;
                case Constants.KnowledgeCourse:
                    courseChoiceKnowledgeButton.FontWeight = FontWeights.Bold;
                    courseChoiceKnowledgeButton.Background = Constants.GraySixBrush;
                    break;
            }
            CheckManageButtons();
        }

        private void ResetButtonIndicators()
        {
            courseChoiceMathButtonIndicator.Visibility = Visibility.Hidden;
            courseChoiceLanguageButtonIndicator.Visibility = Visibility.Hidden;
            courseChoiceKnowledgeButtonIndicator.Visibility = Visibility.Hidden;

        }


        public void ButtonToClick(char course)
        {
            switch (course)
            {
                case Constants.MathCourse:
                    courseChoiceMathButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, courseChoiceMathButton));              //Click the math button
                    break;
                case Constants.LanguageCourse:
                    courseChoiceLanguageButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, courseChoiceLanguageButton));      //Click the language button
                    break;
                case Constants.KnowledgeCourse:
                    courseChoiceKnowledgeButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, courseChoiceKnowledgeButton));    //Click the knowledge button
                    break;
                default:
                    break;
            }
        }

        private void CheckManageButtons()
        {
            if (questionsListBox.SelectedIndex == -1)
            {
                deleteButton.Opacity = Constants.OpacityPointOne;
                deleteButton.IsEnabled = false;
                saveButton.Opacity = Constants.OpacityPointOne;
                saveButton.IsEnabled = false;
                backupButton.Opacity = Constants.OpacityPointOne;
                backupButton.IsEnabled = false;
            }
            else
            {
                deleteButton.Opacity = Constants.OpacityFull;
                deleteButton.IsEnabled = true;
                backupButton.Opacity = Constants.OpacityFull;
                backupButton.IsEnabled = true;
            }
        }
        #endregion
    }
}
