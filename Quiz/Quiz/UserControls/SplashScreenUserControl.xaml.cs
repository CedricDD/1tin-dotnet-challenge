﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 15/04/2015
 */
#endregion


#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
#endregion

namespace Quiz
{
    public partial class SplashScreenUserControl : UserControl
    {
        #region Constructor
        public SplashScreenUserControl()
        {
            InitializeComponent();
        }
        #endregion
    }
}
