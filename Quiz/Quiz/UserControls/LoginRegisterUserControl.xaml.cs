﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 20/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
#endregion

namespace Quiz
{
    public partial class LoginRegisterUserControl : UserControl
    {

        #region Properties
        private MainWindow mainInstance;
        private List<User> userList = new List<User>();
        private User tempUser;
        private User loggedInUser;
        String userType;
        #endregion

        #region Constructor
        public LoginRegisterUserControl(MainWindow mainInstance, String userType)
        {
            InitializeComponent();
            this.mainInstance = mainInstance;
            this.userType = userType;
            CheckUserType();
            SetVisibilityRegisterButton(false);
            LoadUsers();
            Loaded += LoginRegisterUserControl_Loaded;
        }
        #endregion

        #region Methods
        private void CheckUserType()
        {
            if (userType.Equals(Constants.UserTypeStudentText))
            {
                loginRadioButton.IsChecked = true;
                registerRadioButton.IsChecked = false;
                loginRadioButton.Visibility = Visibility.Visible;
                registerRadioButton.Visibility = Visibility.Visible;
                addTeacherLabel.Visibility = Visibility.Hidden;
            }
            else
            {
                loginRadioButton.IsChecked = false;
                registerRadioButton.IsChecked = true;
                loginRadioButton.Visibility = Visibility.Hidden;
                registerRadioButton.Visibility = Visibility.Hidden;
                addTeacherLabel.Visibility = Visibility.Visible;
            }
        }

        private void SetFocus(UIElement tempUI)
        {
            tempUI.Focusable = true;
            Keyboard.Focus(tempUI);
        }

        private void LoginRegisterUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetFocus(loginNameTextBox);
        }

        private void LoadUsers()
        {
            try
            {
                //Adds the Admin user to the list first!
                userList.Add(new Admin(Constants.AdminLoginName,
                    Constants.AdminFirstName,
                    Constants.AdminLastName,
                    Constants.AdminGroup,
                    Constants.AdminPassword));
                //Adds the teacher and student users
                FileManager.CheckUserFiles(userList);
            }
            catch (FileCorruptException ex)
            {
                mainInstance.WindowState = WindowState.Minimized;
                FileManager.WriteToLogFile(String.Concat(Constants.FileCorruptText, ex.GetFilePath()));
                new CustomMessageBox(ex.Message,true).Show();
            }
        }

        private bool LookUpUser()
        {
            bool boolTemp = false;
            for (int i = 0; i < userList.Count; i++)
            {

                if (userList[i].GetLoginName.ToString().Equals(loginNameTextBox.Text.ToLower()))
                {
                    tempUser = userList[i];
                    boolTemp = true;
                }
            }
            return boolTemp;
        }

        private void LogIn()
        {
            if (LookUpUser())
            {
                if (PasswordCheck.ComparePasswords(loginPasswordBox.Password, tempUser.GetPassword))
                {
                    loggedInUser = tempUser;
                    mainInstance.SetLoggedInUser(loggedInUser);
                    SetMainWindowLayout();
                    tempUser = null;
                }
                else
                {
                    loggedInUser = null;
                    new CustomMessageBox(Constants.MessageIfPasswordIncorrect).Show();
                    loginPasswordBox.Clear();
                }
            }
            else
            {
                new CustomMessageBox(Constants.MessageIfUserDoesNotExit).Show();
            }
        }

        private void Register()
        {
            String line;
            if (userType.Equals(Constants.UserTypeStudentText))
            {
                line = String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", Constants.UserTypeStudent,
                    registerLoginNameTextBox.Text.ToLower(), registerFirstNameTextBox.Text,
                    registerLastNameTextBox.Text, registerGroupTextBox.Text.ToUpper(), registerPasswordBox.Password, 0, 0);
            }
            else
            {
                line = String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", Constants.UserTypeTeacher,
                    registerLoginNameTextBox.Text.ToLower(), registerFirstNameTextBox.Text,
                    registerLastNameTextBox.Text, registerGroupTextBox.Text.ToUpper(), registerPasswordBox.Password, 0, 0);
            }
            User temporaryUser = FileManager.CreateSubUserObject(line);
            userList.Add(temporaryUser);
            FileManager.WriteUserFiles(userList);
            loggedInUser = temporaryUser;
            mainInstance.SetLoggedInUser(loggedInUser);
            SetMainWindowLayout();
        }

        private void CheckLoginName()
        {
            bool userExists = false;
            for (int i = 0; i < userList.Count; i++)
            {
                if (userList[i].GetLoginName.ToString().Equals(registerLoginNameTextBox.Text.ToLower()))
                {
                    userExists = true;
                    break;
                }
            }
            if (userExists || registerLoginNameTextBox.Text.Equals(String.Empty))
            {
                xMarkImageLoginName.Visibility = Visibility.Visible;
                checkMarkImageLoginName.Visibility = Visibility.Hidden;
                xMarkImageLoginName.ToolTip = Constants.MessageUserExists;
            }
            else
            {
                xMarkImageLoginName.Visibility = Visibility.Hidden;
                checkMarkImageLoginName.Visibility = Visibility.Visible;
            }
        }

        private void SetVisibilityLogin()
        {
            loginGrid.Visibility = Visibility.Visible;
            registerGrid.Visibility = Visibility.Hidden;
            SetFocus(loginNameTextBox);
        }

        private void SetVisibilityRegister()
        {
            loginGrid.Visibility = Visibility.Hidden;
            registerGrid.Visibility = Visibility.Visible;
            SetFocus(registerFirstNameTextBox);
        }

        private void SetVisibilityRegisterButton(bool onOff)
        {
            if (onOff)
            {
                registerButton.Opacity = Constants.OpacityFull;
                registerButton.IsEnabled = true;
            }
            else
            {
                registerButton.Opacity = Constants.OpacityPointOne;
                registerButton.IsEnabled = false;
            }

        }

        private void SetMainWindowLayout()
        {
            mainInstance.UpdateLoggedInUserLabel();
            mainInstance.SetCorrectLayout();
        }
        #endregion

        #region Handlers
        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button temp = (Button)sender;
            switch (temp.Name)
            {
                case Constants.LoginButtonName:
                    LogIn();
                    break;
                case Constants.RegisterButtonName:
                    Register();
                    break;
            }
        }

        private void CheckedHandler(object sender, RoutedEventArgs e)
        {
            RadioButton temp = (RadioButton)sender;
            switch (temp.Name)
            {
                case Constants.LoginRadioButtonName:
                    SetVisibilityLogin();
                    break;
                case Constants.RegisterRadioButtonName:
                    SetVisibilityRegister();
                    break;
            }
        }

        private void RegisterNamesTextChangedHandler(object sender, TextChangedEventArgs e)
        {
            registerLoginNameTextBox.Text = String.Format("{0}{1}", registerFirstNameTextBox.Text, registerLastNameTextBox.Text).ToLower().Replace(Constants.Space, String.Empty);
            CheckLoginName();
        }

        private void RegisterPasswordChangedHandler(object sender, RoutedEventArgs e)
        {
            PasswordBox tempPasswordBox = (PasswordBox)sender;
            if (PasswordCheck.CheckPassWordFormat(tempPasswordBox.Password) && PasswordCheck.ComparePasswords(registerPasswordBox.Password, registerPasswordRepeatBox.Password))
            {
                checkMarkImagePasswords.Visibility = Visibility.Visible;
                xMarkImagePasswords.Visibility = Visibility.Hidden;
            }
            else
            {
                checkMarkImagePasswords.Visibility = Visibility.Hidden;
                xMarkImagePasswords.Visibility = Visibility.Visible;
                xMarkImagePasswords.ToolTip = Constants.MessagePasswordNotRightFormat;
            }

        }

        private void RegisterLoginNameTextChangedHandler(object sender, TextChangedEventArgs e)
        {
            CheckLoginName();
        }

        private void VisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (checkMarkImageLoginName.Visibility == Visibility.Visible & checkMarkImagePasswords.Visibility == Visibility.Visible)
            {
                SetVisibilityRegisterButton(true);
            }
            else
            {
                SetVisibilityRegisterButton(false);
            }
        }

        private void loginPasswordBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                loginButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, loginButton));
            }
        }
        #endregion

    }
}
