﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 04/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Collections;
using System.Windows.Threading;
#endregion

namespace Quiz
{
    public partial class TakeQuizUserControl : UserControl
    {
        #region Properties
        private char course;
        ObservableCollection<Question> courseQuestions = new ObservableCollection<Question>();
        ObservableCollection<Question> quizQuestions = new ObservableCollection<Question>();
        private int questionNumber = 1;
        private int totalScore = 0;
        private DispatcherTimer quizTimer = new DispatcherTimer();
        private int quizTimerSeconds = 0;
        MainWindow mainInstance;
        #endregion

        #region Constructor
        public TakeQuizUserControl(char course, MainWindow mainInstance)
        {
            InitializeComponent();
            this.course = course;                                                       //Gets the course passed by the window and sets it
            this.mainInstance = mainInstance;                                           //Gets the window instance and sets it
            SetSplashLayout();
        }
        #endregion

        #region Questions

        private void StartQuiz()
        {
            try
            {
                FileManager.CheckCourseFile(course, courseQuestions);                  //Creates the questions for the corresponding course and adds them to the courseQuestions list
                CheckQuestions();                                                      //Checks if there are enough questions to complete a quiz or not
                SetStartQuizLayout();
            }
            catch (FileCorruptException ex)
            {
                new CustomMessageBox(ex.Message).Show();
                FileManager.WriteToLogFile(String.Concat(Constants.FileCorruptText, ex.GetFilePath()));
            }
        }

        private void CheckQuestions()
        {
            if (courseQuestions.Count >= 5)                                             //If there are 5 or more questions it starts the quiz, else it reloads the application
            {
                RandomizeQuestions();                                                   //Picks 5 random questions from courseQuestions and adds them to quizQuestions
                LoadQuestion();                                                         //Load the first question into the UI to initialize
                InitializeTimer();                                                      //Initializes the timer
            }
            else
            {
                new CustomMessageBox(Constants.MessageIfNoQuestionsQuiz,true).Show();
                FileManager.WriteToLogFile(Constants.MessageIfNoQuestionsQuiz);
            }
        }

        private void RandomizeQuestions()
        {
            Question tempQuestion;
            Random random = new Random();
            int randomNumber;
            int length = courseQuestions.Count;
            for (int i = 0; i <= 4; i++)
            {
                randomNumber = random.Next(0, length);                              //Generate a random number between 0 en the amount of question in the list
                tempQuestion = courseQuestions[randomNumber];
                while (quizQuestions.IndexOf(tempQuestion) != -1)                   //Check if the question is not already in the quiz list (prevent duplicate questions)
                {
                    randomNumber = random.Next(0, length);                          //Generate new random number to select a new random question
                    tempQuestion = courseQuestions[randomNumber];
                }
                quizQuestions.Add(tempQuestion);                                    //Add the random question to the quiz list
            }
        }

        private void LoadQuestion()
        {
            Question questionToLoad = quizQuestions[questionNumber - 1];
            quizGrid.Children.Clear();                                                                              //Clear the question detail grid
            quizGrid.Children.Add(questionToLoad.GetQuizDetailsGrid(this));                                         //Add the grid of the corresponding question to the quiz detail grid
            String questionType = GetQuestionTypeNL(questionToLoad.GetQuestionType);                                //Translate the Object type to Dutch
            questionNumberLabel.Content = String.Format("Vraag {0}/5: {1}", questionNumber, questionType);          //Set the content of the quiz header label
            quizTimer.Start();
        }
        #endregion

        #region Handlers
        private void TickHandler(object sender, EventArgs e)
        {
            clockLabel.Content = CalculateRemainingTime();                                      //Show the remaining time in the UI
            quizTimerSeconds++;
            if (quizTimerSeconds == quizQuestions[questionNumber - 1].GetQuestionTimeLimit)     //Proceed when the time limit of the question is reached
            {
                ResetTimer();
                if (questionNumber - 1 == 4)
                {
                    ClickResultButton();
                }
                else
                {
                    ClickNextButton();
                }
            }
        }

        private void ClickHandler(object sender, RoutedEventArgs e)
        {
            Button temp = (Button)sender;
            switch (temp.Name)
            {
                case Constants.NextButtonName:
                    GenerateResultAndScore();                                           //Generates the result grid and score for the current question
                    SetVisibilityNextQuestion();
                    break;
                case Constants.ResultButtonName:
                    GenerateResultAndScore();                                           //Generates the result grid and score for the current question
                    SetVisibilityResult();
                    SaveUserScore();
                    break;
                case Constants.QuitButtonName:
                    SetVisibilityQuitQuiz();
                    break;
                case Constants.StartQuizButtonName:
                    StartQuiz();
                    break;
                case Constants.CancelQuizButtonName:
                    SetVisibilityQuitQuiz();
                    break;
            }
        }

        public void ClickNextButton()
        {
            nextButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, nextButton));      //Go on to the next question
        }

        public void ClickResultButton()
        {
            resultButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, resultButton));      //Go on to the quiz results
        }
        #endregion

        #region Timer

        private void InitializeTimer()
        {
            quizTimer.Interval = TimeSpan.FromSeconds(1);                               //Sets the timer interval to 1 second
            quizTimer.Tick += TickHandler;
        }

        private void ResetTimer()
        {
            quizTimer.Stop();                                                           //Stops the timer and resets the clock to zero
            quizTimerSeconds = 0;

        }

        private String CalculateRemainingTime()
        {
            int minutes = (quizQuestions[questionNumber - 1].GetQuestionTimeLimit - quizTimerSeconds) / 60;         //Minutes = seconds / 60 -> 140 / 60 = 2
            int seconds = (quizQuestions[questionNumber - 1].GetQuestionTimeLimit - quizTimerSeconds) % 60;         //Seconds = Remaining seconds
            return String.Format("{0:D2}:{1:D2}", minutes, seconds);
        }


        #endregion

        #region Results
        private void SaveUserScore()
        {
            if (mainInstance.GetLoggedInUser().GetUserType().Equals(Constants.UserTypeStudentText))
            {
                Student tempStudent = (Student)mainInstance.GetLoggedInUser();
                Question tempQuestion = quizQuestions[questionNumber - 1];
                tempStudent.SetQuizScore(tempStudent.GetQuizScore + totalScore);
                FileManager.WriteUserFile(tempStudent);
                mainInstance.UpdateLoggedInUserLabel();
            }
        }

        private Grid GetCorrectQuestionGrid()                                               //Gets the correct question result grid from the UI to fill
        {
            switch (questionNumber)
            {
                case 1:
                    return questionResult1;
                case 2:
                    return questionResult2;
                case 3:
                    return questionResult3;
                case 4:
                    return questionResult4;
                case 5:
                    return questionResult5;
                default:
                    return new Grid();
            }
        }

        private String GetQuestionTypeNL(char questionType)                                 //Translates the subclass object type to Dutch
        {
            switch (questionType)
            {
                case Constants.OneChoiceQuestionType:
                    return Constants.QuestionTypeORadioButtonContent;
                case Constants.MultipleChoiceQuestionType:
                    return Constants.QuestionTypeMRadioButtonContent;
                case Constants.DragDropQuestionType:
                    return Constants.QuestionTypeDRadioButtonContent;
                case Constants.TableQuestionType:
                    return Constants.QuestionTypeTRadioButtonContent;
                default:
                    return String.Empty;
            }
        }

        private void GenerateResultAndScore()
        {
            Question tempQuestion = quizQuestions[questionNumber - 1];
            tempQuestion.GenerateResult();                                                               //Calls the abstract method GenerateResult, the correct subclass method is then called
            GetCorrectQuestionGrid().Children.Clear();                                                   //Clear the grid
            GetCorrectQuestionGrid().Children.Add(tempQuestion.GetQuestionResult);                       //Gets the question result grid and adds the correct result grid to it
            totalScore += tempQuestion.GetQuestionScore;                                                 //Calculates the total score
            if (mainInstance.GetLoggedInUser().GetUserType().Equals(Constants.UserTypeStudentText))
            {
                FileManager.WriteUserResultsFile(tempQuestion.ToResult(), mainInstance.GetLoggedInUser());     //Writes result to txt file for reports
            }
        }


        #endregion

        #region Visibilities

        private void SetSplashLayout()
        {
            splashGrid.Visibility = Visibility.Visible;
            headerGrid.Visibility = Visibility.Hidden;
            quizGrid.Visibility = Visibility.Hidden;
            resultGrid.Visibility = Visibility.Hidden;
            splashQuizLabel.Content = Constants.SplashQuizLabelContent;
        }

        private void SetStartQuizLayout()
        {
            splashGrid.Visibility = Visibility.Hidden;
            headerGrid.Visibility = Visibility.Visible;
            quizGrid.Visibility = Visibility.Visible;
        }

        public void SetVisibilityQuitQuiz()
        {
            mainInstance.mainContentControl.Content = new SplashScreenUserControl();
            mainInstance.ResetButtons();
        }

        private void SetVisibilityResult()
        {
            SetVisibilityEndOfQuiz();
            resultButton.Visibility = Visibility.Hidden;
            quitButton.Visibility = Visibility.Visible;
        }

        private void SetVisibilityNextQuestion()
        {
            ResetTimer();
            if (questionNumber <= 4)
            {
                questionNumber++;
                LoadQuestion();
                if (questionNumber == 5)
                {
                    nextButton.Visibility = Visibility.Hidden;
                    resultButton.Visibility = Visibility.Visible;
                }
            }
        }

        public void SetVisibilityEndOfQuiz()
        {
            quizGrid.Visibility = Visibility.Hidden;
            resultGrid.Visibility = Visibility.Visible;
            clockLabel.Visibility = Visibility.Hidden;
            questionNumberLabel.Content = String.Format("{0}\t{1}",Constants.YourScoreString, Convert.ToString(totalScore));
        }
        #endregion
    }
}
