﻿#region Class Comment
/*
 * Created by Frankie Claessens - 1TINL
 * 28/04/2015
 */ 
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
#endregion

namespace Quiz
{
    public partial class ResetUserPassword : UserControl
    {
        #region Properties
        private List<User> userList = new List<User>();
        private User selectedUser;
        #endregion

        #region Constructor
        public ResetUserPassword()
        {
            InitializeComponent();
            LoadUsers();
            InitializeUserControl();
        }
        #endregion

        #region Methods
        private void InitializeUserControl()
        {
            ShowHideSaveButton(false);
            userListBox.Items.SortDescriptions.Add(new System.ComponentModel.               //Sort Alphabetically
                SortDescription(Constants.SortContent,System.ComponentModel.
                ListSortDirection.Ascending)); 
        }

        private void LoadUsers()
        {

            FileManager.ReadUserFiles(userList);
            userListBox.ItemsSource = userList;
            SelectFirstItem();
        }

        private void SelectFirstItem()
        {
            if (userListBox.Items.Count != 0)
            {
                userListBox.SelectedIndex = 0;
            }
        }

        private void ShowHideSaveButton(bool onOff)
        {
            if (onOff)
            {
                saveButon.Opacity = Constants.OpacityFull;
                saveButon.IsEnabled = true;
            }
            else
            {
                saveButon.Opacity = Constants.OpacityPointOne;
                saveButon.IsEnabled = false;
            }
        }
        #endregion

        #region Handlers
        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            selectedUser = (User)userListBox.SelectedItem;
            PasswordTextBox.Text = selectedUser.GetPassword;
            userTypeLabel.Content = selectedUser.GetUserType();
        }

        private void saveButon_Click(object sender, RoutedEventArgs e)
        {
            selectedUser.SetPassword(PasswordTextBox.Text);
            FileManager.WriteUserFile(selectedUser);
            PasswordTextBox.Text = selectedUser.GetPassword;
        }

        private void VisibilityChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (checkMarkImagePasswords.Visibility == Visibility.Visible)
            {
                ShowHideSaveButton(true);
            }
            else
            {
                ShowHideSaveButton(false);
            }
        }

        private void PasswordTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (PasswordCheck.CheckPassWordFormat(PasswordTextBox.Text))
            {
                checkMarkImagePasswords.Visibility = Visibility.Visible;
                xMarkImagePasswords.Visibility = Visibility.Hidden;
            }
            else
            {
                checkMarkImagePasswords.Visibility = Visibility.Hidden;
                xMarkImagePasswords.Visibility = Visibility.Visible;
                xMarkImagePasswords.ToolTip = Constants.MessagePasswordNotRightFormat;
            }
        }
        #endregion
    }

}
