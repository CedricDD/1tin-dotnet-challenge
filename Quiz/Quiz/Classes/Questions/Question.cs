﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.ComponentModel;
using System.Windows.Media;
using System.Windows.Threading;
#endregion

namespace Quiz
{
    public abstract class Question
    {
        #region Properties
        protected char course;
        protected int difficulty;
        protected char questionType;
        protected String question;
        protected int questionScore;
        protected int questionTimeLimit = Constants.QuestionTimeLimit;
        protected IReplace iReplaceProperty;
        protected const int pointPerCorrectAnswer = Constants.PointPerCorrectAnswer;
        protected TextBlock questionResult = new TextBlock();
        protected TakeQuizUserControl quizInstance;
        protected ManageQuestionsUserControl manageInstance;
        private Grid allDetailsGrid = new Grid();
        private Grid baseDetailsGrid = new Grid();
        private Grid extendedDetailsGrid = new Grid();
        private Grid quizAllDetailsGrid = new Grid();
        private Grid quizBaseDetailsGrid = new Grid();
        private Grid quizExtendedDetailsGrid = new Grid();
        private Label courseLabel = new Label();
        private Label difficultyLabel = new Label();
        private Label questionTypeLabel = new Label();
        private Label questionLabel = new Label();
        private Label quizQuestionLabel = new Label();
        private RadioButton courseMathRadioButton = new RadioButton();
        private RadioButton courseLanguageRadioButton = new RadioButton();
        private RadioButton courseKnowledgeRadioButton = new RadioButton();
        private RadioButton difficulty1RadioButton = new RadioButton();
        private RadioButton difficulty2RadioButton = new RadioButton();
        private RadioButton difficulty3RadioButton = new RadioButton();
        private RadioButton questionTypeORadioButton = new RadioButton();
        private RadioButton questionTypeMRadioButton = new RadioButton();
        private RadioButton questionTypeDRadioButton = new RadioButton();
        private RadioButton questionTypeTRadioButton = new RadioButton();
        private TextBox questionTextBox = new TextBox();
        protected TextBox quizQuestionTextBox = new TextBox();
        #endregion

        #region Constructor

        public Question(char course, int difficulty, char type, String question)
        {
            this.course = course;
            this.difficulty = difficulty;
            this.questionType = type;
            this.question = question;
            this.questionTimeLimit /= this.difficulty;
        }
        #endregion

        #region Object Methods
        public char GetCourse
        {
            get
            {
                return course;
            }
        }

        public void SetCourse(char course)
        {
            this.course = course;
        }

        public int GetDifficulty
        {
            get
            {
                return difficulty;
            }
        }

        public void SetDifficulty(int difficulty)
        {
            this.difficulty = difficulty;
        }

        public char GetQuestionType
        {
            get
            {
                return questionType;
            }
        }

        public void SetQuestionType(char type)
        {
            this.questionType = type;
        }

        public String GetQuestion
        {
            get
            {
                return question;
            }
        }

        public void SetQuestion(String question)
        {
            this.question = question;
        }

        public int GetQuestionScore
        {
            get
            {
                return this.questionScore;
            }
        }

        public void SetQuestionScore(int questionScore)
        {
            this.questionScore = questionScore;
        }

        public int GetQuestionTimeLimit
        {
            get
            {
                return this.questionTimeLimit;
            }
        }

        public IReplace GetIReplaceProperty
        {
            get
            {
                return this.iReplaceProperty;
            }
        }

        public void SetIReplaceProperty(IReplace iReplaceProperty)
        {
            this.iReplaceProperty = iReplaceProperty;
        }

        public String GetPath()
        {
            switch (this.course)
            {
                case Constants.MathCourse:
                    return Constants.MathPath;
                case Constants.LanguageCourse:
                    return Constants.LanguagePath;
                case Constants.KnowledgeCourse:
                    return Constants.KnowledgePath;
                default:
                    return String.Empty;
            }
        }
        #endregion

        #region ManageQuestions
        public Grid GetDetailsGrid(ManageQuestionsUserControl manageInstance)
        {
            this.manageInstance = manageInstance;
            ClearGrids();
            extendedDetailsGrid = this.GetExtendedDetailsGrid();
            SetContent();
            SetGroupNames();
            SetHandlers();
            SetLayout();
            SetColumsRows();
            AddUIElementsToBaseGrid();
            SetRowsAllDetailsGrid();
            AddGridsToAllDetailsGrid();
            return allDetailsGrid;
        }

        private void AddGridsToAllDetailsGrid()
        {
            allDetailsGrid.Children.Add(baseDetailsGrid);
            allDetailsGrid.Children.Add(extendedDetailsGrid);
        }

        private void SetRowsAllDetailsGrid()
        {
            RowDefinition row1 = new RowDefinition();
            row1.Height = new GridLength(172, GridUnitType.Pixel);
            RowDefinition row2 = new RowDefinition();
            row2.Height = new GridLength(235, GridUnitType.Pixel);
            allDetailsGrid.RowDefinitions.Add(row1);
            allDetailsGrid.RowDefinitions.Add(row2);
            Grid.SetRow(baseDetailsGrid, 0);
            Grid.SetRow(extendedDetailsGrid, 1);
        }

        private void AddUIElementsToBaseGrid()
        {
            baseDetailsGrid.Children.Add(courseLabel);
            baseDetailsGrid.Children.Add(courseMathRadioButton);
            baseDetailsGrid.Children.Add(courseLanguageRadioButton);
            baseDetailsGrid.Children.Add(courseKnowledgeRadioButton);
            baseDetailsGrid.Children.Add(difficultyLabel);
            baseDetailsGrid.Children.Add(difficulty1RadioButton);
            baseDetailsGrid.Children.Add(difficulty2RadioButton);
            baseDetailsGrid.Children.Add(difficulty3RadioButton);
            baseDetailsGrid.Children.Add(questionTypeLabel);
            baseDetailsGrid.Children.Add(questionTypeORadioButton);
            baseDetailsGrid.Children.Add(questionTypeMRadioButton);
            baseDetailsGrid.Children.Add(questionTypeDRadioButton);
            baseDetailsGrid.Children.Add(questionTypeTRadioButton);
            baseDetailsGrid.Children.Add(questionLabel);
            baseDetailsGrid.Children.Add(questionTextBox);
        }

        private void SetColumsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(110, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(130, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(180, GridUnitType.Pixel);
            ColumnDefinition c4 = new ColumnDefinition();
            c4.Width = new GridLength(340, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(35, GridUnitType.Pixel);
            baseDetailsGrid.ColumnDefinitions.Add(c1);
            baseDetailsGrid.ColumnDefinitions.Add(c2);
            baseDetailsGrid.ColumnDefinitions.Add(c3);
            baseDetailsGrid.ColumnDefinitions.Add(c4);
            baseDetailsGrid.RowDefinitions.Add(r1);
            baseDetailsGrid.RowDefinitions.Add(r2);
            baseDetailsGrid.RowDefinitions.Add(r3);
            baseDetailsGrid.RowDefinitions.Add(r4);
            baseDetailsGrid.RowDefinitions.Add(r5);

            Grid.SetColumn(courseLabel, 0);
            Grid.SetRow(courseLabel, 0);
            Grid.SetColumn(courseMathRadioButton, 0);
            Grid.SetRow(courseMathRadioButton, 1);
            Grid.SetColumn(courseLanguageRadioButton, 0);
            Grid.SetRow(courseLanguageRadioButton, 2);
            Grid.SetColumn(courseKnowledgeRadioButton, 0);
            Grid.SetRow(courseKnowledgeRadioButton, 3);
            Grid.SetColumn(difficultyLabel, 1);
            Grid.SetRow(difficultyLabel, 0);
            Grid.SetColumn(difficulty1RadioButton, 1);
            Grid.SetRow(difficulty1RadioButton, 1);
            Grid.SetColumn(difficulty2RadioButton, 1);
            Grid.SetRow(difficulty2RadioButton, 2);
            Grid.SetColumn(difficulty3RadioButton, 1);
            Grid.SetRow(difficulty3RadioButton, 3);
            Grid.SetColumn(questionTypeLabel, 2);
            Grid.SetRow(questionTypeLabel, 0);
            Grid.SetColumn(questionTypeORadioButton, 2);
            Grid.SetRow(questionTypeORadioButton, 1);
            Grid.SetColumn(questionTypeMRadioButton, 2);
            Grid.SetRow(questionTypeMRadioButton, 2);
            Grid.SetColumn(questionTypeDRadioButton, 2);
            Grid.SetRow(questionTypeDRadioButton, 3);
            Grid.SetColumn(questionTypeTRadioButton, 2);
            Grid.SetRow(questionTypeTRadioButton, 4);
            Grid.SetColumn(questionLabel, 3);
            Grid.SetRow(questionLabel, 0);
            Grid.SetColumn(questionTextBox, 3);
            Grid.SetRow(questionTextBox, 1);
            Grid.SetRowSpan(questionTextBox, 2);
        }

        private void SetLayout()
        {
            courseLabel.FontWeight = FontWeights.Bold;
            courseLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            courseLabel.Background = Constants.HighlightBrush1;
            courseLabel.Margin = Constants.ThicknessFive;
            difficultyLabel.FontWeight = FontWeights.Bold;
            difficultyLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            difficultyLabel.Background = Constants.HighlightBrush1;
            difficultyLabel.Margin = Constants.ThicknessFive;
            questionTypeLabel.FontWeight = FontWeights.Bold;
            questionTypeLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            questionTypeLabel.Background = Constants.HighlightBrush1;
            questionTypeLabel.Margin = Constants.ThicknessFive;
            difficultyLabel.ToolTip = Constants.DifficultyToolTipString;
            questionLabel.FontWeight = FontWeights.Bold;
            questionLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            questionLabel.Margin = Constants.ThicknessFive;
            questionLabel.Background = Constants.HighlightBrush1;
            questionTextBox.HorizontalAlignment = HorizontalAlignment.Stretch;
            questionTextBox.Margin = Constants.ThicknessFive;
            questionTextBox.AcceptsReturn = true;
            questionTextBox.TextWrapping = TextWrapping.Wrap;
            questionTextBox.Height = 80;
            questionTextBox.VerticalContentAlignment = VerticalAlignment.Top;
            courseMathRadioButton.Margin = Constants.ThicknessFive;
            courseLanguageRadioButton.Margin = Constants.ThicknessFive;
            courseKnowledgeRadioButton.Margin = Constants.ThicknessFive;
            difficulty1RadioButton.Margin = Constants.ThicknessFive;
            difficulty2RadioButton.Margin = Constants.ThicknessFive;
            difficulty3RadioButton.Margin = Constants.ThicknessFive;
            questionTypeORadioButton.Margin = Constants.ThicknessFive;
            questionTypeMRadioButton.Margin = Constants.ThicknessFive;
            questionTypeDRadioButton.Margin = Constants.ThicknessFive;
            questionTypeTRadioButton.Margin = Constants.ThicknessFive;
            extendedDetailsGrid.Margin = Constants.ThicknessTenBottom;
        }

        private void SetHandlers()
        {
            courseMathRadioButton.Checked += CourseCheckHandler;
            courseLanguageRadioButton.Checked += CourseCheckHandler;
            courseKnowledgeRadioButton.Checked += CourseCheckHandler;
            questionTypeORadioButton.Checked += QuestionTypeCheckedHandler;
            questionTypeMRadioButton.Checked += QuestionTypeCheckedHandler;
            questionTypeDRadioButton.Checked += QuestionTypeCheckedHandler;
            questionTypeTRadioButton.Checked += QuestionTypeCheckedHandler;
            difficulty1RadioButton.Checked += DifficultyCheckHandler;
            difficulty2RadioButton.Checked += DifficultyCheckHandler;
            difficulty3RadioButton.Checked += DifficultyCheckHandler;
            questionTextBox.TextChanged += questionTextBox_TextChanged;
        }

        private void SetGroupNames()
        {
            courseMathRadioButton.GroupName = Constants.CourseRadioButtonGroupname;
            courseLanguageRadioButton.GroupName = Constants.CourseRadioButtonGroupname;
            courseKnowledgeRadioButton.GroupName = Constants.CourseRadioButtonGroupname;
            questionTypeORadioButton.GroupName = Constants.QuestionTypeRadioButtonGroupname;
            questionTypeMRadioButton.GroupName = Constants.QuestionTypeRadioButtonGroupname;
            questionTypeDRadioButton.GroupName = Constants.QuestionTypeRadioButtonGroupname;
            questionTypeTRadioButton.GroupName = Constants.QuestionTypeRadioButtonGroupname;
            difficulty1RadioButton.GroupName = Constants.DifficultyRadioButtonGroupname;
            difficulty2RadioButton.GroupName = Constants.DifficultyRadioButtonGroupname;
            difficulty3RadioButton.GroupName = Constants.DifficultyRadioButtonGroupname;
        }

        private void SetContent()
        {
            courseLabel.Content = Constants.CourseLabelString;
            difficultyLabel.Content = Constants.DifficultyLabelString;
            questionTypeLabel.Content = Constants.QuestionTypeLabelString;
            questionLabel.Content = Constants.QuestionLabelString;
            courseMathRadioButton.Name = Constants.CourseMathRadioButtonName;
            courseLanguageRadioButton.Name = Constants.CourseLanguageRadioButtonName;
            courseKnowledgeRadioButton.Name = Constants.CourseKnowledgeRadioButtonName;
            courseMathRadioButton.Content = Constants.CourseMathName;
            courseLanguageRadioButton.Content = Constants.CourseLanguageName;
            courseKnowledgeRadioButton.Content = Constants.CourseKnowledgeName;
            questionTypeORadioButton.Name = Constants.QuestionTypeORadioButtonName;
            questionTypeMRadioButton.Name = Constants.QuestionTypeMRadioButtonName;
            questionTypeDRadioButton.Name = Constants.QuestionTypeDRadioButtonName;
            questionTypeTRadioButton.Name = Constants.QuestionTypeTRadioButtonName;
            difficulty1RadioButton.Name = Constants.Difficulty1RadioButtonName;
            difficulty2RadioButton.Name = Constants.Difficulty2RadioButtonName;
            difficulty3RadioButton.Name = Constants.Difficulty3RadioButtonName;
            difficulty1RadioButton.Content = Constants.OneString;
            difficulty2RadioButton.Content = Constants.TwoString;
            difficulty3RadioButton.Content = Constants.ThreeString;
            questionTypeORadioButton.Content = Constants.QuestionTypeORadioButtonContent;
            questionTypeMRadioButton.Content = Constants.QuestionTypeMRadioButtonContent;
            questionTypeDRadioButton.Content = Constants.QuestionTypeDRadioButtonContent;
            questionTypeTRadioButton.Content = Constants.QuestionTypeTRadioButtonContent;
            switch (this.course)
            {
                case Constants.MathCourse:
                    courseMathRadioButton.IsChecked = true;
                    questionTypeTRadioButton.Visibility = Visibility.Visible;
                    break;
                case Constants.LanguageCourse:
                    courseLanguageRadioButton.IsChecked = true;
                    questionTypeTRadioButton.Visibility = Visibility.Hidden;
                    break;
                case Constants.KnowledgeCourse:
                    courseKnowledgeRadioButton.IsChecked = true;
                    questionTypeTRadioButton.Visibility = Visibility.Hidden;
                    break;
            }
            switch (this.difficulty)
            {
                case 1:
                    difficulty1RadioButton.IsChecked = true;
                    break;
                case 2:
                    difficulty2RadioButton.IsChecked = true;
                    break;
                case 3:
                    difficulty3RadioButton.IsChecked = true;
                    break;
            }
            switch (this.questionType)
            {
                case Constants.OneChoiceQuestionType:
                    questionTypeORadioButton.IsChecked = true;
                    break;
                case Constants.MultipleChoiceQuestionType:
                    questionTypeMRadioButton.IsChecked = true;
                    break;
                case Constants.DragDropQuestionType:
                    questionTypeDRadioButton.IsChecked = true;
                    break;
                case Constants.TableQuestionType:
                    questionTypeTRadioButton.IsChecked = true;
                    break;
            }
            extendedDetailsGrid = this.GetExtendedDetailsGrid();
            questionTextBox.Text = this.question;
        }

        private void ClearGrids()
        {
            allDetailsGrid.Children.Clear();
            baseDetailsGrid.Children.Clear();
            extendedDetailsGrid.Children.Clear();
        }

        #region Handlers

        private void questionTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.question = questionTextBox.Text;
            manageInstance.SetSaveButtonEnabled();
        }

        private void DifficultyCheckHandler(object sender, RoutedEventArgs e)
        {
            RadioButton temp = (RadioButton)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.Difficulty1RadioButtonName:
                    this.difficulty = 1;
                    break;
                case Constants.Difficulty2RadioButtonName:
                    this.difficulty = 2;
                    break;
                case Constants.Difficulty3RadioButtonName:
                    this.difficulty = 3;
                    break;
            }
        }

        private void CourseCheckHandler(object sender, RoutedEventArgs e)
        {
            RadioButton temp = (RadioButton)sender;
            switch (temp.Name)
            {
                case Constants.CourseMathRadioButtonName:
                    questionTypeTRadioButton.Visibility = Visibility.Visible;
                    this.course = Constants.MathCourse;
                    break;
                case Constants.CourseLanguageRadioButtonName:
                    questionTypeTRadioButton.Visibility = Visibility.Hidden;
                    this.course = Constants.LanguageCourse;
                    break;
                case Constants.CourseKnowledgeRadioButtonName:
                     questionTypeTRadioButton.Visibility = Visibility.Hidden;
                    this.course = Constants.KnowledgeCourse;
                    break;
            }
            manageInstance.SetSaveButtonEnabled();
            manageInstance.ButtonToClick(this.course);
            manageInstance.UpdateCourseList(this.course);
        }

        private void QuestionTypeCheckedHandler(object sender, RoutedEventArgs e)
        {
            /*
             * Calls the interface method to replace the old object with a new one
             */
            RadioButton temp = (RadioButton)sender;            
            switch (temp.Name)
            {
                case Constants.QuestionTypeORadioButtonName:
                    if (this.questionType != Constants.OneChoiceQuestionType)
                    {
                        iReplaceProperty.OnReplaceRequested(this,Constants.OneChoiceQuestionType);
                    }                    
                    break;
                case Constants.QuestionTypeMRadioButtonName:
                    if (this.questionType != Constants.MultipleChoiceQuestionType)
                    {
                        iReplaceProperty.OnReplaceRequested(this, Constants.MultipleChoiceQuestionType);
                    }                    
                    break;
                case Constants.QuestionTypeDRadioButtonName:
                    if (this.questionType != Constants.DragDropQuestionType)
                    {
                        iReplaceProperty.OnReplaceRequested(this, Constants.DragDropQuestionType); 
                    }
                    break;
                case Constants.QuestionTypeTRadioButtonName:
                    if (this.questionType != Constants.TableQuestionType)
                    {
                        iReplaceProperty.OnReplaceRequested(this, Constants.TableQuestionType);     
                    }
                    break;
            }
            CheckQuestionTypeChange();
        }

        private void CheckQuestionTypeChange()
        {
            if (this.course == Constants.MathCourse){
                questionTypeTRadioButton.Visibility = Visibility.Visible;
            }
            else
            {
                questionTypeTRadioButton.Visibility = Visibility.Hidden;
            }
        }
        #endregion
        #endregion

        #region Quiz
        public Grid GetQuizDetailsGrid(TakeQuizUserControl quizInstance)
        {
            this.quizInstance = quizInstance;
            ClearQuizGrids();
            SetQuizContent();
            quizExtendedDetailsGrid = this.GetQuizExtendedDetailsGrid();
            SetQuizLayout();
            SetQuizColumnsRows();
            AddQuizGridUIElements();
            SetColumnsRowsQuizAllDetailsGrid();
            AddGridsToAllQuizDetailsGrid();
            return quizAllDetailsGrid;
        }

        private void AddGridsToAllQuizDetailsGrid()
        {
            quizAllDetailsGrid.Children.Add(quizBaseDetailsGrid);
            quizAllDetailsGrid.Children.Add(quizExtendedDetailsGrid);
        }

        private void SetColumnsRowsQuizAllDetailsGrid()
        {
            RowDefinition row1 = new RowDefinition();
            row1.Height = new GridLength(172, GridUnitType.Pixel);
            RowDefinition row2 = new RowDefinition();
            row2.Height = new GridLength(235, GridUnitType.Pixel);
            quizAllDetailsGrid.RowDefinitions.Add(row1);
            quizAllDetailsGrid.RowDefinitions.Add(row2);
            Grid.SetRow(quizBaseDetailsGrid, 0);
            Grid.SetRow(quizExtendedDetailsGrid, 1);
        }

        private void AddQuizGridUIElements()
        {
            quizBaseDetailsGrid.Children.Add(quizQuestionLabel);
            quizBaseDetailsGrid.Children.Add(quizQuestionTextBox);
        }

        private void SetQuizColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(760, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            quizBaseDetailsGrid.ColumnDefinitions.Add(c1);
            quizBaseDetailsGrid.RowDefinitions.Add(r1);
            quizBaseDetailsGrid.RowDefinitions.Add(r2);
            Grid.SetColumn(quizQuestionLabel, 0);
            Grid.SetRow(quizQuestionLabel, 0);
            Grid.SetColumn(quizQuestionTextBox, 0);
            Grid.SetRow(quizQuestionTextBox, 1);
        }

        private void SetQuizLayout()
        {
            quizQuestionTextBox.IsEnabled = false;
            quizQuestionTextBox.Foreground = Constants.BlackBrush;
            quizQuestionTextBox.VerticalContentAlignment = VerticalAlignment.Center;
            quizQuestionTextBox.Margin = Constants.ThicknessFive;
            quizQuestionLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizQuestionLabel.Margin = Constants.ThicknessFive;
            quizQuestionLabel.FontWeight = FontWeights.Bold;
            quizQuestionLabel.Background = Constants.HighlightBrush1;
            quizQuestionTextBox.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAllDetailsGrid.HorizontalAlignment = HorizontalAlignment.Center;
        }

        private void SetQuizContent()
        {
            quizQuestionLabel.Content = Constants.QuestionLabelString;
            quizQuestionTextBox.Text = this.question;
        }

        private void ClearQuizGrids()
        {
            quizAllDetailsGrid.Children.Clear();
            quizBaseDetailsGrid.Children.Clear();
            quizExtendedDetailsGrid.Children.Clear();
        }
        #endregion  

        #region Result
        public TextBlock GetQuestionResult
        {
            get
            {
                this.questionResult.FontSize = 11;
                this.questionResult.FontFamily = new FontFamily("Courier New");
                this.questionResult.Padding = Constants.ThicknessFive;
                return this.questionResult;
            }
        }

        public String ToResult()
        {
            String dateToday = DateTime.Now.ToString("dd/MM/yyyy");
            return String.Format("{0};{1};{2};{3}", dateToday, this.course, this.questionScore, this.question);
        }
        #endregion

        #region Abstract Methods
        public abstract String ToFile();
        public abstract Grid GetExtendedDetailsGrid();
        public abstract Grid GetQuizExtendedDetailsGrid();
        public abstract void GenerateResult();
        #endregion

    }
}
