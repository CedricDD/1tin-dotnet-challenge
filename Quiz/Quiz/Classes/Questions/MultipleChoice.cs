﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
#endregion

namespace Quiz
{
    class MultipleChoice : Question
    {
        #region Properties
        protected String[] possibleAnswers = new String[5];
        protected bool[] correctAnswers = new bool[5];
        protected bool quizUserAnswer1 = false;
        protected bool quizUserAnswer2 = false;
        protected bool quizUserAnswer3 = false;
        protected bool quizUserAnswer4 = false;
        protected bool quizUserAnswer5 = false;
        private bool didUserAnswerQuiz = false;
        private Grid extendedDetailsGrid = new Grid();
        private Grid quizExtendedDetailsGrid = new Grid();
        private Label multipleAnswersALabel = new Label();
        private Label multipleAnswersBLabel = new Label();
        private TextBox answerA1TextBox = new TextBox();
        private TextBox answerA2TextBox = new TextBox();
        private TextBox answerA3TextBox = new TextBox();
        private TextBox answerA4TextBox = new TextBox();
        private TextBox answerA5TextBox = new TextBox();
        private CheckBox answerB1CheckBox = new CheckBox();
        private CheckBox answerB2CheckBox = new CheckBox();
        private CheckBox answerB3CheckBox = new CheckBox();
        private CheckBox answerB4CheckBox = new CheckBox();
        private CheckBox answerB5CheckBox = new CheckBox();
        private Label quizAnswersALabel = new Label();
        private Label quizAnswersBLabel = new Label();
        private Label quizAnswerA1Label = new Label();
        private Label quizAnswerA2Label = new Label();
        private Label quizAnswerA3Label = new Label();
        private Label quizAnswerA4Label = new Label();
        private Label quizAnswerA5Label = new Label();
        private CheckBox quizAnswerB1CheckBox = new CheckBox();
        private CheckBox quizAnswerB2CheckBox = new CheckBox();
        private CheckBox quizAnswerB3CheckBox = new CheckBox();
        private CheckBox quizAnswerB4CheckBox = new CheckBox();
        private CheckBox quizAnswerB5CheckBox = new CheckBox();
        private int pointPerCorrectAnswerMC = pointPerCorrectAnswer / 5;
        #endregion

        #region Constructors
        public MultipleChoice(char course, int difficulty, char questionType, String question, String[] possibleAnswers, bool[] correctAnswers)
            : base(course, difficulty, questionType, question)
        {
            this.possibleAnswers = possibleAnswers;
            this.correctAnswers = correctAnswers;

        }

        public MultipleChoice(MultipleChoice o) //Copy Constructor
            : base(o.GetCourse, o.GetDifficulty, o.GetQuestionType, o.GetQuestion)
        {
            this.possibleAnswers = o.GetPossibleAnswers();
            this.correctAnswers = o.GetCorrectAnswers();
        }
        #endregion

        #region Object Methods
        public String[] GetPossibleAnswers()
        {
            return possibleAnswers;
        }

        public void SetPossibleAnswers(String[] possibleAnswers)
        {
            this.possibleAnswers = possibleAnswers;
        }

        public bool[] GetCorrectAnswers()
        {
            return correctAnswers;
        }

        public void SetCorrectAnswers(bool[] correctAnswers)
        {
            this.correctAnswers = correctAnswers;
        }

        public override String ToString()
        {
            return this.question;
        }

        public override String ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", this.course, this.difficulty, this.questionType, this.question, possibleAnswers[0], possibleAnswers[1],
                possibleAnswers[2], possibleAnswers[3], possibleAnswers[4], correctAnswers[0], correctAnswers[1], correctAnswers[2], correctAnswers[3], correctAnswers[4]);
        }
        #endregion

        #region ManageQuestions
        public override Grid GetExtendedDetailsGrid()
        {
            extendedDetailsGrid.Children.Clear();
            SetContent();
            SetLayout();
            SetHandlers();
            SetColumnsRows();
            AddUIElementsToGrid();
            return extendedDetailsGrid;
        }

        private void AddUIElementsToGrid()
        {
            extendedDetailsGrid.Children.Add(multipleAnswersALabel);
            extendedDetailsGrid.Children.Add(multipleAnswersBLabel);
            extendedDetailsGrid.Children.Add(answerA1TextBox);
            extendedDetailsGrid.Children.Add(answerA2TextBox);
            extendedDetailsGrid.Children.Add(answerA3TextBox);
            extendedDetailsGrid.Children.Add(answerA4TextBox);
            extendedDetailsGrid.Children.Add(answerA5TextBox);
            extendedDetailsGrid.Children.Add(answerB1CheckBox);
            extendedDetailsGrid.Children.Add(answerB2CheckBox);
            extendedDetailsGrid.Children.Add(answerB3CheckBox);
            extendedDetailsGrid.Children.Add(answerB4CheckBox);
            extendedDetailsGrid.Children.Add(answerB5CheckBox);
        }

        private void SetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(380, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(380, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r6 = new RowDefinition();
            r6.Height = new GridLength(35, GridUnitType.Pixel);

            extendedDetailsGrid.ColumnDefinitions.Add(c1);
            extendedDetailsGrid.ColumnDefinitions.Add(c2);
            extendedDetailsGrid.RowDefinitions.Add(r1);
            extendedDetailsGrid.RowDefinitions.Add(r2);
            extendedDetailsGrid.RowDefinitions.Add(r3);
            extendedDetailsGrid.RowDefinitions.Add(r4);
            extendedDetailsGrid.RowDefinitions.Add(r5);
            extendedDetailsGrid.RowDefinitions.Add(r6);

            Grid.SetColumn(multipleAnswersALabel, 0);
            Grid.SetRow(multipleAnswersALabel, 0);
            Grid.SetColumn(multipleAnswersBLabel, 1);
            Grid.SetRow(multipleAnswersBLabel, 0);

            Grid.SetColumn(answerA1TextBox, 0);
            Grid.SetRow(answerA1TextBox, 1);
            Grid.SetColumn(answerA2TextBox, 0);
            Grid.SetRow(answerA2TextBox, 2);
            Grid.SetColumn(answerA3TextBox, 0);
            Grid.SetRow(answerA3TextBox, 3);
            Grid.SetColumn(answerA4TextBox, 0);
            Grid.SetRow(answerA4TextBox, 4);
            Grid.SetColumn(answerA5TextBox, 0);
            Grid.SetRow(answerA5TextBox, 5);

            Grid.SetColumn(answerB1CheckBox, 1);
            Grid.SetRow(answerB1CheckBox, 1);
            Grid.SetColumn(answerB2CheckBox, 1);
            Grid.SetRow(answerB2CheckBox, 2);
            Grid.SetColumn(answerB3CheckBox, 1);
            Grid.SetRow(answerB3CheckBox, 3);
            Grid.SetColumn(answerB4CheckBox, 1);
            Grid.SetRow(answerB4CheckBox, 4);
            Grid.SetColumn(answerB5CheckBox, 1);
            Grid.SetRow(answerB5CheckBox, 5);
        }

        private void SetHandlers()
        {
            answerA1TextBox.TextChanged += TextChangedHandler;
            answerA2TextBox.TextChanged += TextChangedHandler;
            answerA3TextBox.TextChanged += TextChangedHandler;
            answerA4TextBox.TextChanged += TextChangedHandler;
            answerA5TextBox.TextChanged += TextChangedHandler;
            answerB1CheckBox.Checked += CheckedHandler;
            answerB2CheckBox.Checked += CheckedHandler;
            answerB3CheckBox.Checked += CheckedHandler;
            answerB4CheckBox.Checked += CheckedHandler;
            answerB5CheckBox.Checked += CheckedHandler;
            answerB1CheckBox.Unchecked += UncheckedHandler;
            answerB2CheckBox.Unchecked += UncheckedHandler;
            answerB3CheckBox.Unchecked += UncheckedHandler;
            answerB4CheckBox.Unchecked += UncheckedHandler;
            answerB5CheckBox.Unchecked += UncheckedHandler;
        }

        private void SetLayout()
        {
            multipleAnswersALabel.FontWeight = FontWeights.Bold;
            multipleAnswersALabel.Background = Constants.HighlightBrush1;
            multipleAnswersALabel.Margin = Constants.ThicknessFive;
            multipleAnswersBLabel.FontWeight = FontWeights.Bold;
            multipleAnswersBLabel.Background = Constants.HighlightBrush1;
            multipleAnswersBLabel.Margin = Constants.ThicknessFive;
            multipleAnswersALabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            multipleAnswersBLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            answerB1CheckBox.VerticalAlignment = VerticalAlignment.Center;
            answerB2CheckBox.VerticalAlignment = VerticalAlignment.Center;
            answerB3CheckBox.VerticalAlignment = VerticalAlignment.Center;
            answerB4CheckBox.VerticalAlignment = VerticalAlignment.Center;
            answerB5CheckBox.VerticalAlignment = VerticalAlignment.Center;
            ScaleTransform trans = new ScaleTransform();
            trans.ScaleX = Constants.OnePointSeven;
            trans.ScaleY = Constants.OnePointSeven;
            answerB1CheckBox.LayoutTransform = trans;
            answerB2CheckBox.LayoutTransform = trans;
            answerB3CheckBox.LayoutTransform = trans;
            answerB4CheckBox.LayoutTransform = trans;
            answerB5CheckBox.LayoutTransform = trans;
            answerA1TextBox.Margin = Constants.ThicknessFive;
            answerA2TextBox.Margin = Constants.ThicknessFive;
            answerA3TextBox.Margin = Constants.ThicknessFive;
            answerA4TextBox.Margin = Constants.ThicknessFive;
            answerA5TextBox.Margin = Constants.ThicknessFive;
            answerB1CheckBox.Margin = Constants.ThicknessFive;
            answerB2CheckBox.Margin = Constants.ThicknessFive;
            answerB3CheckBox.Margin = Constants.ThicknessFive;
            answerB4CheckBox.Margin = Constants.ThicknessFive;
            answerB5CheckBox.Margin = Constants.ThicknessFive;
        }

        private void SetContent()
        {
            multipleAnswersALabel.Content = Constants.MultipleAnswerALabelContent;
            multipleAnswersBLabel.Content = Constants.MultipleAnswerBLabelContent;
            answerA1TextBox.Text = possibleAnswers[0];
            answerA2TextBox.Text = possibleAnswers[1];
            answerA3TextBox.Text = possibleAnswers[2];
            answerA4TextBox.Text = possibleAnswers[3];
            answerA5TextBox.Text = possibleAnswers[4];
            answerB1CheckBox.IsChecked = correctAnswers[0];
            answerB2CheckBox.IsChecked = correctAnswers[1];
            answerB3CheckBox.IsChecked = correctAnswers[2];
            answerB4CheckBox.IsChecked = correctAnswers[3];
            answerB5CheckBox.IsChecked = correctAnswers[4];
            answerA1TextBox.Name = Constants.AnswerA1TextBoxName;
            answerA2TextBox.Name = Constants.AnswerA2TextBoxName;
            answerA3TextBox.Name = Constants.AnswerA3TextBoxName;
            answerA4TextBox.Name = Constants.AnswerA4TextBoxName;
            answerA5TextBox.Name = Constants.AnswerA5TextBoxName;
            answerB1CheckBox.Name = Constants.AnswerB1CheckBoxName;
            answerB2CheckBox.Name = Constants.AnswerB2CheckBoxName;
            answerB3CheckBox.Name = Constants.AnswerB3CheckBoxName;
            answerB4CheckBox.Name = Constants.AnswerB4CheckBoxName;
            answerB5CheckBox.Name = Constants.AnswerB5CheckBoxName;
        }

        #region Handlers
        private void UncheckedHandler(object sender, RoutedEventArgs e)
        {
            CheckBox temp = (CheckBox)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.AnswerB1CheckBoxName:
                    this.correctAnswers[0] = false;
                    break;
                case Constants.AnswerB2CheckBoxName:
                    this.correctAnswers[1] = false;
                    break;
                case Constants.AnswerB3CheckBoxName:
                    this.correctAnswers[2] = false;
                    break;
                case Constants.AnswerB4CheckBoxName:
                    this.correctAnswers[3] = false;
                    break;
                case Constants.AnswerB5CheckBoxName:
                    this.correctAnswers[4] = false;
                    break;
            }
        }

        private void CheckedHandler(object sender, RoutedEventArgs e)
        {
            CheckBox temp = (CheckBox)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.AnswerB1CheckBoxName:
                    this.correctAnswers[0] = true;
                    break;
                case Constants.AnswerB2CheckBoxName:
                    this.correctAnswers[1] = true;
                    break;
                case Constants.AnswerB3CheckBoxName:
                    this.correctAnswers[2] = true;
                    break;
                case Constants.AnswerB4CheckBoxName:
                    this.correctAnswers[3] = true;
                    break;
                case Constants.AnswerB5CheckBoxName:
                    this.correctAnswers[4] = true;
                    break;
            }
        }

        private void TextChangedHandler(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.AnswerA1TextBoxName:
                    this.possibleAnswers[0] = answerA1TextBox.Text;
                    break;
                case Constants.AnswerA2TextBoxName:
                    this.possibleAnswers[1] = answerA2TextBox.Text;
                    break;
                case Constants.AnswerA3TextBoxName:
                    this.possibleAnswers[2] = answerA3TextBox.Text;
                    break;
                case Constants.AnswerA4TextBoxName:
                    this.possibleAnswers[3] = answerA4TextBox.Text;
                    break;
                case Constants.AnswerA5TextBoxName:
                    this.possibleAnswers[4] = answerA5TextBox.Text;
                    break;

            }
        }
        #endregion
        #endregion

        #region Quiz
        public override Grid GetQuizExtendedDetailsGrid()
        {
            quizExtendedDetailsGrid.Children.Clear();
            QuizSetContent();
            QuizSetHandlers();
            QuizSetLayout();
            QuizSetColumnsRows();
            QuizAddUIElementsToGrid();
            return quizExtendedDetailsGrid;
        }

        private void QuizAddUIElementsToGrid()
        {
            quizExtendedDetailsGrid.Children.Add(quizAnswersALabel);
            quizExtendedDetailsGrid.Children.Add(quizAnswersBLabel);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA1Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA2Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA3Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA4Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA5Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB1CheckBox);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB2CheckBox);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB3CheckBox);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB4CheckBox);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB5CheckBox);
        }

        private void QuizSetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(350, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(60, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(350, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r6 = new RowDefinition();
            r6.Height = new GridLength(40, GridUnitType.Pixel);

            quizExtendedDetailsGrid.ColumnDefinitions.Add(c1);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c2);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c3);
            quizExtendedDetailsGrid.RowDefinitions.Add(r1);
            quizExtendedDetailsGrid.RowDefinitions.Add(r2);
            quizExtendedDetailsGrid.RowDefinitions.Add(r3);
            quizExtendedDetailsGrid.RowDefinitions.Add(r4);
            quizExtendedDetailsGrid.RowDefinitions.Add(r5);
            quizExtendedDetailsGrid.RowDefinitions.Add(r6);

            Grid.SetColumn(quizAnswersALabel, 0);
            Grid.SetRow(quizAnswersALabel, 0);
            Grid.SetColumn(quizAnswersBLabel, 2);
            Grid.SetRow(quizAnswersBLabel, 0);

            Grid.SetColumn(quizAnswerA1Label, 0);
            Grid.SetRow(quizAnswerA1Label, 1);
            Grid.SetColumn(quizAnswerA2Label, 0);
            Grid.SetRow(quizAnswerA2Label, 2);
            Grid.SetColumn(quizAnswerA3Label, 0);
            Grid.SetRow(quizAnswerA3Label, 3);
            Grid.SetColumn(quizAnswerA4Label, 0);
            Grid.SetRow(quizAnswerA4Label, 4);
            Grid.SetColumn(quizAnswerA5Label, 0);
            Grid.SetRow(quizAnswerA5Label, 5);

            Grid.SetColumn(quizAnswerB1CheckBox, 2);
            Grid.SetRow(quizAnswerB1CheckBox, 1);
            Grid.SetColumn(quizAnswerB2CheckBox, 2);
            Grid.SetRow(quizAnswerB2CheckBox, 2);
            Grid.SetColumn(quizAnswerB3CheckBox, 2);
            Grid.SetRow(quizAnswerB3CheckBox, 3);
            Grid.SetColumn(quizAnswerB4CheckBox, 2);
            Grid.SetRow(quizAnswerB4CheckBox, 4);
            Grid.SetColumn(quizAnswerB5CheckBox, 2);
            Grid.SetRow(quizAnswerB5CheckBox, 5);
        }

        private void QuizSetLayout()
        {
            quizAnswersALabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswersALabel.Background = Constants.HighlightBrush1;
            quizAnswersALabel.Margin = Constants.ThicknessFive;
            quizAnswersBLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswersBLabel.Background = Constants.HighlightBrush1;
            quizAnswersBLabel.Margin = Constants.ThicknessFive;
            quizAnswersALabel.FontWeight = FontWeights.Bold;
            quizAnswersBLabel.FontWeight = FontWeights.Bold;
            quizAnswerA1Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerA2Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerA3Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerA4Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerA5Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerB1CheckBox.VerticalAlignment = VerticalAlignment.Center;
            quizAnswerB2CheckBox.VerticalAlignment = VerticalAlignment.Center;
            quizAnswerB3CheckBox.VerticalAlignment = VerticalAlignment.Center;
            quizAnswerB4CheckBox.VerticalAlignment = VerticalAlignment.Center;
            quizAnswerB5CheckBox.VerticalAlignment = VerticalAlignment.Center;
            quizAnswerA1Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA2Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA3Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA4Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA5Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB1CheckBox.BorderBrush = Constants.BlackBrush;
            quizAnswerB2CheckBox.BorderBrush = Constants.BlackBrush;
            quizAnswerB3CheckBox.BorderBrush = Constants.BlackBrush;
            quizAnswerB4CheckBox.BorderBrush = Constants.BlackBrush;
            quizAnswerB5CheckBox.BorderBrush = Constants.BlackBrush;
            quizAnswerA1Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA2Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA3Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA4Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA5Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB1CheckBox.BorderThickness = Constants.ThicknessOne;
            quizAnswerB2CheckBox.BorderThickness = Constants.ThicknessOne;
            quizAnswerB3CheckBox.BorderThickness = Constants.ThicknessOne;
            quizAnswerB4CheckBox.BorderThickness = Constants.ThicknessOne;
            quizAnswerB5CheckBox.BorderThickness = Constants.ThicknessOne;
            quizAnswerA1Label.Margin = Constants.ThicknessFive;
            quizAnswerA2Label.Margin = Constants.ThicknessFive;
            quizAnswerA3Label.Margin = Constants.ThicknessFive;
            quizAnswerA4Label.Margin = Constants.ThicknessFive;
            quizAnswerA5Label.Margin = Constants.ThicknessFive;
            quizAnswerB1CheckBox.Margin = Constants.ThicknessFive;
            quizAnswerB2CheckBox.Margin = Constants.ThicknessFive;
            quizAnswerB3CheckBox.Margin = Constants.ThicknessFive;
            quizAnswerB4CheckBox.Margin = Constants.ThicknessFive;
            quizAnswerB5CheckBox.Margin = Constants.ThicknessFive;
            ScaleTransform trans = new ScaleTransform();
            trans.ScaleX = Constants.OnePointSeven;
            trans.ScaleY = Constants.OnePointSeven;
            quizAnswerB1CheckBox.LayoutTransform = trans;
            quizAnswerB2CheckBox.LayoutTransform = trans;
            quizAnswerB3CheckBox.LayoutTransform = trans;
            quizAnswerB4CheckBox.LayoutTransform = trans;
            quizAnswerB5CheckBox.LayoutTransform = trans;
        }

        private void QuizSetHandlers()
        {
            quizAnswerB1CheckBox.Checked += quizAnswerCheckedHandler;
            quizAnswerB2CheckBox.Checked += quizAnswerCheckedHandler;
            quizAnswerB3CheckBox.Checked += quizAnswerCheckedHandler;
            quizAnswerB4CheckBox.Checked += quizAnswerCheckedHandler;
            quizAnswerB5CheckBox.Checked += quizAnswerCheckedHandler;
            quizAnswerB1CheckBox.Unchecked += quizAnswerUncheckedHandler;
            quizAnswerB2CheckBox.Unchecked += quizAnswerUncheckedHandler;
            quizAnswerB3CheckBox.Unchecked += quizAnswerUncheckedHandler;
            quizAnswerB4CheckBox.Unchecked += quizAnswerUncheckedHandler;
            quizAnswerB5CheckBox.Unchecked += quizAnswerUncheckedHandler;
        }

        private void QuizSetContent()
        {
            quizAnswersALabel.Content = Constants.MultipleAnswerALabelContent;
            quizAnswersBLabel.Content = Constants.MultipleAnswerBLabelContent;
            quizAnswerA1Label.Content = possibleAnswers[0];
            quizAnswerA2Label.Content = possibleAnswers[1];
            quizAnswerA3Label.Content = possibleAnswers[2];
            quizAnswerA4Label.Content = possibleAnswers[3];
            quizAnswerA5Label.Content = possibleAnswers[4];
            quizAnswerB1CheckBox.Name = Constants.QuizAnswerB1CheckBoxName;
            quizAnswerB2CheckBox.Name = Constants.QuizAnswerB2CheckBoxName;
            quizAnswerB3CheckBox.Name = Constants.QuizAnswerB3CheckBoxName;
            quizAnswerB4CheckBox.Name = Constants.QuizAnswerB4CheckBoxName;
            quizAnswerB5CheckBox.Name = Constants.QuizAnswerB5CheckBoxName;
        }

        #region Handlers
        private void quizAnswerUncheckedHandler(object sender, RoutedEventArgs e)
        {
            CheckBox temp = (CheckBox)sender;
            didUserAnswerQuiz = true;

            switch (temp.Name)
            {
                case Constants.QuizAnswerB1CheckBoxName:
                    this.quizUserAnswer1 = false;
                    break;
                case Constants.QuizAnswerB2CheckBoxName:
                    this.quizUserAnswer2 = false;
                    break;
                case Constants.QuizAnswerB3CheckBoxName:
                    this.quizUserAnswer3 = false;
                    break;
                case Constants.QuizAnswerB4CheckBoxName:
                    this.quizUserAnswer4 = false;
                    break;
                case Constants.QuizAnswerB5CheckBoxName:
                    this.quizUserAnswer5 = false;
                    break;
            }
        }

        private void quizAnswerCheckedHandler(object sender, RoutedEventArgs e)
        {
            CheckBox temp = (CheckBox)sender;
            didUserAnswerQuiz = true;

            switch (temp.Name)
            {
                case Constants.QuizAnswerB1CheckBoxName:
                    this.quizUserAnswer1 = true;
                    break;
                case Constants.QuizAnswerB2CheckBoxName:
                    this.quizUserAnswer2 = true;
                    break;
                case Constants.QuizAnswerB3CheckBoxName:
                    this.quizUserAnswer3 = true;
                    break;
                case Constants.QuizAnswerB4CheckBoxName:
                    this.quizUserAnswer4 = true;
                    break;
                case Constants.QuizAnswerB5CheckBoxName:
                    this.quizUserAnswer5 = true;
                    break;
            }
        }
        #endregion
        #endregion

        #region Result
        public override void GenerateResult()
        {
            this.questionResult.Text += String.Format("Vraag: {0}\n\n{1}{2}{3}\n", this.question, Constants.AnswerLabelContent.PadRight(40), Constants.RightAnswerText.PadRight(40), Constants.YourAnswerText);
            this.questionResult.Background = Constants.LightGreenBrush;

            if (didUserAnswerQuiz == true)
            {
                if (quizUserAnswer1.Equals(correctAnswers[0]))
                {
                    this.questionScore += pointPerCorrectAnswerMC;
                }
                else
                {
                    this.questionResult.Background = Constants.LightRedBrush;
                }
                if (quizUserAnswer2.Equals(correctAnswers[1]))
                {
                    this.questionScore += pointPerCorrectAnswerMC;
                }
                else
                {
                    this.questionResult.Background = Constants.LightRedBrush;
                }
                if (quizUserAnswer3.Equals(correctAnswers[2]))
                {
                    this.questionScore += pointPerCorrectAnswerMC;
                }
                else
                {
                    this.questionResult.Background = Constants.LightRedBrush;
                }
                if (quizUserAnswer4.Equals(correctAnswers[3]))
                {
                    this.questionScore += pointPerCorrectAnswerMC;
                }
                else
                {
                    this.questionResult.Background = Constants.LightRedBrush;
                }
                if (quizUserAnswer5.Equals(correctAnswers[4]))
                {
                    this.questionScore += pointPerCorrectAnswerMC;
                }
                else
                {
                    this.questionResult.Background = Constants.LightRedBrush;
                }
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }

            this.questionScore *= this.difficulty;
            this.questionResult.Text += String.Format("{0}{1}{2}\n", possibleAnswers[0].PadRight(40), BoolToStringNL(correctAnswers[0]).PadRight(40), BoolToStringNL(quizUserAnswer1));
            this.questionResult.Text += String.Format("{0}{1}{2}\n", possibleAnswers[1].PadRight(40), BoolToStringNL(correctAnswers[1]).PadRight(40), BoolToStringNL(quizUserAnswer2));
            this.questionResult.Text += String.Format("{0}{1}{2}\n", possibleAnswers[2].PadRight(40), BoolToStringNL(correctAnswers[2]).PadRight(40), BoolToStringNL(quizUserAnswer3));
            this.questionResult.Text += String.Format("{0}{1}{2}\n", possibleAnswers[3].PadRight(40), BoolToStringNL(correctAnswers[3]).PadRight(40), BoolToStringNL(quizUserAnswer4));
            this.questionResult.Text += String.Format("{0}{1}{2}\n", possibleAnswers[4].PadRight(40), BoolToStringNL(correctAnswers[4]).PadRight(40), BoolToStringNL(quizUserAnswer5));
        }

        private String BoolToStringNL(bool temp)
        {
            if (temp == true)
            {
                return Constants.Juist;
            }
            else
            {
                return Constants.Fout;
            }
        }
        #endregion
    }
}
