﻿#region ClassComment
/*
 * Created By Frankie Claessens - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
#endregion

namespace Quiz
{
    class DragNDrop : Question
    {
        #region Properties
        protected String[] dragAnswers = new String[5];
        protected String[] dropAnswers = new String[5];
        protected String quizUserAnswer1 = Constants.BarString;
        protected String quizUserAnswer2 = Constants.BarString;
        protected String quizUserAnswer3 = Constants.BarString;
        protected String quizUserAnswer4 = Constants.BarString;
        protected String quizUserAnswer5 = Constants.BarString;
        private Grid extendedDetailsGrid = new Grid();
        private Grid quizExtendedDetailsGrid = new Grid();
        private Label dragDropALabel = new Label();
        private Label dragDropBLabel = new Label();
        private TextBox answerA1TextBox = new TextBox();
        private TextBox answerA2TextBox = new TextBox();
        private TextBox answerA3TextBox = new TextBox();
        private TextBox answerA4TextBox = new TextBox();
        private TextBox answerA5TextBox = new TextBox();
        private TextBox answerB1TextBox = new TextBox();
        private TextBox answerB2TextBox = new TextBox();
        private TextBox answerB3TextBox = new TextBox();
        private TextBox answerB4TextBox = new TextBox();
        private TextBox answerB5TextBox = new TextBox();
        private Label quizDragDropALabel = new Label();
        private Label quizDragDropBLabel = new Label();
        private Label quizAnswerA1Label = new Label();
        private Label quizAnswerA2Label = new Label();
        private Label quizAnswerA3Label = new Label();
        private Label quizAnswerA4Label = new Label();
        private Label quizAnswerA5Label = new Label();
        private Label quizAnswerB1Label = new Label();
        private Label quizAnswerB2Label = new Label();
        private Label quizAnswerB3Label = new Label();
        private Label quizAnswerB4Label = new Label();
        private Label quizAnswerB5Label = new Label();
        private int pointPerCorrentAnswerDD = pointPerCorrectAnswer / 5;
        #endregion

        #region Constructors
        public DragNDrop(char course, int difficulty, char questionType, String question, String[] dragAnswers, String[] dropAnswers)
            : base(course, difficulty, questionType, question)
        {
            this.dragAnswers = dragAnswers;
            this.dropAnswers = dropAnswers;
        }
        public DragNDrop(DragNDrop o) //Copy Constructor
            : base(o.GetCourse, o.GetDifficulty, o.GetQuestionType, o.GetQuestion)
        {
            this.dragAnswers = o.GetDragAnswers();
            this.dropAnswers = o.GetDropAnswers();
        }
        #endregion

        #region Object Methods
        public String[] GetDragAnswers()
        {
            return dragAnswers;
        }

        public void SetDragAnswers(String[] dragAnswers)
        {
            this.dragAnswers = dragAnswers;
        }

        public String[] GetDropAnswers()
        {
            return dropAnswers;
        }

        public void SetDropAnswers(String[] dropAnswers)
        {
            this.dropAnswers = dropAnswers;
        }

        public override String ToString()
        {
            return this.question;
        }

        public override String ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6};{7};{8};{9};{10};{11};{12};{13}", this.course, this.difficulty, this.questionType, this.question, dragAnswers[0], dragAnswers[1],
                dragAnswers[2], dragAnswers[3], dragAnswers[4], dropAnswers[0], dropAnswers[1], dropAnswers[2], dropAnswers[3], dropAnswers[4]);
        }
        #endregion

        #region ManageQuestions
        public override Grid GetExtendedDetailsGrid()
        {
            extendedDetailsGrid.Children.Clear();
            SetContent();
            SetLayout();
            SetHandlers();
            SetColumnsRows();
            AddUIElementsToGrid();
            return extendedDetailsGrid;
        }

        private void AddUIElementsToGrid()
        {
            extendedDetailsGrid.Children.Add(dragDropALabel);
            extendedDetailsGrid.Children.Add(dragDropBLabel);
            extendedDetailsGrid.Children.Add(answerA1TextBox);
            extendedDetailsGrid.Children.Add(answerA2TextBox);
            extendedDetailsGrid.Children.Add(answerA3TextBox);
            extendedDetailsGrid.Children.Add(answerA4TextBox);
            extendedDetailsGrid.Children.Add(answerA5TextBox);
            extendedDetailsGrid.Children.Add(answerB1TextBox);
            extendedDetailsGrid.Children.Add(answerB2TextBox);
            extendedDetailsGrid.Children.Add(answerB3TextBox);
            extendedDetailsGrid.Children.Add(answerB4TextBox);
            extendedDetailsGrid.Children.Add(answerB5TextBox);
        }

        private void SetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(380, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(380, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r6 = new RowDefinition();
            r6.Height = new GridLength(35, GridUnitType.Pixel);
            RowDefinition r7 = new RowDefinition();
            r7.Height = new GridLength(25, GridUnitType.Pixel);

            extendedDetailsGrid.ColumnDefinitions.Add(c1);
            extendedDetailsGrid.ColumnDefinitions.Add(c2);
            extendedDetailsGrid.RowDefinitions.Add(r1);
            extendedDetailsGrid.RowDefinitions.Add(r2);
            extendedDetailsGrid.RowDefinitions.Add(r3);
            extendedDetailsGrid.RowDefinitions.Add(r4);
            extendedDetailsGrid.RowDefinitions.Add(r5);
            extendedDetailsGrid.RowDefinitions.Add(r6);
            extendedDetailsGrid.RowDefinitions.Add(r7);

            Grid.SetColumn(dragDropALabel, 0);
            Grid.SetRow(dragDropALabel, 0);
            Grid.SetColumn(dragDropBLabel, 1);
            Grid.SetRow(dragDropBLabel, 0);
            Grid.SetColumn(answerA1TextBox, 0);
            Grid.SetRow(answerA1TextBox, 1);
            Grid.SetColumn(answerA2TextBox, 0);
            Grid.SetRow(answerA2TextBox, 2);
            Grid.SetColumn(answerA3TextBox, 0);
            Grid.SetRow(answerA3TextBox, 3);
            Grid.SetColumn(answerA4TextBox, 0);
            Grid.SetRow(answerA4TextBox, 4);
            Grid.SetColumn(answerA5TextBox, 0);
            Grid.SetRow(answerA5TextBox, 5);
            Grid.SetColumn(answerB1TextBox, 1);
            Grid.SetRow(answerB1TextBox, 1);
            Grid.SetColumn(answerB2TextBox, 1);
            Grid.SetRow(answerB2TextBox, 2);
            Grid.SetColumn(answerB3TextBox, 1);
            Grid.SetRow(answerB3TextBox, 3);
            Grid.SetColumn(answerB4TextBox, 1);
            Grid.SetRow(answerB4TextBox, 4);
            Grid.SetColumn(answerB5TextBox, 1);
            Grid.SetRow(answerB5TextBox, 5);
        }

        private void SetHandlers()
        {
            answerA1TextBox.TextChanged += DragTextChangedHandler;
            answerA2TextBox.TextChanged += DragTextChangedHandler;
            answerA3TextBox.TextChanged += DragTextChangedHandler;
            answerA4TextBox.TextChanged += DragTextChangedHandler;
            answerA5TextBox.TextChanged += DragTextChangedHandler;
            answerB1TextBox.TextChanged += DropTextChangedHandler;
            answerB2TextBox.TextChanged += DropTextChangedHandler;
            answerB3TextBox.TextChanged += DropTextChangedHandler;
            answerB4TextBox.TextChanged += DropTextChangedHandler;
            answerB5TextBox.TextChanged += DropTextChangedHandler;
        }

        private void SetLayout()
        {
            dragDropALabel.FontWeight = FontWeights.Bold;
            dragDropBLabel.FontWeight = FontWeights.Bold;
            dragDropALabel.Background = Constants.HighlightBrush1;
            dragDropALabel.Margin = Constants.ThicknessFive;
            dragDropBLabel.Background = Constants.HighlightBrush1;
            dragDropBLabel.Margin = Constants.ThicknessFive;
            dragDropALabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            dragDropBLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            answerA1TextBox.Margin = Constants.ThicknessFive;
            answerA2TextBox.Margin = Constants.ThicknessFive;
            answerA3TextBox.Margin = Constants.ThicknessFive;
            answerA4TextBox.Margin = Constants.ThicknessFive;
            answerA5TextBox.Margin = Constants.ThicknessFive;
            answerB1TextBox.Margin = Constants.ThicknessFive;
            answerB2TextBox.Margin = Constants.ThicknessFive;
            answerB3TextBox.Margin = Constants.ThicknessFive;
            answerB4TextBox.Margin = Constants.ThicknessFive;
            answerB5TextBox.Margin = Constants.ThicknessFive;
        }

        private void SetContent()
        {
            dragDropALabel.Content = Constants.AnswersOne;
            dragDropBLabel.Content = Constants.AnswersTwo;
            answerA1TextBox.Text = dragAnswers[0];
            answerA2TextBox.Text = dragAnswers[1];
            answerA3TextBox.Text = dragAnswers[2];
            answerA4TextBox.Text = dragAnswers[3];
            answerA5TextBox.Text = dragAnswers[4];
            answerB1TextBox.Text = dropAnswers[0];
            answerB2TextBox.Text = dropAnswers[1];
            answerB3TextBox.Text = dropAnswers[2];
            answerB4TextBox.Text = dropAnswers[3];
            answerB5TextBox.Text = dropAnswers[4];
            answerA1TextBox.Name = Constants.AnswerA1TextBoxName;
            answerA2TextBox.Name = Constants.AnswerA2TextBoxName;
            answerA3TextBox.Name = Constants.AnswerA3TextBoxName;
            answerA4TextBox.Name = Constants.AnswerA4TextBoxName;
            answerA5TextBox.Name = Constants.AnswerA5TextBoxName;
            answerB1TextBox.Name = Constants.AnswerB1TextBoxName;
            answerB2TextBox.Name = Constants.AnswerB2TextBoxName;
            answerB3TextBox.Name = Constants.AnswerB3TextBoxName;
            answerB4TextBox.Name = Constants.AnswerB4TextBoxName;
            answerB5TextBox.Name = Constants.AnswerB5TextBoxName;;
        }

        #region Handlers
        private void DropTextChangedHandler(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.AnswerB1TextBoxName:
                    this.dropAnswers[0] = answerB1TextBox.Text;
                    break;
                case Constants.AnswerB2TextBoxName:
                    this.dropAnswers[1] = answerB2TextBox.Text;
                    break;
                case Constants.AnswerB3TextBoxName:
                    this.dropAnswers[2] = answerB3TextBox.Text;
                    break;
                case Constants.AnswerB4TextBoxName:
                    this.dropAnswers[3] = answerB4TextBox.Text;
                    break;
                case Constants.AnswerB5TextBoxName:
                    this.dropAnswers[4] = answerB5TextBox.Text;
                    break;
            }
        }

        private void DragTextChangedHandler(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            manageInstance.SetSaveButtonEnabled();
            switch (temp.Name)
            {
                case Constants.AnswerA1TextBoxName:
                    this.dragAnswers[0] = answerA1TextBox.Text;
                    break;
                case Constants.AnswerA2TextBoxName:
                    this.dragAnswers[1] = answerA2TextBox.Text;
                    break;
                case Constants.AnswerA3TextBoxName:
                    this.dragAnswers[2] = answerA3TextBox.Text;
                    break;
                case Constants.AnswerA4TextBoxName:
                    this.dragAnswers[3] = answerA4TextBox.Text;
                    break;
                case Constants.AnswerA5TextBoxName:
                    this.dragAnswers[4] = answerA5TextBox.Text;
                    break;
            }
        }
        #endregion
        #endregion

        #region Quiz

        public override Grid GetQuizExtendedDetailsGrid()
        {
            quizExtendedDetailsGrid.Children.Clear();
            QuizSetContent();
            QuizSetAllowDrop();
            QuizSetHandlers();
            QuizSetLayout();
            QuizSetColumnsRows();
            QuizRandomizeDropLabels();
            QuizAddUIElementsToGrid();
            return quizExtendedDetailsGrid;
        }

        private void QuizAddUIElementsToGrid()
        {
            quizExtendedDetailsGrid.Children.Add(quizDragDropALabel);
            quizExtendedDetailsGrid.Children.Add(quizDragDropBLabel);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA1Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA2Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA3Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA4Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerA5Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB1Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB2Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB3Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB4Label);
            quizExtendedDetailsGrid.Children.Add(quizAnswerB5Label);
        }

        private void QuizRandomizeDropLabels()
        {
            List<Label> randomLabels = new List<Label>();
            randomLabels.Add(quizAnswerB1Label);
            randomLabels.Add(quizAnswerB2Label);
            randomLabels.Add(quizAnswerB3Label);
            randomLabels.Add(quizAnswerB4Label);
            randomLabels.Add(quizAnswerB5Label);
            ShuffleList(randomLabels);

            Grid.SetColumn(randomLabels[0], 2);
            Grid.SetColumn(randomLabels[1], 2);
            Grid.SetColumn(randomLabels[2], 2);
            Grid.SetColumn(randomLabels[3], 2);
            Grid.SetColumn(randomLabels[4], 2);
            Grid.SetRow(randomLabels[0], 1);
            Grid.SetRow(randomLabels[1], 2);
            Grid.SetRow(randomLabels[2], 3);
            Grid.SetRow(randomLabels[3], 4);
            Grid.SetRow(randomLabels[4], 5);
        }

        private void QuizSetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(350, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(60, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(350, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r6 = new RowDefinition();
            r6.Height = new GridLength(40, GridUnitType.Pixel);

            quizExtendedDetailsGrid.ColumnDefinitions.Add(c1);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c2);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c3);
            quizExtendedDetailsGrid.RowDefinitions.Add(r1);
            quizExtendedDetailsGrid.RowDefinitions.Add(r2);
            quizExtendedDetailsGrid.RowDefinitions.Add(r3);
            quizExtendedDetailsGrid.RowDefinitions.Add(r4);
            quizExtendedDetailsGrid.RowDefinitions.Add(r5);
            quizExtendedDetailsGrid.RowDefinitions.Add(r6);

            Grid.SetColumn(quizDragDropALabel, 0);
            Grid.SetRow(quizDragDropALabel, 0);
            Grid.SetColumn(quizDragDropBLabel, 2);
            Grid.SetRow(quizDragDropBLabel, 0);

            Grid.SetColumn(quizAnswerA1Label, 0);
            Grid.SetRow(quizAnswerA1Label, 1);
            Grid.SetColumn(quizAnswerA2Label, 0);
            Grid.SetRow(quizAnswerA2Label, 2);
            Grid.SetColumn(quizAnswerA3Label, 0);
            Grid.SetRow(quizAnswerA3Label, 3);
            Grid.SetColumn(quizAnswerA4Label, 0);
            Grid.SetRow(quizAnswerA4Label, 4);
            Grid.SetColumn(quizAnswerA5Label, 0);
            Grid.SetRow(quizAnswerA5Label, 5);
        }

        private void QuizSetLayout()
        {
            quizDragDropALabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizDragDropBLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizDragDropALabel.Margin = Constants.ThicknessFive;
            quizDragDropBLabel.Margin = Constants.ThicknessFive;
            quizDragDropALabel.Background = Constants.HighlightBrush1;
            quizDragDropBLabel.Background = Constants.HighlightBrush1;
            quizDragDropALabel.FontWeight = FontWeights.Bold;
            quizDragDropBLabel.FontWeight = FontWeights.Bold;
            quizAnswerA1Label.Background = Constants.DragDropAnswerBrush1;
            quizAnswerA2Label.Background = Constants.DragDropAnswerBrush2;
            quizAnswerA3Label.Background = Constants.DragDropAnswerBrush3;
            quizAnswerA4Label.Background = Constants.DragDropAnswerBrush4;
            quizAnswerA5Label.Background = Constants.DragDropAnswerBrush5;
            quizAnswerB1Label.Background = Constants.WhiteBrush;
            quizAnswerB2Label.Background = Constants.WhiteBrush;
            quizAnswerB3Label.Background = Constants.WhiteBrush;
            quizAnswerB4Label.Background = Constants.WhiteBrush;
            quizAnswerB5Label.Background = Constants.WhiteBrush;
            quizAnswerA1Label.Foreground = Constants.WhiteBrush;
            quizAnswerA2Label.Foreground = Constants.WhiteBrush;
            quizAnswerA3Label.Foreground = Constants.WhiteBrush;
            quizAnswerA4Label.Foreground = Constants.WhiteBrush;
            quizAnswerA5Label.Foreground = Constants.WhiteBrush;
            quizAnswerA1Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA2Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA3Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA4Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA5Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB1Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB2Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB3Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB4Label.BorderBrush = Constants.BlackBrush;
            quizAnswerB5Label.BorderBrush = Constants.BlackBrush;
            quizAnswerA1Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA2Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA3Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA4Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA5Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB1Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB2Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB3Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB4Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerB5Label.BorderThickness = Constants.ThicknessOne;
            quizAnswerA1Label.Margin = Constants.ThicknessFive;
            quizAnswerA2Label.Margin = Constants.ThicknessFive;
            quizAnswerA3Label.Margin = Constants.ThicknessFive;
            quizAnswerA4Label.Margin = Constants.ThicknessFive;
            quizAnswerA5Label.Margin = Constants.ThicknessFive;
            quizAnswerB1Label.Margin = Constants.ThicknessFive;
            quizAnswerB2Label.Margin = Constants.ThicknessFive;
            quizAnswerB3Label.Margin = Constants.ThicknessFive;
            quizAnswerB4Label.Margin = Constants.ThicknessFive;
            quizAnswerB5Label.Margin = Constants.ThicknessFive;
        }

        private void QuizSetHandlers()
        {
            quizAnswerA1Label.MouseLeftButtonDown += MouseLeftButtonDownHandler;
            quizAnswerA2Label.MouseLeftButtonDown += MouseLeftButtonDownHandler;
            quizAnswerA3Label.MouseLeftButtonDown += MouseLeftButtonDownHandler;
            quizAnswerA4Label.MouseLeftButtonDown += MouseLeftButtonDownHandler;
            quizAnswerA5Label.MouseLeftButtonDown += MouseLeftButtonDownHandler;
            quizAnswerB1Label.Drop += DropHandler;
            quizAnswerB2Label.Drop += DropHandler;
            quizAnswerB3Label.Drop += DropHandler;
            quizAnswerB4Label.Drop += DropHandler;
            quizAnswerB5Label.Drop += DropHandler;
        }

        private void QuizSetAllowDrop()
        {
            quizAnswerB1Label.AllowDrop = true;
            quizAnswerB2Label.AllowDrop = true;
            quizAnswerB3Label.AllowDrop = true;
            quizAnswerB4Label.AllowDrop = true;
            quizAnswerB5Label.AllowDrop = true;
        }

        private void QuizSetContent()
        {
            quizDragDropALabel.Content = Constants.AnswersOne;
            quizDragDropBLabel.Content = Constants.AnswersTwo;
            quizAnswerA1Label.Content = dragAnswers[0];
            quizAnswerA2Label.Content = dragAnswers[1];
            quizAnswerA3Label.Content = dragAnswers[2];
            quizAnswerA4Label.Content = dragAnswers[3];
            quizAnswerA5Label.Content = dragAnswers[4];
            quizAnswerB1Label.Content = dropAnswers[0];
            quizAnswerB2Label.Content = dropAnswers[1];
            quizAnswerB3Label.Content = dropAnswers[2];
            quizAnswerB4Label.Content = dropAnswers[3];
            quizAnswerB5Label.Content = dropAnswers[4];
            quizAnswerB1Label.Name = Constants.QuizAnswerB1LabelName;
            quizAnswerB2Label.Name = Constants.QuizAnswerB2LabelName;
            quizAnswerB3Label.Name = Constants.QuizAnswerB3LabelName;
            quizAnswerB4Label.Name = Constants.QuizAnswerB4LabelName;
            quizAnswerB5Label.Name = Constants.QuizAnswerB5LabelName;
        }

        public static void ShuffleList<E>(IList<E> list)
        {
            Random random = new Random();
            if (list.Count > 1)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    E tmp = list[i];
                    int randomIndex = random.Next(i + 1);

                    //Swap elements
                    list[i] = list[randomIndex];
                    list[randomIndex] = tmp;
                }
            }
        }   

        #region Handlers
        private void MouseLeftButtonDownHandler(object sender, MouseButtonEventArgs e)
        {
            Label dragLabel = (Label)sender;
            String dragLabelString = Convert.ToString(dragLabel.Content);
            String draglabelBackground = Convert.ToString(dragLabel.Background);
            DataObject data = new DataObject(DataFormats.Text, String.Format("{0}|{1}",dragLabelString,draglabelBackground));
            DragDrop.DoDragDrop((DependencyObject)e.Source, data, DragDropEffects.Copy);
        }


        private void DropHandler(object sender, DragEventArgs e)
        {
            String[] dataList = new String[2];
            dataList = Convert.ToString(e.Data.GetData(DataFormats.Text)).Split('|');
            String dragLabelString = dataList[0];
            String dragLabeLBackground = dataList[1];
            Label dropLabel = ((Label)sender);
            String dragDropString = String.Format("{0}|{1}", dragLabelString, dropLabel.Content);
            dropLabel.Background = (SolidColorBrush)(new BrushConverter().ConvertFrom(dragLabeLBackground));
            dropLabel.Foreground = Constants.WhiteBrush;
            switch (dropLabel.Name)
            {
                case Constants.QuizAnswerB1LabelName:
                    quizUserAnswer1 = dragDropString;
                    break;
                case Constants.QuizAnswerB2LabelName:
                    quizUserAnswer2 = dragDropString;
                    break;
                case Constants.QuizAnswerB3LabelName:
                    quizUserAnswer3 = dragDropString;
                    break;
                case Constants.QuizAnswerB4LabelName:
                    quizUserAnswer4 = dragDropString;
                    break;
                case Constants.QuizAnswerB5LabelName:
                    quizUserAnswer5 = dragDropString;
                    break;
            }
        }
        #endregion
        #endregion

        #region Result
        public override void GenerateResult()
        {
            this.questionResult.Text += String.Format("Vraag: {0}\n\n{1}{2}{3}\n", this.question, Constants.StellingText.PadRight(40), Constants.RightAnswerText.PadRight(40), Constants.YourAnswerText);
            this.questionResult.Background = Constants.LightGreenBrush;

            if (quizUserAnswer1.Equals(String.Format("{0}|{1}", dragAnswers[0], dropAnswers[0])))
            {
                this.questionScore += pointPerCorrentAnswerDD;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer2.Equals(String.Format("{0}|{1}", dragAnswers[1], dropAnswers[1])))
            {
                this.questionScore += pointPerCorrentAnswerDD;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer3.Equals(String.Format("{0}|{1}", dragAnswers[2], dropAnswers[2])))
            {
                this.questionScore += pointPerCorrentAnswerDD;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer4.Equals(String.Format("{0}|{1}", dragAnswers[3], dropAnswers[3])))
            {
                this.questionScore += pointPerCorrentAnswerDD;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer5.Equals(String.Format("{0}|{1}", dragAnswers[4], dropAnswers[4])))
            {
                this.questionScore += pointPerCorrentAnswerDD;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }

            this.questionScore *= this.difficulty;
            String[] temp1 = quizUserAnswer1.Split(Constants.BarChar);
            String[] temp2 = quizUserAnswer2.Split(Constants.BarChar);
            String[] temp3 = quizUserAnswer3.Split(Constants.BarChar);
            String[] temp4 = quizUserAnswer4.Split(Constants.BarChar);
            String[] temp5 = quizUserAnswer5.Split(Constants.BarChar);
            this.questionResult.Text += String.Format("{0}{1}{2}\n", dragAnswers[0].PadRight(40), dropAnswers[0].PadRight(40), temp1[1]);
            this.questionResult.Text += String.Format("{0}{1}{2}\n", dragAnswers[1].PadRight(40), dropAnswers[1].PadRight(40), temp2[1]);
            this.questionResult.Text += String.Format("{0}{1}{2}\n", dragAnswers[2].PadRight(40), dropAnswers[2].PadRight(40), temp3[1]);
            this.questionResult.Text += String.Format("{0}{1}{2}\n", dragAnswers[3].PadRight(40), dropAnswers[3].PadRight(40), temp4[1]);
            this.questionResult.Text += String.Format("{0}{1}{2}\n", dragAnswers[4].PadRight(40), dropAnswers[4].PadRight(40), temp5[1]);

        }
        #endregion
    }
}
