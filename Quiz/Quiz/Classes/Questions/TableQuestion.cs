﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 05/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
#endregion

namespace Quiz
{
    class TableQuestion : Question
    {
        #region Properties
        protected int multiplier;
        protected String quizUserAnswer1 = String.Empty;
        protected String quizUserAnswer2 = String.Empty;
        protected String quizUserAnswer3 = String.Empty;
        protected String quizUserAnswer4 = String.Empty;
        protected String quizUserAnswer5 = String.Empty;
        protected String quizUserAnswer6 = String.Empty;
        protected String quizUserAnswer7 = String.Empty;
        protected String quizUserAnswer8 = String.Empty;
        protected String quizUserAnswer9 = String.Empty;
        protected String quizUserAnswer10 = String.Empty;
        private Grid extendedDetailsGrid = new Grid();
        private Grid quizExtendedDetailsGrid = new Grid();
        private Label tableAnswerLabel = new Label();
        private Label quizTableALabel = new Label();
        private Label quizTableBLabel = new Label();
        private Label quizTableAnswer1Label = new Label();
        private Label quizTableAnswer2Label = new Label();
        private Label quizTableAnswer3Label = new Label();
        private Label quizTableAnswer4Label = new Label();
        private Label quizTableAnswer5Label = new Label();
        private Label quizTableAnswer6Label = new Label();
        private Label quizTableAnswer7Label = new Label();
        private Label quizTableAnswer8Label = new Label();
        private Label quizTableAnswer9Label = new Label();
        private Label quizTableAnswer10Label = new Label();
        private TextBox quizTableAnswer1TextBox = new TextBox();
        private TextBox quizTableAnswer2TextBox = new TextBox();
        private TextBox quizTableAnswer3TextBox = new TextBox();
        private TextBox quizTableAnswer4TextBox = new TextBox();
        private TextBox quizTableAnswer5TextBox = new TextBox();
        private TextBox quizTableAnswer6TextBox = new TextBox();
        private TextBox quizTableAnswer7TextBox = new TextBox();
        private TextBox quizTableAnswer8TextBox = new TextBox();
        private TextBox quizTableAnswer9TextBox = new TextBox();
        private TextBox quizTableAnswer10TextBox = new TextBox();
        private TextBox tableAnswerTextbox = new TextBox();
        private int pointPerCorrectAnswerTQ = pointPerCorrectAnswer / 10;
        #endregion

        #region Constructors
        public TableQuestion(char course, int difficulty, char questionType, String question, int multiplier)
            : base(course, difficulty, questionType, question)
        {
            this.multiplier = multiplier;
        }
        public TableQuestion(TableQuestion o) //Copy Constructor
            : base(o.course, o.difficulty, o.questionType, o.question)
        {
            this.multiplier = o.multiplier;
        }
        #endregion

        #region Object Methods
        public int GetMultiplier()
        {
            return this.multiplier;
        }

        public void SetMultiplier(int multiplier)
        {
            this.multiplier = multiplier;
        }

        public override String ToString()
        {
            return String.Format("{0} {1}", this.question, this.multiplier);
        }

        public override String ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4}", this.course, this.difficulty, this.questionType, this.question, this.multiplier);
        }

        #endregion

        #region ManageQuestions
        public override Grid GetExtendedDetailsGrid()
        {
            extendedDetailsGrid.Children.Clear();
            SetContent();
            SetLayout();
            tableAnswerTextbox.TextChanged += TableAnswerTextChanged;
            SetColumnsRows();
            AddUIElementsToGrid();
            return extendedDetailsGrid;
        }

        private void AddUIElementsToGrid()
        {
            extendedDetailsGrid.Children.Add(tableAnswerLabel);
            extendedDetailsGrid.Children.Add(tableAnswerTextbox);
        }

        private void SetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(760, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            extendedDetailsGrid.ColumnDefinitions.Add(c1);
            extendedDetailsGrid.RowDefinitions.Add(r1);
            extendedDetailsGrid.RowDefinitions.Add(r2);

            Grid.SetColumn(tableAnswerLabel, 0);
            Grid.SetRow(tableAnswerLabel, 0);
            Grid.SetColumn(tableAnswerTextbox, 0);
            Grid.SetRow(tableAnswerTextbox, 1);
        }

        private void SetLayout()
        {
            tableAnswerLabel.FontWeight = FontWeights.Bold;
            tableAnswerLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            tableAnswerLabel.Margin = Constants.ThicknessFive;
            tableAnswerLabel.Background = Constants.HighlightBrush1;
            tableAnswerTextbox.Margin = Constants.ThicknessFive;
        }

        private void SetContent()
        {
            tableAnswerLabel.Content = Constants.TableAnswerLabelContent;
            tableAnswerTextbox.Text = Convert.ToString(this.multiplier);
        }

        private void ResetQuestion(TextBox temp)
        {
            new CustomMessageBox(Constants.MessageIfNumberValueNotCorrect).Show();
            temp.Text = String.Empty;
            manageInstance.SetSaveButtonDisabled();
        }
        #region Handlers
        private void TableAnswerTextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            if (!String.IsNullOrEmpty(temp.Text))
            {
                int num;
                bool parseSuccessful = int.TryParse(temp.Text, out num);
                if (parseSuccessful)
                {
                    if (num > 0)
                    {
                        if (num <= 100)
                        {
                            this.multiplier = num;
                            manageInstance.SetSaveButtonEnabled();
                        }
                        else
                        {
                            ResetQuestion(temp);
                        }
                    }
                    else
                    {
                        ResetQuestion(temp);
                    }
                    
                }
                else 
                {
                    ResetQuestion(temp);
                }
            }
        }
        #endregion
        #endregion

        #region Quiz
        public override Grid GetQuizExtendedDetailsGrid()
        {
            quizExtendedDetailsGrid.Children.Clear();
            QuizSetContent();
            QuizSetHandlers();
            QuizSetLayout();
            QuizSetColumnsRows();
            QuizAddUIElementsToGrid();
            QuizSetTabStops();
            return quizExtendedDetailsGrid;
        }

        private void QuizSetTabStops()
        {
            foreach (UIElement GridUIElement in quizExtendedDetailsGrid.Children)
            {
                int rowIndex = Grid.GetRow(GridUIElement);
                int columnIndex = Grid.GetColumn(GridUIElement);
                TextBox temp = GridUIElement as TextBox;                                       //Will not throw exception when cast does not succeed.
                if (temp != null)
                {
                    if (columnIndex == 1)
                    {
                        temp.TabIndex = rowIndex;
                    }
                    else
                    {
                        temp.TabIndex = rowIndex + 5;
                    }
                }
            }
        }

        private void QuizAddUIElementsToGrid()
        {
            quizExtendedDetailsGrid.Children.Add(quizTableALabel);
            quizExtendedDetailsGrid.Children.Add(quizTableBLabel);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer1Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer2Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer3Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer4Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer5Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer6Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer7Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer8Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer9Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer10Label);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer1TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer2TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer3TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer4TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer5TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer6TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer7TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer8TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer9TextBox);
            quizExtendedDetailsGrid.Children.Add(quizTableAnswer10TextBox);
        }

        private void QuizSetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(300, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(80, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(300, GridUnitType.Pixel);
            ColumnDefinition c4 = new ColumnDefinition();
            c4.Width = new GridLength(80, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r3 = new RowDefinition();
            r3.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r4 = new RowDefinition();
            r4.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r5 = new RowDefinition();
            r5.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r6 = new RowDefinition();
            r6.Height = new GridLength(40, GridUnitType.Pixel);

            quizExtendedDetailsGrid.ColumnDefinitions.Add(c1);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c2);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c3);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c4);
            quizExtendedDetailsGrid.RowDefinitions.Add(r1);
            quizExtendedDetailsGrid.RowDefinitions.Add(r2);
            quizExtendedDetailsGrid.RowDefinitions.Add(r3);
            quizExtendedDetailsGrid.RowDefinitions.Add(r4);
            quizExtendedDetailsGrid.RowDefinitions.Add(r5);
            quizExtendedDetailsGrid.RowDefinitions.Add(r6);

            Grid.SetColumn(quizTableALabel, 0);
            Grid.SetRow(quizTableALabel, 0);
            Grid.SetColumnSpan(quizTableALabel, 2);
            Grid.SetColumn(quizTableBLabel, 2);
            Grid.SetRow(quizTableBLabel, 0);
            Grid.SetColumnSpan(quizTableBLabel, 2);

            List<int> labelColumns = GetRandomLabelColumns();
            List<int> textBoxColumns = GetRandomTextBoxColumns();
            List<int> rows = GetRandomRows();

            Grid.SetColumn(quizTableAnswer1Label, labelColumns[0]);
            Grid.SetRow(quizTableAnswer1Label, rows[0]);
            Grid.SetColumn(quizTableAnswer2Label, labelColumns[1]);
            Grid.SetRow(quizTableAnswer2Label, rows[1]);
            Grid.SetColumn(quizTableAnswer3Label, labelColumns[0]);
            Grid.SetRow(quizTableAnswer3Label, rows[2]);
            Grid.SetColumn(quizTableAnswer4Label, labelColumns[1]);
            Grid.SetRow(quizTableAnswer4Label, rows[3]);
            Grid.SetColumn(quizTableAnswer5Label, labelColumns[0]);
            Grid.SetRow(quizTableAnswer5Label, rows[4]);

            Grid.SetColumn(quizTableAnswer6Label, labelColumns[1]);
            Grid.SetRow(quizTableAnswer6Label, rows[0]);
            Grid.SetColumn(quizTableAnswer7Label, labelColumns[0]);
            Grid.SetRow(quizTableAnswer7Label, rows[1]);
            Grid.SetColumn(quizTableAnswer8Label, labelColumns[1]);
            Grid.SetRow(quizTableAnswer8Label, rows[2]);
            Grid.SetColumn(quizTableAnswer9Label, labelColumns[0]);
            Grid.SetRow(quizTableAnswer9Label, rows[3]);
            Grid.SetColumn(quizTableAnswer10Label, labelColumns[1]);
            Grid.SetRow(quizTableAnswer10Label, rows[4]);

            Grid.SetColumn(quizTableAnswer1TextBox, textBoxColumns[0]);
            Grid.SetRow(quizTableAnswer1TextBox, rows[0]);
            Grid.SetColumn(quizTableAnswer2TextBox, textBoxColumns[1]);
            Grid.SetRow(quizTableAnswer2TextBox, rows[1]);
            Grid.SetColumn(quizTableAnswer3TextBox, textBoxColumns[0]);
            Grid.SetRow(quizTableAnswer3TextBox, rows[2]);
            Grid.SetColumn(quizTableAnswer4TextBox, textBoxColumns[1]);
            Grid.SetRow(quizTableAnswer4TextBox, rows[3]);
            Grid.SetColumn(quizTableAnswer5TextBox, textBoxColumns[0]);
            Grid.SetRow(quizTableAnswer5TextBox, rows[4]);

            Grid.SetColumn(quizTableAnswer6TextBox, textBoxColumns[1]);
            Grid.SetRow(quizTableAnswer6TextBox, rows[0]);
            Grid.SetColumn(quizTableAnswer7TextBox, textBoxColumns[0]);
            Grid.SetRow(quizTableAnswer7TextBox, rows[1]);
            Grid.SetColumn(quizTableAnswer8TextBox, textBoxColumns[1]);
            Grid.SetRow(quizTableAnswer8TextBox, rows[2]);
            Grid.SetColumn(quizTableAnswer9TextBox, textBoxColumns[0]);
            Grid.SetRow(quizTableAnswer9TextBox, rows[3]);
            Grid.SetColumn(quizTableAnswer10TextBox, textBoxColumns[1]);
            Grid.SetRow(quizTableAnswer10TextBox, rows[4]);
        }

        public static void ShuffleList<E>(IList<E> list)
        {
            Random random = new Random();
            if (list.Count > 1)
            {
                for (int i = list.Count - 1; i >= 0; i--)
                {
                    E tmp = list[i];
                    int randomIndex = random.Next(i + 1);

                    //Swap elements
                    list[i] = list[randomIndex];
                    list[randomIndex] = tmp;
                }
            }
        }

        private List<int> GetRandomTextBoxColumns()
        {
            List<int> columns = new List<int>();
            columns.Add(1);
            columns.Add(3);
            ShuffleList(columns);
            return columns;
        }

        private List<int> GetRandomLabelColumns()
        {
            List<int> columns = new List<int>();
            columns.Add(0);
            columns.Add(2);
            ShuffleList(columns);
            return columns;
        }

        private List<int> GetRandomRows()
        {
            List<int> rows = new List<int>();
            for (int i = 1; i <= 5; i++)
            {
                rows.Add(i);
            }
                ShuffleList(rows);
            return rows;
        }

        private void QuizSetLayout()
        {
            quizTableALabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableBLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableALabel.Margin = Constants.ThicknessFive;
            quizTableBLabel.Margin = Constants.ThicknessFive;
            quizTableALabel.Background = Constants.HighlightBrush1;
            quizTableBLabel.Background = Constants.HighlightBrush1;
            quizTableALabel.FontWeight = FontWeights.Bold;
            quizTableBLabel.FontWeight = FontWeights.Bold;
            quizTableAnswer1Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer2Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer3Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer4Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer5Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer6Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer7Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer8Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer9Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer10Label.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizTableAnswer1Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer2Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer3Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer4Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer5Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer6Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer7Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer8Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer9Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer10Label.BorderBrush = Constants.BlackBrush;
            quizTableAnswer1Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer2Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer3Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer4Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer5Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer6Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer7Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer8Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer9Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer10Label.BorderThickness = Constants.ThicknessOne;
            quizTableAnswer1Label.Margin = Constants.ThicknessFive;
            quizTableAnswer2Label.Margin = Constants.ThicknessFive;
            quizTableAnswer3Label.Margin = Constants.ThicknessFive;
            quizTableAnswer4Label.Margin = Constants.ThicknessFive;
            quizTableAnswer5Label.Margin = Constants.ThicknessFive;
            quizTableAnswer6Label.Margin = Constants.ThicknessFive;
            quizTableAnswer7Label.Margin = Constants.ThicknessFive;
            quizTableAnswer8Label.Margin = Constants.ThicknessFive;
            quizTableAnswer9Label.Margin = Constants.ThicknessFive;
            quizTableAnswer10Label.Margin = Constants.ThicknessFive;
            quizTableAnswer1TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer2TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer3TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer4TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer5TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer6TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer7TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer8TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer9TextBox.Margin = Constants.ThicknessFive;
            quizTableAnswer10TextBox.Margin = Constants.ThicknessFive;
        }

        private void QuizSetHandlers()
        {
            quizTableAnswer1TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer2TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer3TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer4TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer5TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer6TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer7TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer8TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer9TextBox.TextChanged += quizTableTextChangedHandler;
            quizTableAnswer10TextBox.TextChanged += quizTableTextChangedHandler;
        }

        private void QuizSetContent()
        {
            quizQuestionTextBox.Text += String.Format(" {0}", this.multiplier);
            quizTableALabel.Content = Constants.AnswersOne;
            quizTableBLabel.Content = Constants.AnswersTwo;
            quizTableAnswer1Label.Content = String.Format("1x{0}", this.multiplier);
            quizTableAnswer2Label.Content = String.Format("2x{0}", this.multiplier);
            quizTableAnswer3Label.Content = String.Format("3x{0}", this.multiplier);
            quizTableAnswer4Label.Content = String.Format("4x{0}", this.multiplier);
            quizTableAnswer5Label.Content = String.Format("5x{0}", this.multiplier);
            quizTableAnswer6Label.Content = String.Format("6x{0}", this.multiplier);
            quizTableAnswer7Label.Content = String.Format("7x{0}", this.multiplier);
            quizTableAnswer8Label.Content = String.Format("8x{0}", this.multiplier);
            quizTableAnswer9Label.Content = String.Format("9x{0}", this.multiplier);
            quizTableAnswer10Label.Content = String.Format("10x{0}", this.multiplier);
            quizTableAnswer1TextBox.Name = Constants.QuizTableAnswer1TextBox;
            quizTableAnswer2TextBox.Name = Constants.QuizTableAnswer2TextBox;
            quizTableAnswer3TextBox.Name = Constants.QuizTableAnswer3TextBox;
            quizTableAnswer4TextBox.Name = Constants.QuizTableAnswer4TextBox;
            quizTableAnswer5TextBox.Name = Constants.QuizTableAnswer5TextBox;
            quizTableAnswer6TextBox.Name = Constants.QuizTableAnswer6TextBox;
            quizTableAnswer7TextBox.Name = Constants.QuizTableAnswer7TextBox;
            quizTableAnswer8TextBox.Name = Constants.QuizTableAnswer8TextBox;
            quizTableAnswer9TextBox.Name = Constants.QuizTableAnswer9TextBox;
            quizTableAnswer10TextBox.Name = Constants.QuizTableAnswer10TextBox;
        }

        #region Handlers
        private void quizTableTextChangedHandler(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            if (temp.Text.Length > 25)
            {
                new CustomMessageBox(Constants.LengthWarningString).Show();
            }
            else
            {
                switch (temp.Name)
                {
                    case Constants.QuizTableAnswer1TextBox:
                        quizUserAnswer1 = quizTableAnswer1TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer2TextBox:
                        quizUserAnswer2 = quizTableAnswer2TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer3TextBox:
                        quizUserAnswer3 = quizTableAnswer3TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer4TextBox:
                        quizUserAnswer4 = quizTableAnswer4TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer5TextBox:
                        quizUserAnswer5 = quizTableAnswer5TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer6TextBox:
                        quizUserAnswer6 = quizTableAnswer6TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer7TextBox:
                        quizUserAnswer7 = quizTableAnswer7TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer8TextBox:
                        quizUserAnswer8 = quizTableAnswer8TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer9TextBox:
                        quizUserAnswer9 = quizTableAnswer9TextBox.Text;
                        break;
                    case Constants.QuizTableAnswer10TextBox:
                        quizUserAnswer10 = quizTableAnswer10TextBox.Text;
                        break;
                } 
            }
            
        }
        #endregion
        #endregion

        #region Result
        public override void GenerateResult()
        {
            this.questionResult.Text += String.Format("Vraag: {0} {1}\n\n{2}{3}{4}{5}{6}{7}\n", this.question, this.multiplier.ToString(), Constants.OpgaveText.PadRight(20), Constants.RightAnswerText.PadRight(20), 
                Constants.YourAnswerText.PadRight(20), Constants.OpgaveText.PadRight(20), Constants.RightAnswerText.PadRight(20), Constants.YourAnswerText.PadRight(20));
            this.questionResult.Background = Constants.LightGreenBrush;

            if (quizUserAnswer1.Equals(Convert.ToString(1 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer2.Equals(Convert.ToString(2 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer3.Equals(Convert.ToString(3 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer4.Equals(Convert.ToString(4 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer5.Equals(Convert.ToString(5 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer6.Equals(Convert.ToString(6 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer7.Equals(Convert.ToString(7 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer8.Equals(Convert.ToString(8 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer9.Equals(Convert.ToString(9 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            if (quizUserAnswer10.Equals(Convert.ToString(10 * multiplier)))
            {
                this.questionScore += pointPerCorrectAnswerTQ;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            
            this.questionScore *= this.difficulty;
            this.questionResult.Text += String.Format("{0}{1}{2}{3}{4}{5}\n",
                String.Format("1x{0}", this.multiplier).PadRight(20), Convert.ToString(1 * this.multiplier).PadRight(20), quizUserAnswer1.PadRight(20),
                String.Format("6x{0}", this.multiplier).PadRight(20), Convert.ToString(6 * this.multiplier).PadRight(20), quizUserAnswer6.PadRight(20));
            this.questionResult.Text += String.Format("{0}{1}{2}{3}{4}{5}\n",
                String.Format("2x{0}", this.multiplier).PadRight(20), Convert.ToString(2 * this.multiplier).PadRight(20), quizUserAnswer2.PadRight(20),
                String.Format("7x{0}", this.multiplier).PadRight(20), Convert.ToString(7 * this.multiplier).PadRight(20), quizUserAnswer7.PadRight(20));
            this.questionResult.Text += String.Format("{0}{1}{2}{3}{4}{5}\n",
                String.Format("3x{0}", this.multiplier).PadRight(20), Convert.ToString(3 * this.multiplier).PadRight(20), quizUserAnswer3.PadRight(20),
                String.Format("8x{0}", this.multiplier).PadRight(20), Convert.ToString(8 * this.multiplier).PadRight(20), quizUserAnswer8.PadRight(20));
            this.questionResult.Text += String.Format("{0}{1}{2}{3}{4}{5}\n",
                String.Format("4x{0}", this.multiplier).PadRight(20), Convert.ToString(4 * this.multiplier).PadRight(20), quizUserAnswer4.PadRight(20),
                String.Format("9x{0}", this.multiplier).PadRight(20), Convert.ToString(9 * this.multiplier).PadRight(20), quizUserAnswer9.PadRight(20));
            this.questionResult.Text += String.Format("{0}{1}{2}{3}{4}{5}\n",
                String.Format("5x{0}", this.multiplier).PadRight(20), Convert.ToString(5 * this.multiplier).PadRight(20), quizUserAnswer5.PadRight(20),
                String.Format("10x{0}", this.multiplier).PadRight(20), Convert.ToString(10 * this.multiplier).PadRight(20), quizUserAnswer10.PadRight(20));
        }
        #endregion
    }
}