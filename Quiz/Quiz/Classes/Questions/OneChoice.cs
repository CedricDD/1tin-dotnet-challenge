﻿    #region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 30/03/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Input;
#endregion

namespace Quiz
{
    class OneChoice : Question
    {
        #region Properties
        protected String correctAnswer;
        protected String quizUserAnswer = String.Empty;
        private Grid extendedDetailsGrid = new Grid();
        private Grid quizExtendedDetailsGrid = new Grid();
        private Label correctAnswerLabel = new Label();
        private Label quizAnswerLabel = new Label();
        private TextBox correctAnswerTextBox = new TextBox();
        private TextBox quizAnswerTextBox = new TextBox();
        #endregion

        #region Constructors
        public OneChoice(char course, int difficulty, char questionType, String question, String correctAnswer)
            : base(course, difficulty, questionType, question)
        {
            this.correctAnswer = correctAnswer;

        }

        public OneChoice(OneChoice o) //Copy Constructor
            : base(o.GetCourse, o.GetDifficulty, o.GetQuestionType, o.GetQuestion)
        {
            this.correctAnswer = o.GetCorrectAnswer();
        }
        #endregion

        #region Object Methods
        public String GetCorrectAnswer()
        {
            return correctAnswer;
        }

        public void SetCorrectAnswer(String correctAnswer)
        {
            this.correctAnswer = correctAnswer;
        }

        public override String ToString()
        {
            return this.question;
        }

        public override String ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4}", this.course, this.difficulty, this.questionType, this.question, this.correctAnswer);
        }

        #endregion

        #region ManageQuestions

        public override Grid GetExtendedDetailsGrid()
        {  
            extendedDetailsGrid.Children.Clear();
            SetContent();
            correctAnswerTextBox.TextChanged += correctAnswerTextBox_TextChanged;
            SetLayout();
            SetColumnsRows();
            AddUIElementsToGrid();
            return extendedDetailsGrid;
        }

        private void AddUIElementsToGrid()
        {
            extendedDetailsGrid.Children.Add(correctAnswerLabel);
            extendedDetailsGrid.Children.Add(correctAnswerTextBox);
        }

        private void SetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(760, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            extendedDetailsGrid.ColumnDefinitions.Add(c1);
            extendedDetailsGrid.RowDefinitions.Add(r1);
            extendedDetailsGrid.RowDefinitions.Add(r2);
            Grid.SetColumn(correctAnswerLabel, 0);
            Grid.SetRow(correctAnswerLabel, 0);
            Grid.SetColumn(correctAnswerTextBox, 0);
            Grid.SetRow(correctAnswerTextBox, 1);
        }

        private void SetLayout()
        {
            correctAnswerLabel.FontWeight = FontWeights.Bold;
            correctAnswerLabel.Background = Constants.HighlightBrush1;
            correctAnswerLabel.Margin = Constants.ThicknessFive;
            correctAnswerLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            correctAnswerTextBox.Margin = Constants.ThicknessFive;
            correctAnswerTextBox.HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        private void SetContent()
        {
            correctAnswerLabel.Content = Constants.AnswerLabelContent;
            correctAnswerTextBox.Text = this.correctAnswer;
        }

        #region Handlers

        private void correctAnswerTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.correctAnswer = correctAnswerTextBox.Text;
            manageInstance.SetSaveButtonEnabled();
        }
        #endregion
        #endregion

        #region Quiz
        public override Grid GetQuizExtendedDetailsGrid()
        {
            quizExtendedDetailsGrid.Children.Clear();
            quizAnswerLabel.Content = Constants.AnswerLabelContent;
            quizAnswerTextBox.KeyDown += quizAnswerTextBox_KeyDown;
            quizAnswerTextBox.TextChanged += TextChangedHandler;
            QuizSetLayout();
            QuizSetColumnsRows();
            QuizAddUIElementsToGrid();
            return quizExtendedDetailsGrid;
        }

        private void QuizAddUIElementsToGrid()
        {
            quizExtendedDetailsGrid.Children.Add(quizAnswerLabel);
            quizExtendedDetailsGrid.Children.Add(quizAnswerTextBox);
        }

        private void QuizSetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(760,GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(40, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            quizExtendedDetailsGrid.ColumnDefinitions.Add(c1);
            quizExtendedDetailsGrid.RowDefinitions.Add(r1);
            quizExtendedDetailsGrid.RowDefinitions.Add(r2);
            Grid.SetColumn(quizAnswerLabel, 0);
            Grid.SetRow(quizAnswerLabel, 0);
            Grid.SetColumn(quizAnswerTextBox, 0);
            Grid.SetRow(quizAnswerTextBox, 1);
        }

        private void QuizSetLayout()
        {
            quizAnswerLabel.FontWeight = FontWeights.Bold;
            quizAnswerLabel.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizAnswerLabel.Margin = Constants.ThicknessFive;
            quizAnswerLabel.Background = Constants.HighlightBrush1;
            quizAnswerTextBox.Margin = Constants.ThicknessFive;
        }

        #region Handlers
        void quizAnswerTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter){
                if (quizInstance.nextButton.Visibility == Visibility.Visible)
                {
                    quizInstance.nextButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, quizInstance.nextButton));
                }
                else
                {
                    quizInstance.resultButton.RaiseEvent(new RoutedEventArgs(Button.ClickEvent, quizInstance.resultButton));
                }
            }
        }

        private void TextChangedHandler(object sender, TextChangedEventArgs e)
        {
            TextBox temp = (TextBox)sender;
            if (temp.Text.Length > 25)
            {
                new CustomMessageBox(Constants.LengthWarningString).Show();
            }
            else
            {
                this.quizUserAnswer = quizAnswerTextBox.Text;
            }
            
        }
        #endregion
        #endregion

        #region Result
        public override void GenerateResult()
        {
            if (quizUserAnswer.ToLower().Equals(correctAnswer.ToLower()))
            {
                this.questionScore = pointPerCorrectAnswer;
                this.questionResult.Background = Constants.LightGreenBrush;
            }
            else
            {
                this.questionResult.Background = Constants.LightRedBrush;
            }
            this.questionResult.Text = String.Format("Vraag: {0}\n\n{1}{2}\n{3}{4}", this.question, Constants.RightAnswerText.PadRight(50), Constants.YourAnswerText, this.correctAnswer.PadRight(50), this.quizUserAnswer);
            this.questionScore *= this.difficulty;
        }
        #endregion
    }
}
