﻿#region Class Comment
/*
 * Created By Hans Habraken - 1TINL
 * 03/04/2015
 */ 
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
#endregion

namespace Quiz
{
    public class Student : User
    {
        #region Properties
        private int quizScore;
        private int gameScore;
        private Grid reportGrid = new Grid();
        Label firstNameLabelDesc = new Label();
        Label firstNameLabel = new Label();
        Label lastNameLabelDesc = new Label();
        Label lastNameLabel = new Label();
        Label quizScoreLabelDesc = new Label();
        Label quizScoreLabel = new Label();
        Label gameScoreLabelDesc = new Label();
        Label gameScoreLabel = new Label();
        #endregion

        #region Constructor
        public Student(String loginName, String firstName, String lastName, String group, String password, int quizScore, int gameScore)
            : base(loginName, firstName, lastName, group, password)
        {
            this.quizScore = quizScore;
            this.gameScore = gameScore;
            
        }
        #endregion

        #region Object Methods
        public int GetQuizScore
        {
            get
            {
                return this.quizScore;
            }
        }

        public void SetQuizScore(int quizScore)
        {
            this.quizScore = quizScore;
        }

        public int GetGameScore
        {
            get
            {
                return this.gameScore;
            }
        }

        public void SetGameScore(int gameScore)
        {
            this.gameScore = gameScore;
        }

        public override string GetUserType()
        {
            return Constants.UserTypeStudentText;
        }

        public override string ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4};{5};{6};{7}", Constants.UserTypeStudent, this.loginName, this.firstName, this.lastName, this.group, this.password,this.quizScore,this.gameScore);
        }
        #endregion

        #region Report

        public Grid GetReportGrid()
        {
            reportGrid.Children.Clear();
            SetContent();
            SetLayout();
            SetColumnsRows();
            AddUIElementsToGrid();
            return this.reportGrid;
        }

        private void AddUIElementsToGrid()
        {
            reportGrid.Children.Add(firstNameLabelDesc);
            reportGrid.Children.Add(lastNameLabelDesc);
            reportGrid.Children.Add(quizScoreLabelDesc);
            reportGrid.Children.Add(gameScoreLabelDesc);
            reportGrid.Children.Add(firstNameLabel);
            reportGrid.Children.Add(lastNameLabel);
            reportGrid.Children.Add(quizScoreLabel);
            reportGrid.Children.Add(gameScoreLabel);
        }

        private void SetColumnsRows()
        {
            ColumnDefinition c1 = new ColumnDefinition();
            c1.Width = new GridLength(210, GridUnitType.Pixel);
            ColumnDefinition c2 = new ColumnDefinition();
            c2.Width = new GridLength(210, GridUnitType.Pixel);
            ColumnDefinition c3 = new ColumnDefinition();
            c3.Width = new GridLength(210, GridUnitType.Pixel);
            ColumnDefinition c4 = new ColumnDefinition();
            c4.Width = new GridLength(210, GridUnitType.Pixel);
            RowDefinition r1 = new RowDefinition();
            r1.Height = new GridLength(50, GridUnitType.Pixel);
            RowDefinition r2 = new RowDefinition();
            r2.Height = new GridLength(40, GridUnitType.Pixel);
            reportGrid.ColumnDefinitions.Add(c1);
            reportGrid.ColumnDefinitions.Add(c2);
            reportGrid.ColumnDefinitions.Add(c3);
            reportGrid.ColumnDefinitions.Add(c4);
            reportGrid.RowDefinitions.Add(r1);
            reportGrid.RowDefinitions.Add(r2);

            Grid.SetColumn(firstNameLabelDesc, 0);
            Grid.SetRow(firstNameLabelDesc, 0);
            Grid.SetColumn(lastNameLabelDesc, 1);
            Grid.SetRow(lastNameLabelDesc, 0);
            Grid.SetColumn(quizScoreLabelDesc, 2);
            Grid.SetRow(quizScoreLabelDesc, 0);
            Grid.SetColumn(gameScoreLabelDesc, 3);
            Grid.SetRow(gameScoreLabelDesc, 0);
            Grid.SetColumn(firstNameLabel, 0);
            Grid.SetRow(firstNameLabel, 1);
            Grid.SetColumn(lastNameLabel, 1);
            Grid.SetRow(lastNameLabel, 1);
            Grid.SetColumn(quizScoreLabel, 2);
            Grid.SetRow(quizScoreLabel, 1);
            Grid.SetColumn(gameScoreLabel, 3);
            Grid.SetRow(gameScoreLabel, 1);
        }

        private void SetLayout()
        {
            firstNameLabelDesc.FontWeight = FontWeights.Bold;
            firstNameLabelDesc.HorizontalAlignment = HorizontalAlignment.Stretch;
            firstNameLabelDesc.Background = Constants.HighlightBrush1;
            firstNameLabelDesc.Margin = Constants.ThicknessFive;
            lastNameLabelDesc.FontWeight = FontWeights.Bold;
            lastNameLabelDesc.HorizontalAlignment = HorizontalAlignment.Stretch;
            lastNameLabelDesc.Background = Constants.HighlightBrush1;
            lastNameLabelDesc.Margin = Constants.ThicknessFive;
            quizScoreLabelDesc.FontWeight = FontWeights.Bold;
            quizScoreLabelDesc.HorizontalAlignment = HorizontalAlignment.Stretch;
            quizScoreLabelDesc.Background = Constants.HighlightBrush1;
            quizScoreLabelDesc.Margin = Constants.ThicknessFive;
            gameScoreLabelDesc.FontWeight = FontWeights.Bold;
            gameScoreLabelDesc.HorizontalAlignment = HorizontalAlignment.Stretch;
            gameScoreLabelDesc.Background = Constants.HighlightBrush1;
            gameScoreLabelDesc.Margin = Constants.ThicknessFive;
            reportGrid.HorizontalAlignment = HorizontalAlignment.Stretch;
        }

        private void SetContent()
        {
            firstNameLabelDesc.Content = Constants.ReportFirstNameLabelDesc;
            lastNameLabelDesc.Content = Constants.ReportLastNameLabelDesc;
            quizScoreLabelDesc.Content = Constants.ReportQuizScoreLabelDesc;
            gameScoreLabelDesc.Content = Constants.ReportGameScoreLabelDesc;
            firstNameLabel.Content = this.firstName;
            lastNameLabel.Content = this.lastName;
            quizScoreLabel.Content = this.quizScore;
            gameScoreLabel.Content = this.gameScore;
        }
        #endregion
    }
}
