﻿#region Class Comment
/*
 * Created By Hans Habraken - 1TINL
 * 03/04/2015
 * 
 * Edited by Frankie Claessens - 1TINL
 * 20/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Quiz
{
    public abstract class User
    {
        #region Properties
        protected String loginName;
        protected String firstName;
        protected String lastName;
        protected String group;
        protected String password;
        #endregion

        #region Constructor
        public User(String loginName, String firstName, String lastName, String group, String password)
        {
            this.loginName = loginName;
            this.firstName = firstName;
            this.lastName = lastName;
            this.group = group;
            this.password = password;
        }
        #endregion

        #region Object Methods
        public String GetLoginName
        {
            get {
                return this.loginName;
            }
        }

        public void SetLoginName(String loginName)
        {
            this.loginName = loginName;
        }

        public String GetFirstName
        {
            get
            {
                return this.firstName;
            }
        }

        public void SetFirstName(String firstName)
        {
            this.firstName = firstName;
        }

        public String GetLastName
        {
            get 
            {
                return this.lastName;
            }
        }

        public void SetLastName(String lastName)
        {
            this.lastName = lastName;
        }

        public String GetGroup
        {
            get
            {
                return this.group;
            }
        }

        public void SetGroup(String group)
        {
            this.group = group;
        }

        public String GetPassword
        {
            get
            {
                return this.password;
            }
        }

        public void SetPassword(String password)
        {
            this.password = password;
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", this.firstName, this.lastName);
        }
        #endregion

        #region Abstract Methods
        public abstract String GetUserType();
        public abstract String ToFile();
        #endregion
    }
}
