﻿#region Class Comment
/*
 * Created By Hans Habraken - 1TINL
 * 03/04/2015
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Quiz
{
    public class Admin : User
    {
        #region Constructor
        public Admin(String loginName, String firstName, String lastName, String group, String password)
            : base(loginName, firstName, lastName, group, password)
        {

        }
        #endregion

        #region Methods
        public override string GetUserType()
        {
            return Constants.UserTypeAdminText;
        }

        public override string ToFile()
        {
            return String.Format("{0};{1};{2};{3};{4};{5}", Constants.UserTypeAdmin, this.loginName, this.firstName, this.lastName, this.group, this.password);
        }
        #endregion
    }
}
