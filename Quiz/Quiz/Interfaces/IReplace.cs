﻿#region ClassComment
/*
 * Created by Frankie Claessens - 1TINL
 * 12/04/2015
 * This interface has to be implemented in classes where a Question object is created/replaced.
 * Passing this interface type as a property and calling the method, will pass the old object to the class that needs it.
 */
#endregion

#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#endregion

namespace Quiz
{
    public interface IReplace
    {
        void OnReplaceRequested(Question oldQuestionObject, char questionType);
    }
}
