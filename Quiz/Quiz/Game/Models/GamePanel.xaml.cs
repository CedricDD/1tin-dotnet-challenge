﻿#region Usings
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
#endregion

namespace Quiz
{
    #region Comments
    //Date: 22/04/15 18:00
    //Author: Cédric De Dycker
    //Xaml Author: Frankie Claessens
    #endregion

    public partial class GamePanel : UserControl
    {
        #region Properties
        Game g;
        private double value;
        private int multi;
        private double factor = 1;
        private double xBar = 11, barHeight;
        #endregion

        #region Constructor
        public GamePanel(Game g)
        {
            InitializeComponent();
            this.g = g;
            powerBarRect.Height = Constants.ScreenHeight - 225;
            barHeight = powerBarRect.Height;
            powerBarRect.Margin = new Thickness(0, 5, 0, 0);
            value = 100;
            multi = +1;
        }
        #endregion

        #region Methods
        public void Update()
        {
            timeleftLabel.Content = CalculateRemainingTime(g.GetTimeLeft());
            xBar += 1 * factor * multi;
            value += 1 * multi;
            factor += 0.025;
            if (factor > 9) factor = 9;
            if (xBar < 10 || xBar >= barHeight ) multi *= -1;
            selectorRect.Margin = new Thickness(0, xBar, 0, 0);
            
        }
        #region Comments
        //Date: 02/05/15 20:00
        //Author: Frankie Claessens
        #endregion
        private String CalculateRemainingTime(int timeLeft)
        {
            int minutes = timeLeft / 60;         //Minutes = seconds / 60 -> 140 / 60 = 2
            int seconds = timeLeft % 60;         //Seconds = Remaining seconds
            return String.Format("{0:D2}:{1:D2}", minutes, seconds);
        }
        public double GetValue()
        {            
            return (xBar *-1 + barHeight) / 20;            
        }
        public void ResetSpeed()
        {
            factor = 1;
        }
        #endregion
    }
}
