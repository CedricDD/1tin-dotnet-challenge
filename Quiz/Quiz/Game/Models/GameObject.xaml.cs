﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz
{
    #region Comments
    //Date: 20/04/15 23:00
    //Author: Cédric De Dycker
    #endregion
    public abstract partial class GameObject : UserControl
    {
        #region Properties
        private double x, y;
        public double Angle, Speed;
        public double X { get { return x; } set { x = value; } }
        public double Y { get { return y; } set { y = value; } }
        public double dx;
        public double dy;
        public int width, height;
        public bool solid;
        private Constants.ObjectState objectState;
        protected static GameState game;

        #region Getters && Setters
        public Grid GetObjectGrid()
        {
            return ObjectGrid;
        }
        #endregion

        #endregion

        #region Constructor
        public GameObject(double x, double y, int width, int height, bool solid)
        {
            InitializeComponent();
            //Variables
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.solid = solid;
            Angle = 0;
            Speed = 0;
            Init();
            objectState = Constants.ObjectState.Alive;
            
        }
        public GameObject(double x, double y, int width, int height, bool solid, double Angle, double Speed)
        {
            InitializeComponent();
            //Variables
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
            this.solid = solid;
            this.Angle = Angle;
            this.Speed = Speed;
            Init();

        }
        #endregion

        #region Methods

        #region Core
        public abstract void Update();
        public abstract void Draw(Canvas c);
        
        private void Init()
        {
            ObjectGrid.Width = width;
            ObjectGrid.Height = height;
            ObjectGrid.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            ObjectGrid.VerticalAlignment = System.Windows.VerticalAlignment.Top;

            objectState = Constants.ObjectState.Alive;
        }
        #endregion
        public static void SetGameState(GameState game)
        {
            GameObject.game = game;
        }
        public void CheckAngle()
        {
            Angle = Math.Atan2(dy, dx) * 180 / Math.PI;
        }
        public Constants.ObjectState GetObjectState()
        {
            return objectState;
        }
        public void SetObjectState(Constants.ObjectState state)
        {
            this.objectState = state;
        }
         #endregion
    }
}
