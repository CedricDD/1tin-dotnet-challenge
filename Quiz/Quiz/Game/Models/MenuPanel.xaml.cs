﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz
{

    #region Comments
    //Date: 23/04/15 21:00
    //Author: Cédric De Dycker
    #endregion
    public partial class MenuPanel : UserControl
    {
        #region Variables
        private MenuState menu;
        public int width = 180;
        public int height = 150;
        #endregion

        #region Constructor
        public MenuPanel(MenuState menu)
        {
            InitializeComponent();
            this.menu = menu;
        }
        #endregion

        #region Methods
        public void Start_Click(object sender, EventArgs e)
        {
            menu.StartGame();
        }
        public void Help_Click(object sender, EventArgs e)
        {
            menu.StartHelp();
        }
        public void Quit_Click(object sender, EventArgs e)
        {
            menu.EndGame();
        }
        public void Story_Click(object sender, EventArgs e)
        {
            menu.StartStory();
        }
        #endregion
    }
}
