﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz
{
    #region Comments
    //Date: 20/04/15 23:00
    //Author: Cédric De Dycker
    #endregion
    public partial class Ship : UserControl
    {
        
        #region Constructor
        public Ship(int width, int height, string path)
        {
            InitializeComponent();

            spriteButton.Width = width;
            spriteButton.Height = height;
            spriteImage.Source = ImageManager.GetBitMapImage(path,width,height);

        }
        #endregion

    }
}
