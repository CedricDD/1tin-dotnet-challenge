﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Quiz
{
    #region Comments
    //Date: 16/04/15 14:00
    //Author: Cédric De Dycker
    #endregion
    public class MenuState: State
    {
        #region Variables
        private Canvas menuScreen;
        private MenuPanel buttons;
        private Background background;
        private Player[] players;
        private Computer[] enemies;
        private CollisionManager cm;
        private Title title;
        private List<GameObject> list;
        #region Random
        private Randomizer r;
        private Randomizer angle;
        private Randomizer speed;
        private Randomizer xpos;
        private Randomizer ypos;
        #endregion
        #endregion

        #region Constructor
        public MenuState(Game g)
            :base(g)
        {
            Init();
            cm = new CollisionManager(Constants.ScreenWidth, Constants.ScreenHeight);
        }
        #endregion

        #region Methods

        public override void Update()
        {
            for (int i = 0; i < players.Length; i++)
            {
                players[i].Move();
            }
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].Move();
            }
            if (list != null) cm.CollisionCheck(list, false);
        }
        public override void Draw(Canvas c)
        {
            menuScreen.Children.Clear();
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].Draw(menuScreen);
            }
            for (int i = 0; i < players.Length; i++)
            {
                players[i].Draw(menuScreen);
            }
            menuScreen.Children.Add(title);
            menuScreen.Children.Add(buttons);
            c.Children.Add(menuScreen);
        }

        public override void Init()
        {
             #region Setup Display

            #region Window
            menuScreen = new Canvas();
            menuScreen.Height = Constants.ScreenHeight;
            menuScreen.Width = Constants.ScreenWidth;
            menuScreen.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            menuScreen.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            background = new Background(Constants.MenuBackgroundPath);
            menuScreen.Background = background.GetBrush();
            title = new Title();
            title.Margin = new Thickness(Constants.ScreenWidth / 2 -430, Constants.ScreenHeight / 2 - 320, 0, 0);
            title.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            title.VerticalAlignment = System.Windows.VerticalAlignment.Center;
             #endregion

            #region Panel
            buttons = new MenuPanel(this);
            buttons.Margin = new Thickness(Constants.ScreenWidth / 2 - buttons.width / 2, Constants.ScreenHeight - (buttons.height * 1.5), 0, 0);
            #endregion

             #endregion

            #region Setup Decoration

            #region Random
            r = new Randomizer();
            angle = new Randomizer(r.Next());
            speed = new Randomizer(r.Next() * angle.Next());
            xpos = new Randomizer(r.Next() * speed.Next());
            ypos = new Randomizer(r.Next() * xpos.Next());
            Constants.SelectedEnemy = xpos.Next(0, 1);
            Constants.SelectedShip = ypos.Next(0, 1);
            #endregion

            #region Create decor
            int amount = r.Next(10, 20);
            int pamount = amount / 2;
            int eamount = amount - pamount;
            players = new Player[pamount];
            enemies = new Computer[eamount];
            list = new List<GameObject>();

            for (int i = 0; i < players.Length; i++)
            {
                players[i] = new Player(xpos.Next(128, Constants.ScreenWidth - 128), ypos.Next(64, Constants.ScreenHeight - 64), i);
                players[i].SetAngle(angle.Next(0, 360));
                players[i].SetSpeed(speed.Next(3, 6));
                players[i].UpdateMovement();
                list.Add(players[i]);
               
            }
            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i] = new Computer(xpos.Next(128, Constants.ScreenWidth - 128), ypos.Next(64, Constants.ScreenHeight - 64), i);
                enemies[i].SetAngle(angle.Next(0, 360));
                enemies[i].SetSpeed(speed.Next(3, 6));
                enemies[i].UpdateMovement();
                list.Add(enemies[i]);
               
            }
            #endregion

            Constants.SelectedEnemy = 0;
            Constants.SelectedShip = 0;
            #endregion

        }

        #region Controlers
        public void StartGame()
        {
            game.StartShipSelection();
        }
        public void StartHelp()
        {
            game.StartHelp();
        }
        public void EndGame()
        {
            game.StopGame();
        }
        public void StartStory()
        {
            game.StartStory();
        }
        #endregion

        #endregion
    }
}
