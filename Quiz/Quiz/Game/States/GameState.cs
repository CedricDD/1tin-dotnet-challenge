﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Quiz
{
    #region Comments
    //Date: 16/04/15 13:00
    //Author: Cédric De Dycker
    #endregion

    public class GameState: State
    {
        #region Properties
        #region Display
        private Canvas playCanvas;
        private Background background;
        private GamePanel gp;
        private int width, height;
        #endregion
        #region Objects
        private List<GameObject> gameobjects;
        private Player[] players;
        private Computer[] enemies;
        private int[][] pCoordinates;
        private int[][] eCoordinates;
        private int AmountOfShips;
        private int amount = 0;
        public int playerAmount;
        public int enemyAmount;
        private Player selectedPlayer;
        #region Mouse
        private double crossx, crossy;
        private PointerCross cross;
      #endregion
        #endregion
        #region Controls
        private CollisionManager cm;
        private Randomizer rn;
        private bool setDirection = false;
        private AI ai;
        private bool player = true; // if true: player
        private bool computer = false;
        #endregion
        #endregion

        #region Constructor
        public GameState(Game g, int TimeToPlay, int AmountOfShips)
        :base(g)
        {
            this.AmountOfShips = AmountOfShips;
        }
        #endregion

        #region Methods
        public override void Update()
        {
            gp.Update();
            if (gameobjects != null)
            {
                for (int i = 0; i < gameobjects.Count; i++)
                { 
                    gameobjects[i].Update();
                }
                cm.CollisionCheck(gameobjects, true);
                CheckTurn();
                //Turn checks
                if (computer)
                {
                    ai.ComputerTurn();
                    computer = false;
                    gp.ResetSpeed();
                    player = true;
                }
            }
        }

        public override void Draw(Canvas c)
        {
            playCanvas.Children.Clear();
            #region PlayCanvas
            if (gameobjects != null)
            {
                for (int i = 0; i < gameobjects.Count; i++)
                {
                    try
                    {
                        gameobjects[i].Draw(playCanvas);
                    }
                    catch (FailedShipMarginException)
                    {
                    }
                }
            }
            if (setDirection)
            {
            cross.Margin = new Thickness(crossx - 16,crossy - 16,0,0);
            playCanvas.Children.Add(cross);
            }
            #endregion
            #region Bigger Canvas
            c.Children.Add(playCanvas);
            c.Children.Add(gp);
            #endregion
        }

        public override void Init()
        {
            #region Setup Display
            #region Canvas
            playCanvas = new Canvas();
            playCanvas.Width = Constants.ScreenWidth;
            playCanvas.Height = Constants.ScreenHeight;
            playCanvas.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            playCanvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            playCanvas.MouseLeftButtonDown += MouseDownClick;
            width = Constants.ScreenWidth - 50;
            height = Constants.ScreenHeight;

            
            #endregion
            #region Background
            background = new Background(Constants.GameBackgroundPath);
            playCanvas.Background = background.GetBrush();
            #endregion
            #region Setup Panel
            gp = new GamePanel(this.game);
            gp.HorizontalAlignment = HorizontalAlignment.Right;
            gp.VerticalAlignment = VerticalAlignment.Center;
            gp.Margin = new Thickness(Constants.ScreenWidth - 60, 0, 20, 0);
            gp.shootButton.Click += Shoot;
            #endregion
            #endregion
            #region Setup Assets
            cross = new PointerCross();
            cm = new CollisionManager(width, height);
            rn = new Randomizer();
            #region amount
            if (Constants.ObjectSize * AmountOfShips <= (Constants.ObjectSize/2 + Constants.ObjectSize * (2 * AmountOfShips))+Constants.ObjectSize/2)
            {
                amount = AmountOfShips;
            }
            else
            {
                amount = 6;
            }
            playerAmount = amount;
            enemyAmount = amount;
            #endregion
            #region Setup Coordinates
            pCoordinates = new int[amount][];
            eCoordinates = new int[amount][];
            for (int i = 0; i < amount; i++)
            {
                pCoordinates[i] = new int[2];
                eCoordinates[i] = new int[2];
                pCoordinates[i][0] = 100;
                pCoordinates[i][1] = (Constants.ObjectSize/2 + Constants.ObjectSize * (2 * i));
                eCoordinates[i][0] = width - 100;
                eCoordinates[i][1] = Constants.ObjectSize  + Constants.ObjectSize*(2*i);
            }
            #endregion
            gameobjects = new List<GameObject>();
            players = new Player[amount];
            enemies = new Computer[amount];
            #endregion
            #region Setup Ships
            SetupShips();
            GameObject.SetGameState(this);
            player = true;
            computer = false;
            #endregion
            #region Setup Controls
            ai = new AI(this);
            game.PlayTime.Start();
            #endregion
        }
        private void SetupShips()
        {
            for (int i = 0; i < amount; i++)
            {
                players[i] = new Player(pCoordinates[i][0], pCoordinates[i][1], i);
                enemies[i] = new Computer(eCoordinates[i][0], eCoordinates[i][1], i, 180, 0);
                #region Set Handlers
                (players[i]).hit.spriteButton.Click += (players[i]).Player_Click;
                (players[i]).alive.spriteButton.Click += (players[i]).Player_Click;
                #endregion
                gameobjects.Add(players[i]);
                gameobjects.Add(enemies[i]);

            }
        }
       

       
        private void CheckTurn()
        {
            if (playerAmount == 0 || enemyAmount == 0)
            {
                selectedPlayer = null;
                gameobjects.Clear();
                game.WriteScore();
                Init();
                
            }
            if (selectedPlayer != null && selectedPlayer.dx == 0 && selectedPlayer.dy == 0 && !computer && !player)
            {
                computer = true;
            }
            
            
        }

        public void SelectShip(int id)
        {
            if (player)
            {
                for (int i = 0; i < players.Length; i++)
                {
                    players[i].selected = false;
                }
                players[id].selected = true;
                selectedPlayer = players[id];
            }
            
        }

        public void RemovePlayer(int id)
        {
            try
            {
                if (selectedPlayer.Equals(players[id]) && selectedPlayer != null) { selectedPlayer = null; player = false; computer = true; }
                playerAmount--;
                gameobjects.Remove(players[id]);
            }
            catch (NullReferenceException)
            {
                String exMessage = String.Format("{0} / {1} / {2}", Constants.FailedShipRemove, players.ToString(), players[id].ToString());
                if (selectedPlayer != null)
                {
                    exMessage = String.Format("{0} / {1}", exMessage, selectedPlayer.ToString());
                }
                throw new FailedShipRemove(exMessage);
            }            
        }
        public void RemoveComputer(int id)
        {
            gameobjects.Remove(enemies[id]);
            enemyAmount--;
        }

        #region Handler
        private void MouseDownClick(object sender, MouseButtonEventArgs e)
        {
            if (selectedPlayer != null && player)
            {
                Point mouse = e.GetPosition(playCanvas);
                crossx = mouse.X;
                crossy = mouse.Y;
                double angle = Calculator.AngleTwoPoints((selectedPlayer.X - selectedPlayer.width / 2), (selectedPlayer.Y - selectedPlayer.height / 2), crossx - 16, crossy - 16);
                selectedPlayer.SetAngle(angle);
                setDirection = true;
              
            }
        }
        public void Shoot(object sender, EventArgs e)
        {
            if (selectedPlayer != null && player)
            {
                double speed = gp.GetValue();
                if (speed > Constants.MaxSpeed) speed = Constants.MaxSpeed;
                if (speed < 5) speed = 5;
                selectedPlayer.SetSpeed(speed);
                selectedPlayer.UpdateMovement();              
                setDirection = false;
                player = false;
            }
        }
       
        #endregion

        #region Getters && Setters
        public Player[] GetPlayers()
        {
            return players;
        }
        public Computer[] GetEnemies()
        {
            return enemies;
        }
        public List<GameObject> GetGameObjects()
        {
            return gameobjects;
        }
       
        #endregion
        #endregion
    }
}
