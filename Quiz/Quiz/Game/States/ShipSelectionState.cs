﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 01/05/15 20:30
    //Author: Cédric De Dycker
    #endregion

    class ShipSelectionState : State
    {
        #region Properties
        private ShipSelectionPanel panel;
        #endregion

        #region Constructor
        public ShipSelectionState(Game game)
        :base(game)
        {

        }
        #endregion

        #region Methods
        public override void Update()
        {
        }

        public override void Draw(System.Windows.Controls.Canvas c)
        {
            c.Children.Add(panel);
        }

        public override void Init()
        {
            panel = new ShipSelectionPanel();
            panel.Ship0Button.Click += Ship1Button_Click;
            panel.Ship1Button.Click += Ship0Button_Click;
            panel.backButton.Click += backButton_Click;

        }

        void backButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            game.SetMenu(sender, e);
        }

        void Ship1Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Constants.SelectedShip = 1;
            Constants.SelectedEnemy = 1;
            game.StartPlay();
        }

        void Ship0Button_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Constants.SelectedShip = 0;
            Constants.SelectedEnemy = 0;
            game.StartPlay();
        }
        #endregion
    }
}
