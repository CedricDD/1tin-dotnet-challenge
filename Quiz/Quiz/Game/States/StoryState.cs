﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments
    //Date: 16/04/15 13:00
    //Author: Cédric De Dycker
    #endregion
    class StoryState : State
    {
        #region Properties
        private StoryPanel panel;
        #endregion
        #region Constructor
        public StoryState(Game game)
        :base(game)
        {

        }
        #endregion
        #region Methods
        public override void Update()
        {
            //Story Does not update
        }
        public override void Draw(System.Windows.Controls.Canvas c)
        {
            c.Children.Add(panel);
        }
        public override void Init()
        {
            panel = new StoryPanel();
            panel.backButton.Click += game.SetMenu;
        }
        #endregion
    }
}
