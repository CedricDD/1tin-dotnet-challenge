﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Quiz
{

    #region Comments
    //Date: 28/04/15 16:00
    //Author: Cédric De Dycker
    #endregion

    class HelpState: State
    {  
        #region Properties
        private HelpPanel panel;
       
        #endregion

        #region Constructor
        public HelpState(Game g)
            :base(g)
        {
            Init();
        }
        #endregion

        #region Methods

        public override void Update()
        {

        }
        public override void Draw(Canvas c)
        {
           c.Children.Add(panel);
        }

        public override void Init()
        {
             #region Setup Display

            panel = new HelpPanel();
            panel.backButton.Click += backButton_Click;

             #endregion

        }

        private void backButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            game.SetMenu(sender, e);
        }

        #endregion
    }
}
