﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quiz
{
    #region Comments
    //Date: 16/04/15 13:00
    //Author: Cédric De Dycker
    #endregion

    public abstract partial class State : UserControl
    {
        #region Properties
        protected Game game;
        private static State currentState = null;
        #endregion

        #region Constructor
        public State(Game g)
        {
            InitializeComponent();
            game = g;        
        }
        #endregion

        #region Methods
        public abstract void Update();
        public abstract void Draw(Canvas c);
        public abstract void Init();

        public static State GetState()
        {
            return currentState;
        }
        public static void SetState(State newState)
        {
            currentState = newState;
        }
        #endregion
    }
}
