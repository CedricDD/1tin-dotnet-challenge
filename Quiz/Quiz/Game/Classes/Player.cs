﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Quiz
{
    #region Comments
    //Date: 10/04/15 18:00
    //Author: Cédric De Dycker
    #endregion

   public class Player: GameObject, IMove
    {
        #region Properties
        public bool selected = false;
        private int id;
        public Ship alive;
        public Ship hit;
        public Ship selectedAlive;
        public Ship selectedHit;
        private Ship currentShip;
        #endregion

        #region Constrcutor
        public Player(double x, double y, int id)
            : base(x, y, Constants.ObjectSize, Constants.ObjectSize, false)
        {
            this.id = id;
           
            Init();

            currentShip = alive;
            
        }
        #endregion

        #region Methods
        #region Core
        public override void Update()
        {
            Move();
            Friction();
            CheckSelection();
        }
        public override void Draw(Canvas c)
        {
            if (base.GetObjectState() == Constants.ObjectState.Dead)
            {
                return;
            }
            try
            {
                if (currentShip != null)
                {
                    RotateTransform r = new RotateTransform(Angle + 90);
                    currentShip.spriteImage.RenderTransform = r;
                    RenderOptions.SetBitmapScalingMode(alive, BitmapScalingMode.Fant);
                    currentShip.Margin = new System.Windows.Thickness(X, Y, 0, 0);
                    c.Children.Add(currentShip);
                }
            }
            catch (ArgumentException)
            {
                new FailedShipMarginException(String.Format("{0}\tShip: {1}", Constants.FailedShipMargin, id));
            }
        }
        private void Init()
        {
            alive = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}", Constants.playerBasePath, Constants.SelectedShip, Constants.imageExtension));
            hit = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}{3}", Constants.playerBasePath, Constants.SelectedShip, "Hit", Constants.imageExtension));
            selectedAlive = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}{3}", Constants.playerBasePath, Constants.SelectedShip, "Selected", Constants.imageExtension));
            selectedHit = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}{3}", Constants.playerBasePath, Constants.SelectedShip, "SelectedHit", Constants.imageExtension));
        }
        #endregion
        public void CheckSelection()
        {
            if (selected)
            {

                if (GetObjectState() == Constants.ObjectState.Hit) currentShip = selectedHit;
                if (GetObjectState() == Constants.ObjectState.Alive) currentShip = selectedAlive; return;
                
            }
            if (!selected)
            {

                if (GetObjectState() == Constants.ObjectState.Hit) currentShip = hit; 
                if (GetObjectState() == Constants.ObjectState.Alive) currentShip = alive;return;
            }
        }       
        public void Player_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Ship:" + id + "Angle:" + Angle);
            game.SelectShip(id);
           
        }
        public void Friction()
        {
            dx *= Constants.frictionCoefficient;
            dy *= Constants.frictionCoefficient;
            if (Math.Abs(dx) <= 0.05) dx = 0;
            if (Math.Abs(dy) <= 0.05) dy = 0;
        }
        public void UpdateMovement()
        {
            dx = Speed * Math.Cos((Angle * Math.PI) / 180);
            dy = Speed * Math.Sin((Angle * Math.PI) / 180);
        }
        public void Move()
        {
            X += dx;
            Y += dy;          
        }
        public void RemoveSip()
        {
            game.RemovePlayer(id);
        }
        public override string ToString()
        {
            return String.Format("Schip: {0} / {1} / {2}", id, X, Y);
        }
        #endregion

        #region Getters && Setters
        public Ship GetSprite()
        {
            return currentShip;
        }
        public void SetSpeed(double speed)
        {
            Speed = speed;
        }
        public void SetAngle(double angle)
        {
            Angle = angle;
        }
        public int GetId()
        {
            return id;
        }
        #endregion
    }
}
