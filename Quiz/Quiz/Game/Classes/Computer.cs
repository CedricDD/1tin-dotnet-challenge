﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Quiz
{

    #region Comments
    //Date: 10/04/15 22:00
    //Author: Cédric De Dycker
    #endregion

    public class Computer: GameObject, IMove
    {
        #region Properties
        public bool selected;
        private int id;
        private Ship alive;
        private Ship hit;
        #endregion

        #region Constrcutor
        public Computer(double x, double y, int id)
            : base(x, y, Constants.ObjectSize, Constants.ObjectSize, false)
        {
            this.id = id;
            
            Init();
        }
        public Computer(double x, double y, int id, double Angle, double Speed)
            : base(x, y, Constants.ObjectSize, Constants.ObjectSize, false, Angle, Speed)
        {
            this.id = id;
           
            Init();
            
        }
        #endregion

        #region Methods
        #region Core
        public override void Update()
        {
            Move();
            Friction();
        }
        public override void Draw(Canvas c)
        {
            if (base.GetObjectState() == Constants.ObjectState.Alive)
            {
                RotateTransform r = new RotateTransform(Angle + 90);
                alive.spriteImage.RenderTransform = r;
                RenderOptions.SetBitmapScalingMode(alive, BitmapScalingMode.Fant);
                alive.Margin = new System.Windows.Thickness(X, Y, 0, 0);
                c.Children.Add(alive);
            }
            if (base.GetObjectState() == Constants.ObjectState.Hit)
            {
                RotateTransform r = new RotateTransform(Angle + 90);
                hit.spriteImage.RenderTransform = r;
                RenderOptions.SetBitmapScalingMode(hit, BitmapScalingMode.Fant);
                hit.Margin = new System.Windows.Thickness(X, Y, 0, 0);
                c.Children.Add(hit);
            }
            if (base.GetObjectState() == Constants.ObjectState.Dead)
            {
                Ellipse e = new Ellipse();
                e.Width = 64;
                e.Height = 64;
                    e.Fill = new SolidColorBrush(Colors.Red);
                e.Margin = new System.Windows.Thickness(X, Y, 0, 0);
                c.Children.Add(e);
            }
        }
        private void Init()
        {
            alive = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}", Constants.enemyBasePath, Constants.SelectedShip, Constants.imageExtension));
            hit = new Ship(Constants.ObjectSize, Constants.ObjectSize, String.Format("{0}{1}{2}{3}", Constants.enemyBasePath, Constants.SelectedShip, "Hit", Constants.imageExtension));
        }
        #endregion
        public void Player_Click(object sender, EventArgs e)
        {
            selected = true;
        }
        public void Friction()
        {
            dx *= Constants.frictionCoefficient;
            dy *= Constants.frictionCoefficient;
            if (Math.Abs(dx) <= 0.05) dx = 0;
            if (Math.Abs(dy) <= 0.05) dy = 0;
        }
        public void UpdateMovement()
        {
            dx = Speed * Math.Cos((Angle * Math.PI) / 180);
            dy = Speed * Math.Sin((Angle * Math.PI) / 180);
        }
        public void Move()
        {
            X += dx;
            Y += dy;
        }
        public void EnemeyRemove()
        {
            game.RemoveComputer(id);
        }
        #endregion
        #region Getters && Setters
        public int GetId()
        {
            return id;
        }
        public void SetSpeed(double speed)
        {
            Speed = speed;
        }
        public void SetAngle(double angle)
        {
            Angle = angle;
        }
        #endregion
    }
}
