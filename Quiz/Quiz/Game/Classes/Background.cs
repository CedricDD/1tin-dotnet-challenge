﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Quiz
{

    #region Comments
    //Date: 23/04/15 22:00
    //Author: Cédric De Dycker
    #endregion

    class Background
    {
        #region Properties
        private BitmapImage background;
        private ImageBrush backgroundBrush;
        private String path;
        #endregion
        #region Constructor
        public Background(String path)
        {
            this.path = path;
            CreateImage(path);
            backgroundBrush = new ImageBrush(background);
        }
        #endregion
        #region Methods
        private void CreateImage(String path)
        {           
           background = ImageManager.GetBitMapImage(path, Constants.ScreenWidth, Constants.ScreenHeight);
           RenderOptions.SetBitmapScalingMode(background, BitmapScalingMode.Fant);
        }
        public BitmapImage GetRawImage()
        {
            return background;
        }
        public ImageBrush GetBrush()
        {
            return backgroundBrush;
        }
        #endregion
    }
}
