﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{

    #region Comments
    //Date: 10/04/15 18:30
    //Author: Cédric De Dycker
    #endregion

    class CollisionManager
    {
        #region Properties
        private int width, height;
        #endregion
        #region Constructor
        public CollisionManager(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        #endregion
        #region Methods
        public void CollisionCheck(List<GameObject> list, bool doHit)
        {
            CanvasCollision(list);
            ObjectCollision(list, doHit);
        }
        public void CanvasCollision(List<GameObject> list)
        {
            #region Loop
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] == null) return;

                if (list[i].X < 0)
                {
                    list[i].X = 0;
                    list[i].dx *= -1;
                    list[i].CheckAngle();
                    
                }
                if (list[i].Y < 0)
                {
                    list[i].Y = 0;
                    list[i].dy *= -1;
                    list[i].CheckAngle();
                }
                if (list[i].X > (width - list[i].width))
                {
                    list[i].X = width - list[i].width;
                    list[i].dx *= -1;
                    list[i].CheckAngle();
                }
                if (list[i].Y > (height - list[i].height))
                {
                    list[i].Y = height - list[i].height;
                    list[i].dy *= -1;
                    list[i].CheckAngle();
                }

            }
            #endregion
        }
        public void ObjectCollision(List<GameObject> list, bool doHit)
        {
            for (int i = 0; i < list.Count; i++)
            {
                for (int j = i + 1; j < list.Count; j++)
                {
                    #region Circle Collision Check
                    if (list[i] == null || list[j] == null || list[i].GetObjectState() == Constants.ObjectState.Dead || list[j].GetObjectState() == Constants.ObjectState.Dead) return;
                    double vx = list[j].X - list[i].X;
                    double vy = list[j].Y - list[i].Y;
                    double dist = Math.Sqrt(Math.Pow(vx, 2) + Math.Pow(vy, 2));
                    #endregion
                    if (dist <= (list[i].width / 2 + list[j].width / 2))
                    {
                        #region Calculations
                        double nx = vx / dist;
                        double ny = vy / dist;
                        double midpx = (list[i].X + list[j].X) / 2;//Middle point x value of line between intersecting circles
                        double midpy = (list[i].Y + list[j].Y) / 2;// y value

                        //Reposition Balls so they do not hit each other
                        list[i].X = midpx - list[i].width/2 * nx;
                        list[i].Y = midpy - list[i].height/2 * ny;
                        list[j].X = midpx + list[j].width/2 * nx;
                        list[j].Y = midpy + list[j].height/2 * ny;

                        //Etablish new vector
                        double dvec = (list[i].dx - list[j].dx) * nx;
                        dvec += (list[i].dy - list[j].dy) * ny;

                        //Fragment vector on both axes
                        double dvx = dvec * nx;
                        double dvy = dvec * ny;

                        //Predefinen elastic collision
                        if (!list[i].solid)
                        {
                            list[i].dx -= dvx;
                            list[i].dy -= dvy;
                            list[i].CheckAngle();
                                                    }
                        if (!list[j].solid)
                        {
                            list[j].dx += dvx;
                            list[j].dy += dvy;
                            list[j].CheckAngle();
                        }

                        if (doHit) { CheckState(list[i], list[j]); }
                        #endregion
                    }

                }
            }
        }
        private void CheckState(GameObject g, GameObject gg)
        {
            if (g is Player)
            {
                #region Player
                if (gg is Player)
                {
                    if ((g.GetObjectState() == Constants.ObjectState.Alive && gg.GetObjectState() == Constants.ObjectState.Hit))
                    {
                        gg.SetObjectState(Constants.ObjectState.Alive);
                    }
                    if ((g.GetObjectState() == Constants.ObjectState.Hit && gg.GetObjectState() == Constants.ObjectState.Alive))
                    {
                        g.SetObjectState(Constants.ObjectState.Alive);
                    }
                }
                #endregion

                #region Computer
                if (gg is Computer)
                {
                    if (gg.GetObjectState() == Constants.ObjectState.Hit) { gg.SetObjectState(Constants.ObjectState.Dead); ((Computer)gg).EnemeyRemove(); };
                    if (gg.GetObjectState() == Constants.ObjectState.Alive) gg.SetObjectState(Constants.ObjectState.Hit);
                    if (g.GetObjectState() == Constants.ObjectState.Hit)
                    {
                        g.SetObjectState(Constants.ObjectState.Dead); try { ((Player)g).RemoveSip(); }
                        catch (FailedShipRemove)
                        {
                        }
                    }
                    if (g.GetObjectState() == Constants.ObjectState.Alive) g.SetObjectState(Constants.ObjectState.Hit);
                }
                #endregion
            }

            if (g is Computer)
            {
                #region Computer
                if (gg is Computer)
                {
                    if ((g.GetObjectState() == Constants.ObjectState.Alive && gg.GetObjectState() == Constants.ObjectState.Hit))
                    {
                        gg.SetObjectState(Constants.ObjectState.Alive);
                    }
                    if ((g.GetObjectState() == Constants.ObjectState.Hit && gg.GetObjectState() == Constants.ObjectState.Alive))
                    {
                        g.SetObjectState(Constants.ObjectState.Alive);
                    }
                }
                #endregion

                #region Player
                if (gg is Player)
                {
                    if (gg.GetObjectState() == Constants.ObjectState.Hit)
                    {
                        gg.SetObjectState(Constants.ObjectState.Dead); try { ((Player)gg).RemoveSip(); }
                        catch (FailedShipRemove)
                        {
                        }
                    }
                    if (gg.GetObjectState() == Constants.ObjectState.Alive) gg.SetObjectState(Constants.ObjectState.Hit);
                    if (g.GetObjectState() == Constants.ObjectState.Hit) { g.SetObjectState(Constants.ObjectState.Dead); ((Computer)g).EnemeyRemove(); }
                    if (g.GetObjectState() == Constants.ObjectState.Alive) g.SetObjectState(Constants.ObjectState.Hit);
                }
                #endregion
            }
            Game.AddScore();
        }
        #endregion
    }
}
