﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{
    #region Comments    
     //Date: 23/04/15 22:00
     //Author: Cédric De Dycker     
    #endregion

    class AI
    {
        #region Variables
        private GameState game;
        private Randomizer ShipSelector;
        private Randomizer offsetPercentage;
        private Randomizer Speed;
        #endregion

        #region Constructor
        public AI(GameState game)
        {
            this.game = game;
            Init();
        }
        #endregion

        #region Methods
        public void Init()
        {
            ShipSelector = new Randomizer();
            offsetPercentage = new Randomizer();
            Speed = new Randomizer();
        }
        public void ComputerTurn()
        {
            if (game.enemyAmount > 0)
            {
                //Select Ships
                GameObject First = game.GetGameObjects()[ShipSelector.Next(0, game.GetGameObjects().Count - 1)];
                Computer Enemy = null;

                while (First is Player)
                {
                    First = game.GetGameObjects()[ShipSelector.Next(0, game.GetGameObjects().Count - 1)];
                }
                if (First is Computer)
                {
                    Enemy = (Computer)First;
                }

                GameObject Other = game.GetGameObjects()[ShipSelector.Next(0, game.GetGameObjects().Count - 1)];
                while (Other is Computer && ((Computer)Other).GetId() == Enemy.GetId())
                {
                    Other = game.GetGameObjects()[ShipSelector.Next(0, game.GetGameObjects().Count - 1)];
                }

                //Select speed && Angle
                int speed = Speed.Next(5, Constants.MaxSpeed);
                double angle = 0;
                if (Other is Computer)
                {
                    angle = Calculator.AngleTwoPoints(Enemy.X - Constants.ObjectSize, Enemy.Y - Constants.ObjectSize / 2, Other.X - Constants.ObjectSize / 2, Other.Y - Constants.ObjectSize / 2);
                }
                if (Other is Player)
                {
                    angle = Calculator.AngleTwoPoints(Enemy.X - Constants.ObjectSize, Enemy.Y - Constants.ObjectSize / 2, Other.X - Constants.ObjectSize / 2, Other.Y - Constants.ObjectSize / 2);
                }
                angle += offsetPercentage.Next(0, 5);
                Enemy.SetAngle(angle);
                Enemy.SetSpeed(speed);
                Enemy.UpdateMovement();
            }
        }
        #endregion
    }
}
