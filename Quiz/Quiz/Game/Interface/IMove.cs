﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quiz
{

    #region Comments
    //Date: 10/04/15 19:00
    //Author: Cédric De Dycker
    #endregion

    interface IMove
    {
        void Friction();
        void UpdateMovement();
        void Move();
        void SetSpeed(double speed);
        void SetAngle(double angle);
    }
}
