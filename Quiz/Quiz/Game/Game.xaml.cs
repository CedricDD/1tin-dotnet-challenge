﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Quiz
{
    /// <summary>
    /// Date: 2-4-2015 22:35
    /// Author: Cédric De Dycker
    /// </summary>
    public partial class Game : UserControl
    {
        #region Variables
        private int startAmount;
        private Canvas gameCanvas;
        private bool running = false;
        private MainWindow window;
        private int passedtime = 0;
        private static int score = 0;
        private int timeToPlay;
        private int timeLeft;

        #region State
        private State Menu;
        private State Play;
        private State Help;
        private State Story;
        private State ShipSelection;
        #endregion

        #region Timers
        private DispatcherTimer GameTick;
        private double TimeTick;
        public DispatcherTimer PlayTime;
        #endregion

        #endregion

        #region Constructor
        public Game(MainWindow w, int timeToPlay)
        {
            InitializeComponent();
            InitAssets();
            window = w;
            this.timeToPlay = timeToPlay;
        }
        #endregion

        #region Methods
        #region Core
        private void GameLoop(object sender, EventArgs e)
        {
            //Main loop of the program.
            Update();
            Draw();
        }
        private void Update()
        {
            if (State.GetState() == null) return;
            State.GetState().Update();
        }
        private void Draw()
        {
            gameCanvas.Children.Clear();
            if (State.GetState() == null) return;
            State.GetState().Draw(gameCanvas);
        }
        #endregion
        #region Controls
        public void StartLoop()
        {
            if (GameTick == null || running) return; // If running or not initialized return
            GameTick.Start();
            running = true;
        }
        public void StopLoop()
        {
            if (running) return; // Check for Wrong call
            GameTick.Stop();// Stop main loop timer
            PlayTime.Stop();// Stop time to play timer
            //Reset Window
            window.mainContentControl.Content = new SplashScreenUserControl();
            //Reset some game variables and dispose of some elements;
            GameReset();
            if (window.GetLoggedInUser() == null) return; // If not user end game here
            WriteScore();

        }
        public void GameReset()
        {
            gameCanvas.Children.Clear();
            //Gabrage collector will clean this.
            Play = null;
            Menu = null;
            Help = null;
        }
        public void StartPlay()
        {
            Play.Init();
            State.SetState(Play);
        }
        public void StartHelp()
        {
            Help.Init();
            State.SetState(Help);
        }
        public void StartStory()
        {
            Story.Init();
            State.SetState(Story);
        }
        public void StartShipSelection()
        {
            ShipSelection.Init();
            State.SetState(ShipSelection);
        }
        public void StopGame()
        {
            if (!running) return;
            running = false;
            StopLoop();
        }
        private void PlayTime_Tick(object sender, EventArgs e)
        {
            if (passedtime >= timeToPlay)
            {
                StopGame();
            }
            passedtime++;
            timeLeft = timeToPlay - passedtime;
        }
        public void WriteScore()
        {
            if (window.GetLoggedInUser() is Student) // Check if current user is student
            {
                if (passedtime < timeToPlay)
                {
                    //Give unused quizscore back to user
                    ((Student)window.GetLoggedInUser()).SetQuizScore(timeToPlay - passedtime);
                }
                if (Play != null)
                {
                    //Calculate score
                    int tempscore = score + (((GameState)Play).playerAmount / Constants.ShipAmount) * 10 - (((GameState)Play).enemyAmount / Constants.ShipAmount) * 10;
                    if (tempscore < 0) tempscore = 0;
                    ((Student)window.GetLoggedInUser()).SetGameScore(tempscore);
                }
                //Write changes to user file
                FileManager.WriteUserFile(window.GetLoggedInUser());
                window.UpdateLoggedInUserLabel();

            }
        }
        #endregion
        #region Getters && Setters
        public bool GetRunningState()
        {
            return running;
        }
        public int GetTimeLeft()
        {
            return timeLeft;
        }
        public static void AddScore()
        {
            score += 10;
        }
        public void SetMenu(object sender, EventArgs e)
        {
            State.SetState(Menu);
        }
        #endregion

        private void InitAssets()
        {
            #region Setup Variables
            startAmount = Constants.ShipAmount;
            Constants.playerSpritePath = String.Format("{0}{1}{2}", Constants.playerBasePath, Constants.SelectedShip, Constants.imageExtension);
            Constants.enemySpritePath = String.Format("{0}{1}{2}", Constants.enemyBasePath, Constants.SelectedEnemy, Constants.imageExtension);

            Constants.MenuBackgroundPath = String.Format("{0}{1}{2}", Constants.BackgroundBasePath, Constants.SelectedMenuBackground, Constants.imageExtension);
            Constants.GameBackgroundPath = String.Format("{0}{1}{2}", Constants.BackgroundBasePath, Constants.SelectedGameBackground, Constants.imageExtension);
            #endregion

            #region Setup Screen
            gameCanvas = new Canvas();
            gameCanvas.Height = Constants.ScreenHeight;
            gameCanvas.Width = Constants.ScreenWidth;
            gameCanvas.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            gameCanvas.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            this.Content = gameCanvas;
            #endregion

            #region Setup States
            Menu = new MenuState(this);
            Help = new HelpState(this);
            Story = new StoryState(this);
            ShipSelection = new ShipSelectionState(this);
            Play = new GameState(this, timeToPlay, Constants.ShipAmount);
            State.SetState(Menu);
            #endregion

            #region Setup Timers
            TimeTick = 1000 / Constants.fps;
            GameTick = new DispatcherTimer();
            GameTick.Interval = TimeSpan.FromMilliseconds(TimeTick);
            GameTick.Tick += GameLoop;

            PlayTime = new DispatcherTimer();
            PlayTime.Interval = TimeSpan.FromSeconds(1);
            PlayTime.Tick += PlayTime_Tick;

            #endregion
        }



        #endregion
    }
}
